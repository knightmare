/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module mesengine;
private:

import iv.strex;
import iv.vfs;
import iv.vfs.io;

import mesactor;


version = mesdbg_dump_generated_code;
version = mesdbg_dump_compiled_code;
//version = mesdbg_dump_dump_execution;
//version = mes_paranoid_arrays; // always check array access (no need to, compiler will generate bounds checking)
public __gshared bool MESDisableDumps = false;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared void delegate (string msg) mesOnCompilerMessage;


public void compilerMessage(A...) (string fmt, A args) {
  if (mesOnCompilerMessage !is null) {
    import std.format : format;
    mesOnCompilerMessage(fmt.format(args));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class CompilerError : Exception {
  Loc loc;
  this() (in auto ref Loc aloc, string msg, string file=__FILE__, usize line=__LINE__) nothrow @safe @nogc {
    loc = aloc;
    super(msg, file, line);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class OverloadNotFound : Exception {
  this (string msg, string file=__FILE__, usize line=__LINE__) nothrow @safe @nogc {
    super(msg, file, line);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct Loc {
  int line, col;
  string fname;

  @property string toString () const {
    import std.format : format;
    return "(%d,%d)".format(line, col);
  }

  @property string toFullString () const {
    import std.format : format;
    if (fname.length) return "%s (%d,%d)".format(fname, line, col);
    return "(%d,%d)".format(line, col);
  }
}


public struct Token {
public:
  enum T {
    LitNone,
    LitStr,
    LitInt,
    LitId,
    // delimiter (sval is char or two chars)
    LitDelim,
    // keywords
    Bool,
    Ref,
    Out,
    In,
    True,
    False,
    Int,
    String,
    Actor,
    ActorDef,
    Void,
    Auto,
    Struct,
    Return,
    If,
    Else,
    While,
    For,
    Switch,
    Case,
    Const,
    Enum,
    Cast,
    Method,
    Function,
    Alias,
    Continue,
    Break,
    Null,
    Is,
    New,
    Abs, //HACK
    Default,
  }

public:
  T type = T.LitNone;
  Loc loc;
  string sval;
  int ival;

public:
  @property bool empty () const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitNone); }
  @property bool str () const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitStr); }
  @property bool num () const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitInt); }

  bool id () const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitId); }
  bool delim () const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitDelim); }
  bool kw () const pure nothrow @safe @nogc { pragma(inline, true); return (type >= T.Bool); }

  bool id (const(char)[] v) const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitId && sval == v); }
  bool delim (const(char)[] v) const pure nothrow @safe @nogc { pragma(inline, true); return (type == T.LitDelim && sval == v); }
  bool kw (T v) const pure nothrow @safe @nogc { pragma(inline, true); return (v >= T.Bool && type == v); }
}


public class ParserBase {
public:

protected:
  Loc mTokLoc;
  Loc mCurLoc;
  char mNextCh;
  char mCurCh;
  Token mCurToken;

protected:
  abstract char getch (); // 0: eof

  final void readToken (ref Token ctk) {
    ctk = ctk.init;
    ctk.type = Token.T.LitNone;
    skipSpaces();
    ctk.loc = mCurLoc;
    if (mCurCh == 0) return;

    // number?
    if (mCurCh >= '0' && mCurCh <= '9') {
      int base = 0;
      if (mCurCh == '0') {
        switch (mNextCh) {
          case 'x': case 'X': base = 16; break;
          case 'o': case 'O': base = 8; break;
          case 'b': case 'B': base = 2; break;
          case 'd': case 'D': base = 10; break;
          default: break;
        }
      }
      if (base == 0) {
        base = 10;
      } else {
        skipch();
        skipch();
        if (digitInBase(mCurCh, base) < 0) errorAt(ctk.loc, "invalid number");
      }
      int n = 0;
      for (;;) {
        if (mCurCh != '_') {
          if (digitInBase(mCurCh, base) < 0) break;
          n = n*base+digitInBase(mCurCh, base);
          if (n < 0) errorAt(ctk.loc, "integer overflow");
        }
        skipch();
      }
      if (mCurCh >= 127 || isalpha(mCurCh)) errorAt(ctk.loc, "invalid number");
      ctk.type = ctk.T.LitInt;
      ctk.ival = n;
      return;
    }

    // identifier?
    if (mCurCh == '_' || isalpha(mCurCh) || mCurCh >= 127) {
      while (mCurCh && (mCurCh == '_' || isalnum(mCurCh) || mCurCh >= 127)) {
        ctk.sval ~= mCurCh;
        skipch();
      }
      ctk.type = ctk.T.LitId;
      if (isalpha(ctk.sval[0])) {
        mainloop: foreach (string mname; __traits(allMembers, ctk.T)) {
          static if (mname == "Actor" || (mname.length > 3 && mname[0..3] == "Lit")) {
            // nothing
          } else {
            if (ctk.sval.length == mname.length && ctk.sval[0] == cast(char)(mname[0]+32)) {
              bool ok = true;
              foreach (immutable idx, immutable char ch; mname[1..$]) {
                if (ctk.sval[idx+1] != ch) { ok = false; break; }
              }
              if (ok) {
                ctk.type = __traits(getMember, ctk.T, mname);
                break mainloop;
              }
            }
          }
        }
        if (ctk.sval == "Actor") ctk.type = Token.T.Actor;
      }
      return;
    }

    // backslash is skipped
    char parseEscapeChar () {
      char res;
      if (!mCurCh) errorAt(ctk.loc, "invalid escape");
      switch (mCurCh) {
        case 't': res = '\t'; break;
        case 'n': res = '\n'; break;
        case 'r': res = '\r'; break;
        case 'e': res = '\x1b'; break;
        case 'x':
          skipch(); // 'x'
          int n = digitInBase(mCurCh, 16);
          if (n < 0) errorAt(ctk.loc, "invalid hex escape");
          skipch(); // first hex digit
          if (digitInBase(mCurCh, 16) >= 0) {
            n = n*16+digitInBase(mCurCh, 16);
            skipch();
          }
          return cast(char)n;
        default:
          if (isalnum(mCurCh)) errorAt(ctk.loc, "invalid escape");
          res = mCurCh;
          break;
      }
      skipch();
      return res;
    }

    // string?
    if (mCurCh == '"') {
      skipch();
      while (mCurCh && mCurCh != '"') {
        if (mCurCh == '\\') {
          skipch();
          ctk.sval ~= parseEscapeChar();
        } else {
          ctk.sval ~= mCurCh;
          skipch();
        }
      }
      if (mCurCh != '"') errorAt(ctk.loc, "invalid string");
      skipch();
      ctk.type = ctk.T.LitStr;
      return;
    }

    // char?
    if (mCurCh == '\'') {
      skipch();
      if (!mCurCh) errorAt(ctk.loc, "invalid char");
      char ch;
      if (mCurCh == '\\') {
        skipch();
        ch = parseEscapeChar();
      } else {
        ch = mCurCh;
        skipch();
      }
      if (mCurCh != '\'') errorAt(ctk.loc, "\"'\" expected");
      skipch();
      ctk.type = ctk.T.LitInt;
      ctk.ival = cast(int)ch;
      return;
    }

    // delimiter
    ctk.type = ctk.T.LitDelim;
         if (mCurCh == '<' && mNextCh == '=') { ctk.sval = "<="; skipch(); }
    else if (mCurCh == '>' && mNextCh == '=') { ctk.sval = ">="; skipch(); }
    else if (mCurCh == '!' && mNextCh == '=') { ctk.sval = "!="; skipch(); }
    else if (mCurCh == '=' && mNextCh == '=') { ctk.sval = "=="; skipch(); }
    else if (mCurCh == '+' && mNextCh == '+') { ctk.sval = "++"; skipch(); }
    else if (mCurCh == '-' && mNextCh == '-') { ctk.sval = "--"; skipch(); }
    else if (mCurCh == '&' && mNextCh == '&') { ctk.sval = "&&"; skipch(); }
    else if (mCurCh == '|' && mNextCh == '|') { ctk.sval = "||"; skipch(); }
    else if (mCurCh == '+' && mNextCh == '=') { ctk.sval = "+="; skipch(); }
    else if (mCurCh == '-' && mNextCh == '=') { ctk.sval = "-="; skipch(); }
    else if (mCurCh == '*' && mNextCh == '=') { ctk.sval = "*="; skipch(); }
    else if (mCurCh == '/' && mNextCh == '=') { ctk.sval = "/="; skipch(); }
    else if (mCurCh == '%' && mNextCh == '=') { ctk.sval = "%="; skipch(); }
    else if (mCurCh == '&' && mNextCh == '=') { ctk.sval = "&="; skipch(); }
    else if (mCurCh == '|' && mNextCh == '=') { ctk.sval = "|="; skipch(); }
    else if (mCurCh == '^' && mNextCh == '=') { ctk.sval = "^="; skipch(); }
    else if (mCurCh == '<' && mNextCh == '<') { ctk.sval = "<<"; skipch(); }
    else if (mCurCh == '>' && mNextCh == '>') { ctk.sval = ">>"; skipch(); }
    else if (mCurCh == '.' && mNextCh == '.') {
      skipch(); // first dot
      if (mNextCh == '.') { ctk.sval = "..."; skipch(); } else ctk.sval = "..";
    } else ctk.sval ~= mCurCh;
    skipch();
  }

  void nextToken () {
    readToken(mCurToken);
  }

  void setup () {
    mCurLoc.line = 1;
    mCurLoc.col = 1;
    mCurLoc.fname = filename;
    mCurCh = getch();
    mNextCh = (mCurCh ? getch() : '\0');
  }

protected:
  static struct SavedState {
    Loc mCurLoc;
    char mNextCh;
    char mCurCh;
    Token mCurToken;

    this (ParserBase pr) nothrow @safe @nogc { saveFrom(pr); }

    void saveFrom (ParserBase pr) nothrow @safe @nogc {
      if (pr !is null) {
        this.mCurLoc = pr.mCurLoc;
        this.mNextCh = pr.mNextCh;
        this.mCurCh = pr.mCurCh;
        this.mCurToken = pr.mCurToken;
      }
    }

    void restoreTo (ParserBase pr) nothrow @safe @nogc {
      if (pr !is null) {
        pr.mCurLoc = this.mCurLoc;
        pr.mNextCh = this.mNextCh;
        pr.mCurCh = this.mCurCh;
        pr.mCurToken = this.mCurToken;
      }
    }
  }

public:
  // first token is NOT read after creation
  this () { setup(); }

  abstract @property string filename () nothrow @safe;

  final @property ref Token tk () nothrow @safe @nogc { pragma(inline, true); return mCurToken; }

  // at the location of the current token
  final void error (string amsg, string file=__FILE__, usize line=__LINE__) {
    throw new CompilerError(mCurToken.loc, amsg, file, line);
    assert(0); // "noreturn" hack
  }

  final void errorAt() (in auto ref Loc aloc, string amsg, string file=__FILE__, usize line=__LINE__) {
    throw new CompilerError(aloc, amsg, file, line);
    assert(0); // "noreturn" hack
  }

  final void skipch () {
    if (mCurCh == '\n') { mCurLoc.col = 1; ++mCurLoc.line; } else ++mCurLoc.col;
    mCurCh = mNextCh;
    if (mNextCh != 0) mNextCh = getch();
  }

  final void skipSpaces () {
    while (mCurCh) {
      // single-line comment
      if (mCurCh == '/' && mNextCh == '/') {
        while (mCurCh && mCurCh != '\n') skipch();
        continue;
      }
      // multiline comment
      if (mCurCh == '/' && mNextCh == '*') {
        skipch();
        skipch();
        while (mCurCh) {
          if (mCurCh == '*' && mNextCh == '/') {
            skipch();
            skipch();
            break;
          }
          skipch();
        }
        continue;
      }
      // multiline nested comment
      if (mCurCh == '/' && mNextCh == '+') {
        skipch();
        skipch();
        int level = 1;
        while (mCurCh) {
          if (mCurCh == '+' && mNextCh == '/') {
            skipch();
            skipch();
            if (--level == 0) break;
            continue;
          }
          if (mCurCh == '/' && mNextCh == '+') {
            skipch();
            skipch();
            ++level;
            continue;
          }
          skipch();
        }
        continue;
      }
      if (mCurCh > ' ') break;
      skipch();
    }
  }

  final void next() () { nextToken(); }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class FileParser : ParserBase {
private:
  VFile fl;
  string curfname = null;

private:
  static struct MySave {
    SavedState st;
    VFile fl;
  }

  MySave[] fstack;

private:
  void push () {
    if (fstack.length >= 32) error("too many nested includes");
    fstack.length += 1;
    fstack[$-1].st.saveFrom(this);
    fstack[$-1].fl = fl;
  }

  void pop () {
    if (fstack.length == 0) error("inclide stack underflow");
    fl = fstack[$-1].fl;
    fstack[$-1].st.restoreTo(this);
    fstack.length -= 1;
    fstack.assumeSafeAppend;
    curfname = null;
  }

protected:
  override char getch () {
    char res;
    if (fl.rawRead((&res)[0..1]).length != 0) {
      if (res == 0) res = ' ';
    } else {
      res = 0;
    }
    return res;
  }

private:
  void delegate (const(char)[] fname) onInclude;

public:
  // first token is NOT read after creation
  this (VFile afl, void delegate (const(char)[] fname) aOnInclude=null) {
    fl = afl;
    super();
    onInclude = aOnInclude;
    if (onInclude !is null && fl.isOpen) onInclude(fl.name);
  }

  override @property string filename () nothrow @safe {
    if (curfname is null && fl.isOpen) curfname = fl.name.idup;
    return curfname;
  }

  override void nextToken () {
    for (;;) {
      super.nextToken();
      if (mCurToken.empty) {
        // eof, pop file
        if (fstack.length == 0) return;
        pop();
      } else if (mCurToken.delim("#")) {
        super.nextToken();
        if (mCurToken.empty) error("parser directive expected");
        if (!mCurToken.id("include")) error("inknown compiler directive");
        super.nextToken();
        if (!mCurToken.str) error("string expected");
        string fname = mCurToken.sval;
        if (fname.length == 0) error("empty file name");
        string oldname = fl.name.idup;
        auto lslash = oldname.lastIndexOf('/');
        oldname = oldname[0..lslash+1];
        oldname ~= fname;
        auto nfl = VFile(oldname);
        push();
        fl = nfl;
        if (onInclude !is null) onInclude(fl.name);
        curfname = null;
        setup();
      } else {
        return;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// VIRTUAL MACHINE
//
enum OpCode {
  Nop, // 3 bytes: payload
  CheckStack, // u16: number of stack slots this function is using
  // literals
  LitNU, // dest; followed by Nop payload, unsigned
  LitNS, // dest; followed by Nop payload, signed
  LitU16, // dest; U16 literal
  LitS16, // dest; S16 literal
  Mov, // dest, op0 as src
  Inc, // dest, op0 -- dest = op0+1
  Dec, // dest, op0 -- dest = op0-1
  // integer math (dest, op0, op1)
  Add,
  Sub,
  Mul,
  Div,
  Mod,
  Shl,
  Shr,
  BitAnd,
  BitOr,
  BitXor,
  // integer comparisons
  ILess,
  IGreat,
  ILessEqu,
  IGreatEqu,
  // string comparisons
  SLess,
  SGreat,
  SLessEqu,
  SGreatEqu,
  // the following are ok for any types
  Equ,
  NotEqu,
  // logical operators
  LogAnd,
  LogOr,
  // one-op math
  IntAbs, // dest, op0
  // branching
  Jump, // s16: offset (from next opcode)
  JTrue, // src: slot; s16: offset (from next opcode)
  JFalse, // src: slot; s16: offset (from next opcode)
  // unary logical
  LogNot, // dest, op0
  LogBNorm, // dest, op0 -- normalize boolean value
  // globals
  GLoad, // dest, 2 bytes: index (offset in global area)
  GStore, // src, 2 bytes: index (offset in global area)
  // arguments
  ALoad, // dest (slot), src (argnum)
  AStore, // dest (argnum), src (slot)
  ACount, // dest -- push number of arguments
  // fields
  FLoad, // dest (slot), actid, fldidx -- load field value to dest
  FStore, // src (slot), actid, fldidx -- store field value from src
  // strings
  StrLen, // dest, src (string idx)
  StrLoadChar, // dest, src (string idx), strofs
  // arrays
  Bounds, // ofs (slot), u16: maxidx
  LArrClear, // dest is first slot; u16 is cell count
  GArrLoad, // dest, globofs (slot), cellofs (slot): [dest] = gvars[[op0]+[op1]]
  GArrStore, // src, globofs (slot), cellofs (slot): gvars[[op0]+[op1]] = [src]
  LArrLoad, // dest, bpofs (slot), cellofs (slot): [dest] = [op0+[op1]]
  LArrStore, // src, bpofs (slot), cellofs (slot): [op0+[op1]] = [src]
  // pointers
  //   pointer type encoded in bits 30-31:
  //     0: pointer to stack slot (from 0)
  //     1: pointer to global
  //     2: pointer to field (bits 0-13: field index; bits 14-29: actor index; so max field index is 16383)
  //     3: reserved
  PtrLoad, // dest, ptr, ofs
  PtrStore, // src, ptr, ofs
  // create pointers to various things
  MakeGPtr, // dest, slot: index -- make pointer to global
  MakeAPtr, // dest, slot: argnum -- make pointer to argument
  MakeLPtr, // dest, slot -- make pointer to local (to slot)
  MakeFPtr, // dest, actidslot, fldofs -- make pointer to field
  // subroutines
  Ret, // slot 0 is result
  Call, // vm call; addrslot; argslot; argcount
  //
  Invalid,
}
static assert(OpCode.Nop == 0);


align(1) struct VMOp {
align(1):
public:
  uint instr;

public nothrow @safe @nogc:
  this (uint ainstr) pure {
    pragma(inline, true);
    instr = ainstr;
  }

  this (OpCode opc, ubyte d=0, ubyte op0=0, ubyte op1=0) pure {
    pragma(inline, true);
    instr = (cast(uint)opc)|(d<<8)|(op0<<16)|(op1<<24);
  }

  void setS16 (short v) {
    pragma(inline, true);
    instr = (instr&0xffff)|((cast(uint)v)<<16);
  }

  void setU16 (ushort v) {
    pragma(inline, true);
    instr = (instr&0xffff)|((cast(uint)v)<<16);
  }

  static VMOp makeU16 (OpCode opc, ubyte d, ushort v) {
    pragma(inline, true);
    return VMOp((cast(uint)opc)|(d<<8)|((cast(uint)v)<<16));
  }

  static VMOp makeS16 (OpCode opc, ubyte d, short v) {
    pragma(inline, true);
    return VMOp((cast(uint)opc)|(d<<8)|((cast(uint)v)<<16));
  }

  static VMOp makeIVal (OpCode opc, int ival) pure nothrow @safe @nogc {
    pragma(inline, true);
    if (((ival>>24)&0xff) != 0 && ((ival>>24)&0xff) != 0xff) assert(0, "integer value out of range");
    return VMOp((cast(uint)opc)|(ival<<8));
  }

const pure:
  @property OpCode opcode () { pragma(inline, true); return ((instr&0xff) < OpCode.Invalid ? cast(OpCode)(instr&0xff) : OpCode.Invalid); }

  @property ubyte dest () { pragma(inline, true); return (instr>>8)&0xff; }
  @property ubyte src () { pragma(inline, true); return (instr>>8)&0xff; }
  @property ubyte op0 () { pragma(inline, true); return (instr>>16)&0xff; }
  @property ubyte op1 () { pragma(inline, true); return (instr>>24)&0xff; }
  @property int ival () { pragma(inline, true); return cast(int)((instr>>8)|(instr&0x8000_0000u ? 0xff00_0000u : 0)); }
  @property uint uval () { pragma(inline, true); return cast(uint)(instr>>8); }
  @property ushort u16 () { pragma(inline, true); return cast(ushort)(instr>>16); }
  @property short s16 () { pragma(inline, true); return cast(short)(instr>>16); }
}


string decodeSlot (int si) {
  import std.format : format;
  if (si < 0 || si > 255) return "R%d".format(si);
  for (Scope sc = curscope; sc !is null; sc = sc.parent) {
    foreach (Variable var; sc.vars) {
      if (!var.global && var.idx == si) return "[%s]".format(var.name);
    }
  }
  return "r%d".format(si);
}

string decodeArgSlot (int si) {
  import std.format : format;
  if (si < 0 || si > 255) return "R%d".format(si);
  int asi = -(si+1);
  for (Scope sc = curscope; sc !is null; sc = sc.parent) {
    foreach (Variable var; sc.vars) {
      if (!var.global && var.idx == asi) return "[%s]".format(var.name);
    }
  }
  return "r%d".format(si);
}


string decodeOpcode (int pc, VMOp v0, VMOp v1=VMOp(0)) {
  import std.format : format;
  auto opc = v0.opcode;
  if (opc == OpCode.Invalid) return "<invalid>";
  final switch (opc) {
    case OpCode.Nop: return "Nop (%d)".format(v0.ival);
    case OpCode.CheckStack: return "CheckStack (%d)".format(v0.u16);
    case OpCode.LitNU: return "LitNU %s,%u".format(decodeSlot(v0.dest), v1.uval);
    case OpCode.LitNS: return "LitNS %s,%d".format(decodeSlot(v0.dest), v1.ival);
    case OpCode.LitU16: return "LitU16 %s,%u".format(decodeSlot(v0.dest), v0.u16);
    case OpCode.LitS16: return "LitS16 %s,%d".format(decodeSlot(v0.dest), v0.s16);
    case OpCode.Mov:
    case OpCode.Inc:
    case OpCode.Dec:
      return "%s %s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.Add:
    case OpCode.Sub:
    case OpCode.Mul:
    case OpCode.Div:
    case OpCode.Mod:
    case OpCode.Shl:
    case OpCode.Shr:
    case OpCode.BitAnd:
    case OpCode.BitOr:
    case OpCode.BitXor:
    case OpCode.ILess:
    case OpCode.IGreat:
    case OpCode.ILessEqu:
    case OpCode.IGreatEqu:
    case OpCode.SLess:
    case OpCode.SGreat:
    case OpCode.SLessEqu:
    case OpCode.SGreatEqu:
    case OpCode.Equ:
    case OpCode.NotEqu:
    case OpCode.LogAnd:
    case OpCode.LogOr:
      return "%s %s,%s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.IntAbs:
      return "%s %s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.Jump: // s16: offset (from next opcode)
      return "Jump %04X".format(pc+1+v0.s16);
    case OpCode.JTrue: // src: slot; s16: offset (from next opcode)
    case OpCode.JFalse: // src: slot; s16: offset (from next opcode)
      return "%s %s,%04X".format(opc, decodeSlot(v0.src), pc+1+v0.s16);
    case OpCode.LogNot: return "%s %s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.LogBNorm: return "%s %s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.GLoad: return "GLoad %s,[%u]".format(decodeSlot(v0.dest), v0.u16);
    case OpCode.GStore: return "GStore %s,[%u]".format(decodeSlot(v0.src), v0.u16);
    case OpCode.ALoad: return "ALoad %s,%s".format(decodeSlot(v0.dest), v0.op0);
    case OpCode.AStore: return "AStore %s,%s".format(decodeArgSlot(v0.dest), v0.op0);
    case OpCode.ACount: return "ACount %s".format(decodeSlot(v0.dest));
    case OpCode.FLoad: return "FLoad %s,%s,%s".format(decodeSlot(v0.dest), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.FStore: return "FStore %s,%s,%s".format(decodeSlot(v0.src), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.StrLen: return "StrLen %s,%s".format(decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.StrLoadChar: return "StrLoadChar %s,%s,%s".format(decodeSlot(v0.dest), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.Bounds: return "Bounds %s,%u".format(decodeSlot(v0.dest), v0.u16);
    case OpCode.LArrClear: return "LArrClear %s,%u".format(decodeSlot(v0.dest), v0.u16);
    case OpCode.GArrLoad:
    case OpCode.GArrStore:
    case OpCode.LArrLoad:
    case OpCode.LArrStore:
    case OpCode.PtrLoad:
    case OpCode.PtrStore:
      return "%s %s,%s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.MakeGPtr:
    case OpCode.MakeAPtr:
    case OpCode.MakeLPtr:
      return "%s %s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0));
    case OpCode.MakeFPtr:
      return "%s %s,%s,%s".format(opc, decodeSlot(v0.dest), decodeSlot(v0.op0), decodeSlot(v0.op1));
    case OpCode.Ret: return "Ret";
    case OpCode.Call: return "Call %s,%s,%u".format(decodeSlot(v0.src), decodeSlot(v0.op0), v0.op1);
    case OpCode.Invalid: return "<invalid>";
  }
  assert(0);
}


// ////////////////////////////////////////////////////////////////////////// //
struct BuiltinDg {
  string name;
  int delegate (in ref BuiltinDg bdg, int argslot, int argc) handler;
  TypeFunc type; // so we can finish builtin registration later
}

__gshared BuiltinDg[] builtinlist;


import std.traits;


TypeFunc buildFuncType(bool allowvararg, bool abortonerror, RT, A...) () {
  auto tf = new TypeFunc();
       static if (is(RT == void)) tf.aux = typeVoid;
  else static if (is(Unqual!RT == int)) tf.aux = typeInt;
  else static if (is(Unqual!RT == bool)) tf.aux = typeBool;
  else static if (is(RT == ActorId)) tf.aux = typeActor;
  else static if (is(Unqual!RT == enum)) {
    bool error = true;
    enum enumname = (Unqual!RT).stringof;
    if (auto etp = enumname in gtypes) {
      if (auto tp = cast(TypeEnum)(*etp)) {
        if (tp.name == enumname) {
          tf.aux = tp;
          error = false;
        }
      }
    }
    if (error) {
      //static if (abortonerror) static assert(0, "invalid builtin return type: "~enumname); else return null;
      if (abortonerror) assert(0, "invalid builtin return type: "~enumname); else return null;
    }
  } else { static if (abortonerror) static assert(0, "invalid builtin return type: "~RT.stringof); else return null; }
  foreach (immutable idx, immutable ptype; A) {
         static if (is(Unqual!ptype == int)) tf.args ~= TypeFunc.Arg(typeInt);
    else static if (is(Unqual!ptype == bool)) tf.args ~= TypeFunc.Arg(typeBool);
    else static if (is(ptype == ActorId)) tf.args ~= TypeFunc.Arg(typeActor);
    else static if (is(ptype == string) || is(ptype == const(char)[])) tf.args ~= TypeFunc.Arg(typeStr);
    else static if (is(Unqual!ptype == enum)) {
      bool error = true;
      enum enumname = (Unqual!ptype).stringof;
      if (auto etp = enumname in gtypes) {
        if (auto tp = cast(TypeEnum)(*etp)) {
          if (tp.name == enumname) {
            tf.args ~= TypeFunc.Arg(tp);
            error = false;
          }
        }
      }
      if (error) {
        static if (abortonerror) assert(0, "invalid argument type: "~enumname); else return null;
      }
    } else static if (allowvararg && is(ptype == const(int)[])) {
      static if (idx != A.length-1) { static if (abortonerror) static assert(0, "'vargs' must be last"); else return null; }
      tf.hasrest = true;
    } else {
      static if (abortonerror) static assert(0, "invalid argument type: "~ptype.stringof); else return null;
    }
  }
  return tf;
}


TypeFunc buildFuncTypeFromDg(DG, bool allowvararg=false, bool abortonerror=true) (scope DG dg=null) if (isCallable!DG) {
  return buildFuncType!(allowvararg, abortonerror, ReturnType!DG, Parameters!DG);
}


// fix builtin index (create new empty builtin if necessary)
private int fixBuiltin (string fullname, TypeFunc ft) {
  assert(ft !is null);
  assert(ft.isFunc);
  assert(fullname.length);
  if (builtinlist.length == 0) {
    // dummy builtin
    builtinlist.length = 1;
  } else {
    // find existing builtin
    foreach (immutable bidx, ref BuiltinDg bdg; builtinlist) {
      //stderr.writeln("looking for '", fullname, "'; bdg=", bdg);
      if (fullname != bdg.name) continue;
      if (bdg.type != ft) continue;
      // i found her!
      return -cast(int)bidx;
    }
  }
  // create new one
  if (builtinlist.length > short.max) assert(0, "too many builtins");
  int bidx = cast(int)builtinlist.length;
  builtinlist.length += 1;
  builtinlist[bidx].name = fullname;
  builtinlist[bidx].type = ft;
  builtinlist[bidx].handler = delegate (in ref BuiltinDg bdg, int argslot, int argc) {
    throw new Exception("builtin '"~bdg.name~"' is not defined");
  };
  return -bidx;
}


// no string returns yet (we need string GC)
public void mesRegisterBuiltin(T:const(char)[], DG) (T name, DG dg) if (isCallable!DG) {
  assert(dg !is null);

  int fnidx = -1;
  auto tf = buildFuncTypeFromDg!(DG, true, true);
  alias RT = ReturnType!DG;
  alias pars = Parameters!DG;

  if (auto fp = name in funclist) {
    // find function overload
    foreach (/*immutable ovidx,*/ ref fni; fp.ovloads) {
      TypeFunc ovt = fni.type;
      if (ovt == tf) {
        int idx = fni.pc;
        if (idx >= 0) throw new Exception("cannot overload non-builtin '"~name.idup~"'");
        idx = -idx;
        assert(idx >= 0 && idx < builtinlist.length);
        fnidx = idx;
        break;
      }
    }
  }

  // if nothing was found, this in "inb4 builtin"
  if (fnidx == -1) {
    // create new builtin record
    if (builtinlist.length > short.max) assert(0, "too many builtins");
    fnidx = cast(int)builtinlist.length;
    builtinlist.length += 1;
    static if (is(T == string)) builtinlist[fnidx].name = name; else builtinlist[fnidx].name = name.idup;
    builtinlist[fnidx].type = tf;
  }

  builtinlist[fnidx].handler = delegate (in ref BuiltinDg bdg, int argslot, int argc) {
    pars args;
    foreach (immutable idx, immutable ptype; pars) {
      static if (is(Unqual!ptype == int)) {
        args[idx] = (idx < argc ? vmstack[argslot+argc-idx-1] : 0);
      } else static if (is(Unqual!ptype == bool)) {
        args[idx] = (idx < argc ? (vmstack[argslot+argc-idx-1] != 0) : false);
      } else static if (is(ptype == ActorId)) {
        args[idx] = ActorId(idx < argc ? vmstack[argslot+argc-idx-1] : 0);
      } else static if (is(ptype == string) || is(ptype == const(char)[])) {
        args[idx] = (idx < argc ? strlist[vmstack[argslot+argc-idx-1]] : null);
      } else static if (is(Unqual!ptype == enum)) {
        args[idx] = (idx < argc ? cast(ptype)vmstack[argslot+argc-idx-1] : ptype.min);
      } else static if (is(ptype == const(int)[])) {
        static assert(idx == pars.length-1, "'vargs' must be last");
        args[idx] = vmstack[argslot..argslot+argc];
      } else {
        static assert(0, "invalid argument type: "~ptype.stringof);
      }
    }
    static if (is(RT == void)) {
      dg(args);
      return 0;
    } else {
      auto res = dg(args);
           static if (is(Unqual!RT == int)) return res;
      else static if (is(Unqual!RT == bool)) return (res ? 1 : 0);
      else static if (is(RT == ActorId)) return res.id;
      else static if (is(Unqual!RT == enum)) {
        return cast(RT)res;
      } else static assert(0, "invalid builtin return type: "~RT.stringof);
    }
  };
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared Token[] cursource; // first pass will collect all tokens here
__gshared int curtokidx;
__gshared Token emptytoken;

ref const(Token) token () nothrow @trusted @nogc { pragma(inline, true); return (curtokidx >= 0 && curtokidx < cursource.length ? cursource.ptr[curtokidx] : emptytoken); }
void skipToken () nothrow @trusted @nogc { if (curtokidx < cursource.length) ++curtokidx; }

void forgetSource () {
  delete cursource;
  curtokidx = 0;
}


public void compileError (string msg, string file=__FILE__, usize line=__LINE__) {
  if (cursource.length == 0) throw new CompilerError(Loc.init, msg, file, line);
  if (curtokidx >= 0 && curtokidx < cursource.length) throw new CompilerError(token.loc, msg, file, line);
  throw new CompilerError(cursource[$-1].loc, msg, file, line);
  assert(0);
}

public void compileErrorAt() (in auto ref Loc aloc, string msg, string file=__FILE__, usize line=__LINE__) {
  throw new CompilerError(aloc, msg, file, line);
  assert(0);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int[] vmglobals;
__gshared VMOp[] vmcode;
__gshared int[65536] vmstack;
__gshared int vmbp, vmargc;
struct RStackItem {
  uint pc; // uint.max: no more
  int bp, argc; // top arg will contain result
}
__gshared RStackItem[32768] vmrstack;
__gshared int vmrsp;
// vmbp points to the first slot *after* all args; this is `result` slot


// ////////////////////////////////////////////////////////////////////////// //
__gshared TypeBase[] vatypes;

int putVAType (TypeBase t) {
  assert(t !is null);
  if (vatypes.length == 0) vatypes.reserve(128);
  foreach (immutable idx, TypeBase vt; vatypes) if (vt is t) return cast(int)idx;
  vatypes ~= t;
  return cast(int)vatypes.length-1;
}


// can return `null`
public int mesVAGetArgCount (const(int)[] vargs) nothrow @trusted @nogc { pragma(inline, true); return cast(int)vargs.length/2; }


public TypeBase mesVAGetArgType (const(int)[] vargs, int aidx) nothrow @trusted @nogc {
  if (aidx < 0 || aidx >= vargs.length/2) return null;
  int t = vargs[$-aidx*2-1];
  return (t >= 0 && t < vatypes.length ? vatypes[t] : null);
}


public VT mesVAGetArgValue(VT) (const(int)[] vargs, int aidx) nothrow @trusted
if (is(VT == bool) || is(VT == int) || is(VT == string) || is(VT == enum) || is(VT == ActorId))
{
  static if (is(VT == enum)) enum DefVal = VT.min; else enum DefVal = VT.init;
  if (aidx < 0 || aidx >= vargs.length/2) return DefVal;
  // type
  int t = vargs[$-aidx*2-1];
  if (t < 0 || t >= vatypes.length) return DefVal;
  TypeBase tp = vatypes[t];
  if (tp.isVoid) return VT.init;
  // value
  int v = vargs[$-aidx*2-2];
  // convert
  if (tp.isBool) {
         static if (is(VT == bool)) return (v != 0);
    else static if (is(VT == int)) return v;
    else static if (is(VT == string)) return (v ? "true" : "false");
    else static if (is(VT == enum)) return VT.min;
    else static if (is(VT == ActorId)) return ActorId.init;
    else static assert(0, "wtf?!");
  } else if (tp.isInt) {
         static if (is(VT == bool)) return (v != 0);
    else static if (is(VT == int)) return v;
    else static if (is(VT == string)) { import std.conv; return v.to!string; }
    else static if (is(VT == enum)) {
      foreach (string mname; __traits(allMembers, VT)) {
        if (v == __traits(getMember, VT, mname)) return __traits(getMember, VT, mname);
      }
    } else static if (is(VT == ActorId)) return ActorId.init;
    else static assert(0, "wtf?!");
  } else if (tp.isStr) {
         static if (is(VT == bool)) return (v >= 0 && v < strlist.length ? (strlist[v].length != 0) : false);
    else static if (is(VT == int)) return v;
    else static if (is(VT == string)) return (v >= 0 && v < strlist.length ? strlist[v] : null);
    else static if (is(VT == enum)) {
      if (v >= 0 && v < strlist.length) {
        string s = strlist[v];
        foreach (string mname; __traits(allMembers, VT)) {
          if (s == __traits(getMember, VT, mname)) return __traits(getMember, VT, mname);
        }
      }
      return VT.min;
    } else static if (is(VT == ActorId)) return ActorId.init;
    else static assert(0, "wtf?!");
  } else if (tp.isActor) {
         static if (is(VT == bool)) return ActorId(v).alive;
    else static if (is(VT == int)) return v;
    else static if (is(VT == string)) return "Actor";
    else static if (is(VT == enum)) return VT.min;
    else static if (is(VT == ActorId)) return ActorId(v);
    else static assert(0, "wtf?!");
  } else if (tp.isNull) {
         static if (is(VT == bool)) return false;
    else static if (is(VT == int)) return 0;
    else static if (is(VT == string)) return "null";
    else static if (is(VT == enum)) return VT.min;
    else static if (is(VT == ActorId)) return ActorId.init;
    else static assert(0, "wtf?!");
  } else if (tp.isEnum) {
         static if (is(VT == bool)) return (v != 0);
    else static if (is(VT == int)) return v;
    else static if (is(VT == string)) {
      auto et = cast(TypeEnum)tp;
      foreach (const ref kv; et.members.byKeyValue) if (kv.value == v) return kv.key;
      return "???";
    } else static if (is(VT == enum)) {
      auto et = cast(TypeEnum)tp;
      if (et.name == (Unqual!VT).stringof) {
        foreach (const ref kv; et.members.byKeyValue) {
          if (kv.value == v) {
            foreach (string mname; __traits(allMembers, VT)) {
              if (kv.key == __traits(getMember, VT, mname)) return __traits(getMember, VT, mname);
            }
            return VT.min;
          }
        }
      }
      return VT.min;
    } else static if (is(VT == ActorId)) return ActorId.init;
    else static assert(0, "wtf?!");
  }
  return DefVal;
}


// ////////////////////////////////////////////////////////////////////////// //
public int vmExec(A...) (uint pc, A args) {
  if (vmbp >= vmstack.length || vmstack.length-vmbp < args.length+1) throw new Exception("out of vm stack for args");
  auto bp = vmbp;
  foreach_reverse (immutable idx, ref arg; args) {
    alias ptype = typeof(arg);
    static if (is(ptype == byte) || is(ptype == ubyte) ||
               is(ptype == short) || is(ptype == ushort) ||
               is(ptype == int))
    {
      vmstack[bp++] = arg;
    } else static if (is(ptype == bool)) {
      vmstack[bp++] = (arg ? 1 : 0);
    } else static if (is(ptype == ActorId)) {
      vmstack[bp++] = arg.id;
    } else static if (is(ptype : const(char)[])) {
      static assert(0, "no string args yet");
    } else static if (is(Unqual!ptype == enum)) {
      bool error = true;
      enum enumname = (Unqual!ptype).stringof;
      if (auto etp = enumname in gtypes) {
        if (auto tp = cast(TypeEnum)(*etp)) {
          if (tp.name == enumname) {
            if (cast(int)arg >= ptype.min && cast(int)arg <= ptype.max) {
              vmstack[bp++] = cast(int)arg;
              error = false;
            }
          }
        }
      }
      if (error) throw new Exception("unknown enum '"~enumname~"'");
    } else {
      static assert(0, "invalid argument type: "~ptype.stringof);
    }
  }
  auto oldargc = vmargc;
  auto oldbp = vmbp;
  scope(exit) { vmbp = oldbp; vmargc = oldargc; }
  vmargc = cast(int)args.length;
  vmbp += vmargc;
  return vmExecIntr(pc);
}


private int vmExecIntr (uint pc) {
  if (vmrsp >= vmrstack.length-2) assert(0, "internal vm error");
  auto oldrsp = vmrsp;
  vmrstack.ptr[vmrsp++] = RStackItem(uint.max, vmbp, vmargc);
  scope(exit) {
    //stderr.writeln("vmrsp=", vmrsp, "; oldrsp=", oldrsp);
    if (vmrsp < oldrsp) assert(0, "internal vm error");
    if (vmrsp > oldrsp) {
      vmrsp = oldrsp;
      vmbp = vmrstack.ptr[vmrsp+1].bp;
      vmargc = vmrstack.ptr[vmrsp+1].argc;
    }
  }
  if (vmbp >= vmstack.length || vmstack.length-vmbp < 256+3) throw new Exception("vm stack overflow");
  //vmargc = 0;
  while (pc < vmcode.length) {
    auto op = vmcode.ptr[pc++];
    version(mesdbg_dump_dump_execution) {
      if (!MESDisableDumps) stderr.writefln("%04X: %s  bp=%d; argc=%d", pc-1, decodeOpcode(pc-1, op), vmbp, vmargc);
    }
    final switch (op.opcode) {
      case OpCode.Nop: break;
      case OpCode.CheckStack:
        if (vmbp >= vmstack.length || vmstack.length-vmbp < op.u16) throw new Exception("vm stack overflow");
        break;
      case OpCode.LitNU: // next Nop is value
        if (pc >= vmcode.length || vmcode.ptr[pc].opcode != OpCode.Nop) assert(0, "internal vm error");
        vmstack.ptr[vmbp+op.dest] = vmcode.ptr[pc].uval;
        ++pc;
        break;
      case OpCode.LitNS: // next Nop is value
        if (pc >= vmcode.length || vmcode.ptr[pc].opcode != OpCode.Nop) assert(0, "internal vm error");
        vmstack.ptr[vmbp+op.dest] = vmcode.ptr[pc].ival;
        ++pc;
        break;
      case OpCode.LitU16: vmstack.ptr[vmbp+op.dest] = op.u16; break;
      case OpCode.LitS16: vmstack.ptr[vmbp+op.dest] = op.s16; break;
      case OpCode.Mov: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]; break;
      case OpCode.Inc: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]+1; break;
      case OpCode.Dec: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]-1; break;
      case OpCode.Add: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]+vmstack.ptr[vmbp+op.op1]; break;
      case OpCode.Sub: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]-vmstack.ptr[vmbp+op.op1]; break;
      case OpCode.Mul: vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]*vmstack.ptr[vmbp+op.op1]; break;
      case OpCode.Div:
        if (vmstack.ptr[vmbp+op.op1] == 0) throw new Exception("division by zero");
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]/vmstack.ptr[vmbp+op.op1];
        break;
      case OpCode.Mod:
        if (vmstack.ptr[vmbp+op.op1] == 0) throw new Exception("division by zero");
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]%vmstack.ptr[vmbp+op.op1];
        break;
      case OpCode.Shl:
        int sft = vmstack.ptr[vmbp+op.op1];
        if (sft < 0 || sft > 31) throw new Exception("invalid shift range");
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]<<sft;
        break;
      case OpCode.Shr:
        int sft = vmstack.ptr[vmbp+op.op1];
        if (sft < 0 || sft > 31) throw new Exception("invalid shift range");
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]>>sft;
        break;
      case OpCode.BitAnd:
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]&vmstack.ptr[vmbp+op.op1];
        break;
      case OpCode.BitOr:
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]|vmstack.ptr[vmbp+op.op1];
        break;
      case OpCode.BitXor:
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp+op.op0]^vmstack.ptr[vmbp+op.op1];
        break;
      case OpCode.ILess:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] < vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.IGreat:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] > vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.ILessEqu:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] <= vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.IGreatEqu:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] >= vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.SLess:
        vmstack.ptr[vmbp+op.dest] = (strlist.ptr[vmstack.ptr[vmbp+op.op0]] < strlist.ptr[vmstack.ptr[vmbp+op.op1]] ? 1 : 0);
        break;
      case OpCode.SGreat:
        vmstack.ptr[vmbp+op.dest] = (strlist.ptr[vmstack.ptr[vmbp+op.op0]] > strlist.ptr[vmstack.ptr[vmbp+op.op1]] ? 1 : 0);
        break;
      case OpCode.SLessEqu:
        vmstack.ptr[vmbp+op.dest] = (strlist.ptr[vmstack.ptr[vmbp+op.op0]] <= strlist.ptr[vmstack.ptr[vmbp+op.op1]] ? 1 : 0);
        break;
      case OpCode.SGreatEqu:
        vmstack.ptr[vmbp+op.dest] = (strlist.ptr[vmstack.ptr[vmbp+op.op0]] >= strlist.ptr[vmstack.ptr[vmbp+op.op1]] ? 1 : 0);
        break;
      case OpCode.Equ:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] == vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        //version(mesdbg_dump_dump_execution) stderr.writefln("  dest=%u; op0=%u; op1=%u; [%u]=%u", op.dest, op.op0, op.op1, op.dest, vmstack.ptr[vmbp+op.dest]);
        break;
      case OpCode.NotEqu:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] != vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.LogAnd:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] && vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.LogOr:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] || vmstack.ptr[vmbp+op.op1] ? 1 : 0);
        break;
      case OpCode.IntAbs:
        int v = vmstack.ptr[vmbp+op.op0];
        if (v == 0x8000_0000) throw new Exception("cannot make positive integer number");
        vmstack.ptr[vmbp+op.dest] = (v < 0 ? -v : v);
        break;
      case OpCode.Jump: // s16: offset (from next opcode)
        pc += op.s16;
        break;
      case OpCode.JTrue: // src: slot; s16: offset (from next opcode)
        if (vmstack.ptr[vmbp+op.src]) pc += op.s16;
        break;
      case OpCode.JFalse: // src: slot; s16: offset (from next opcode)
        //version(mesdbg_dump_dump_execution) stderr.writefln("  [%u]=%d", op.src, vmstack.ptr[vmbp+op.src]);
        if (!vmstack.ptr[vmbp+op.src]) pc += op.s16;
        break;
      case OpCode.LogNot:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] ? 0 : 1);
        break;
      case OpCode.LogBNorm:
        vmstack.ptr[vmbp+op.dest] = (vmstack.ptr[vmbp+op.op0] ? 1 : 0);
        break;
      case OpCode.GLoad: // dest, 2 bytes: index (offset in global area)
        vmstack.ptr[vmbp+op.dest] = vmglobals.ptr[op.u16];
        break;
      case OpCode.GStore: // src, 2 bytes: index (offset in global area)
        vmglobals.ptr[op.u16] = vmstack.ptr[vmbp+op.src];
        break;
      case OpCode.ALoad:
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[vmbp-1-op.op0];
        break;
      case OpCode.AStore:
        vmstack.ptr[vmbp-1-op.dest] = vmstack.ptr[vmbp+op.op0];
        break;
      case OpCode.ACount:
        vmstack.ptr[vmbp+op.dest] = vmargc;
        break;
      case OpCode.FLoad: // dest (slot), actid, fldidx -- load field value to dest
        auto aid = ActorId(cast(uint)vmstack.ptr[vmbp+op.op0]);
        if (aid.alive) {
          //vmstack.ptr[vmbp+op.dest] = actors[aid.id&0xffff][vmstack.ptr[vmbp+op.op1]];
          vmstack.ptr[vmbp+op.dest] = actorGetById(aid).ptr[vmstack.ptr[vmbp+op.op1]];
        } else {
          // it is not a bug to access dead actor
          vmstack.ptr[vmbp+op.dest] = 0;
        }
        break;
      case OpCode.FStore: // src (slot), actid, fldidx -- store field value from src
        auto aid = ActorId(cast(uint)vmstack.ptr[vmbp+op.op0]);
        // it is not a bug to access dead actor; just do nothing
        if (aid.alive) {
          actorGetById(aid).ptr[vmstack.ptr[vmbp+op.op1]] = vmstack.ptr[vmbp+op.src];
        }
        break;
      case OpCode.StrLen: // dest, src (string idx)
        int sidx = vmstack.ptr[vmbp+op.op0];
        if (sidx >= 0 && sidx < strlist.length) {
          vmstack.ptr[vmbp+op.dest] = cast(int)strlist.ptr[sidx].length;
        } else {
          vmstack.ptr[vmbp+op.dest] = 0;
        }
        break;
      case OpCode.StrLoadChar: // dest, src (string idx), strofs
        int sidx = vmstack.ptr[vmbp+op.op0];
        if (sidx >= 0 && sidx < strlist.length) {
          string s = strlist.ptr[sidx];
          int sofs = vmstack.ptr[vmbp+op.op1];
          vmstack.ptr[vmbp+op.dest] = (sofs >= 0 && sofs < s.length ? cast(int)(s.ptr[sofs]) : 0);
        } else {
          vmstack.ptr[vmbp+op.dest] = 0;
        }
        break;
      case OpCode.Bounds: // ofs (slot), u16: maxidx
        int aofs = vmstack.ptr[vmbp+op.src];
        if (aofs < 0 || aofs >= op.u16) {
          import std.conv : to;
          throw new Exception("array access out of bounds ("~aofs.to!string~")");
        }
        break;
      case OpCode.LArrClear: // dest is first slot; u16 is cell count
        if (op.u16 > 0) {
          if (vmbp+op.dest+op.u16 > vmstack.length) assert(0, "internal codegen error");
          vmstack.ptr[vmbp+op.dest..vmbp+op.dest+op.u16] = 0;
        }
        break;
      case OpCode.GArrLoad: // dest, globofs (slot), cellofs (slot): [dest] = gvars[[op0]+[op1]]
        int abase = vmstack.ptr[vmbp+op.op0];
        int aofs = vmstack.ptr[vmbp+op.op1];
        version(mes_paranoid_arrays) {
          if (aofs < 0 || aofs >= vmglobals.length || abase < 0 || abase >= vmglobals.length || aofs+abase >= vmglobals.length) {
            throw new Exception("array access out of bounds");
          }
        }
        vmstack.ptr[vmbp+op.dest] = vmglobals.ptr[abase+aofs];
        break;
      case OpCode.GArrStore: // src, globofs (slot), cellofs (slot): gvars[[op0]+[op1]] = [src]
        int abase = vmstack.ptr[vmbp+op.op0];
        int aofs = vmstack.ptr[vmbp+op.op1];
        version(mes_paranoid_arrays) {
          if (aofs < 0 || aofs >= vmglobals.length || abase < 0 || abase >= vmglobals.length || aofs+abase >= vmglobals.length) {
            throw new Exception("array access out of bounds");
          }
        }
        vmglobals.ptr[abase+aofs] = vmstack.ptr[vmbp+op.src];
        break;
      case OpCode.LArrLoad: // dest, bpofs (slot), cellofs (slot): [dest] = [op0+[op1]]
        int abase = vmbp+op.op0;
        int aofs = vmstack.ptr[vmbp+op.op1];
        version(mes_paranoid_arrays) {
          if (abase < 0 || abase >= vmstack.length || aofs < 0 || aofs >= vmstack.length || abase+aofs < 0 || abase+aofs >= vmstack.length) {
            throw new Exception("array access out of bounds");
          }
        }
        vmstack.ptr[vmbp+op.dest] = vmstack.ptr[abase+aofs];
        break;
      case OpCode.LArrStore: // src, bpofs (slot), cellofs (slot): [op0+[op1]] = [src]
        int abase = vmbp+op.op0;
        int aofs = vmstack.ptr[vmbp+op.op1];
        version(mes_paranoid_arrays) {
          if (abase < 0 || abase >= vmstack.length || aofs < 0 || aofs >= vmstack.length || abase+aofs < 0 || abase+aofs >= vmstack.length) {
            throw new Exception("array access out of bounds");
          }
        }
        vmstack.ptr[abase+aofs] = vmstack.ptr[vmbp+op.src];
        break;
      //   pointer type encoded in bits 30-31:
      //     0: pointer to stack slot (from 0)
      //     1: pointer to global
      //     2: pointer to field (bits 0-13: field index; bits 14-29: actor index; so max field index is 16383)
      //     3: reserved
      //FIXME: REMOVE PASTA!
      case OpCode.PtrLoad: // dest, ptr, ofs
        int ofs = vmstack.ptr[vmbp+op.op1];
        if (ofs < short.min || ofs > short.max) assert(0, "internal compiler error");
        uint pval = cast(uint)vmstack.ptr[vmbp+op.op0];
        //stderr.writefln("pval=0x%08x; pv=%u; ofs=%d", pval, pval&0x3fff_ffff, ofs);
        int* xptr = null;
        final switch (pval>>30) {
          case 0: // pointer to stack slot
            if (pval >= vmstack.length || pval+ofs >= vmstack.length) assert(0, "internal compiler error");
            xptr = vmstack.ptr+pval+ofs;
            break;
          case 1:
            pval &= 0x3fff_ffff;
            if (pval+ofs >= vmglobals.length) assert(0, "internal compiler error");
            xptr = vmglobals.ptr+pval+ofs;
            break;
          case 2: // pointer to field (bits 0-13: field index; bits 14-29: actor index; so max field index is 16383)
            int aidx = (pval>>14)&0xffff;
            auto act = actorGetByIndex(aidx);
            if (act is null) assert(0, "internal compiler error");
            int fidx = (pval&0x3fff)+ofs;
            if (fidx < 0 || fidx >= act.length) assert(0, "internal compiler error");
            xptr = act.ptr+fidx;
            break;
          case 3: assert(0, "internal compiler error");
        }
        vmstack.ptr[vmbp+op.dest] = *xptr;
        break;
      case OpCode.PtrStore: // src, ptr, ofs
        int ofs = vmstack.ptr[vmbp+op.op1];
        if (ofs < short.min || ofs > short.max) assert(0, "internal compiler error");
        uint pval = cast(uint)vmstack.ptr[vmbp+op.op0];
        int* xptr = null;
        final switch (pval>>30) {
          case 0: // pointer to stack slot
            if (pval >= vmstack.length || pval+ofs >= vmstack.length) assert(0, "internal compiler error");
            xptr = vmstack.ptr+pval+ofs;
            break;
          case 1:
            pval &= 0x3fff_ffff;
            if (pval+ofs >= vmglobals.length) assert(0, "internal compiler error");
            xptr = vmglobals.ptr+pval+ofs;
            break;
          case 2: // pointer to field (bits 0-13: field index; bits 14-29: actor index; so max field index is 16383)
            int aidx = (pval>>14)&0xffff;
            auto act = actorGetByIndex(aidx);
            if (act is null) assert(0, "internal compiler error");
            int fidx = (pval&0x3fff)+ofs;
            if (fidx < 0 || fidx >= act.length) assert(0, "internal compiler error");
            xptr = act.ptr+fidx;
            break;
          case 3: assert(0, "internal compiler error");
        }
        *xptr = vmstack.ptr[vmbp+op.src];
        break;
      case OpCode.MakeGPtr: // dest, slot: index -- make pointer to global
        int gidx = vmstack.ptr[vmbp+op.op0];
        if (gidx < 0 || gidx >= vmglobals.length) assert(0, "internal compiler error");
        vmstack.ptr[vmbp+op.dest] = gidx|0x4000_0000;
        break;
      case OpCode.MakeAPtr: // dest, slot: argnum -- make pointer to argument
        assert(0);
      case OpCode.MakeLPtr: // dest, slot -- make pointer to local (to slot)
        int gidx = vmbp+op.op0;
        if (gidx < 0 || gidx >= vmstack.length) assert(0, "internal compiler error");
        vmstack.ptr[vmbp+op.dest] = gidx;
        break;
      case OpCode.MakeFPtr: // dest, actidslot, fldofs -- make pointer to field
        auto aid = ActorId(vmstack.ptr[vmbp+op.op0]);
        if (!aid.alive) throw new Exception("cannot create pointer to dead object");
        int fidx = vmstack.ptr[vmbp+op.op1];
        if (fidx < 0 || fidx > 0x3fff) assert(0, "internal compiler error");
        vmstack.ptr[vmbp+op.dest] = ((aid.id&0xffff)<<14)|fidx|0x8000_0000;
        break;
      case OpCode.Ret:
        // function result is in r0; copy it to the top argument slot, if there is any
        if (vmrsp == oldrsp) assert(0, "wtf?!");
        immutable int res = vmstack.ptr[vmbp];
        if (vmargc) vmstack.ptr[vmbp-vmargc] = res;
        --vmrsp;
        vmbp = vmrstack.ptr[vmrsp].bp;
        vmargc = vmrstack.ptr[vmrsp].argc;
        if (vmrstack.ptr[vmrsp].pc == uint.max) return res;
        pc = vmrstack.ptr[vmrsp].pc;
        break;
      case OpCode.Call: // addrslot; argslot; argcount
        if (vmrsp+1 > vmrstack.length) throw new Exception("vm call stack overflow");
        int newpc = cast(int)vmstack.ptr[vmbp+op.src];
        // check function type
        if (newpc < 0) {
          // builtin
          newpc = -newpc;
          assert(newpc > 0);
          if (newpc >= builtinlist.length) assert(0, "internal vm error");
          vmstack.ptr[vmbp+op.op0] = builtinlist.ptr[newpc].handler(builtinlist.ptr[newpc], vmbp+op.op0, op.op1);
        } else if (newpc > 0) {
          // normal
          vmrstack.ptr[vmrsp++] = RStackItem(pc, vmbp, vmargc);
          pc = vmstack.ptr[vmbp+op.src];
          vmargc = op.op1;
          vmbp += op.op0+vmargc;
        } else {
          throw new Exception("cannot call null function");
        }
        break;
      case OpCode.Invalid:
        assert(0, "internal vm error");
    }
  }
  assert(0, "internal vm error");
}


// ////////////////////////////////////////////////////////////////////////// //
//mesdbg_dump_generated_code
void emitCode (VMOp opc, size_t line=__LINE__) {
  vmcode ~= opc;
  version(mesdbg_dump_generated_code) {
    if (!MESDisableDumps) {
      stderr.writefln("%04X: %s   (%u)  %s", cast(uint)vmcode.length-1, decodeOpcode(cast(int)vmcode.length-1, opc), line, token.loc.toString);
    }
  }
}

void emitILit (ubyte dest, int ival) {
  if (ival >= 0 && ival <= ushort.max) {
    vmcode ~= VMOp((OpCode.LitU16)|(dest<<8)|(ival<<16));
    version(mesdbg_dump_generated_code) {
      if (!MESDisableDumps) stderr.writefln("%04X: %s", cast(uint)vmcode.length-1, decodeOpcode(cast(int)vmcode.length-1, vmcode[$-1]));
    }
    return;
  }
  if (ival >= short.min && ival <= short.max) {
    vmcode ~= VMOp((OpCode.LitS16)|(dest<<8)|(ival<<16));
    version(mesdbg_dump_generated_code) {
      if (!MESDisableDumps) stderr.writefln("%04X: %s", cast(uint)vmcode.length-1, decodeOpcode(cast(int)vmcode.length-1, vmcode[$-1]));
    }
    return;
  }
  // fits into 3 unsigned bytes?
  if (ival >= 0 && ival <= 0x00ff_ffff) {
    vmcode ~= VMOp((OpCode.LitNU)|(dest<<8));
  } else {
    // fits into 3 signed bytes?
    if (((ival>>24)&0xff) != 0 && ((ival>>24)&0xff) != 0xff) assert(0, "integer value out of range");
    if ((ival>>23) != 0 && (ival>>23) != -1) assert(0, "integer value out of range");
    vmcode ~= VMOp((OpCode.LitNS)|(dest<<8));
  }
  vmcode ~= VMOp((OpCode.Nop)|(ival<<8));
  version(mesdbg_dump_generated_code) {
    if (!MESDisableDumps) stderr.writefln("%04X: %s", cast(uint)vmcode.length-2, decodeOpcode(cast(int)vmcode.length-2, vmcode[$-2]), decodeOpcode(cast(int)vmcode.length-1, vmcode[$-1]));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// TYPE DEFINITIONS
//
public class TypeBase {
public:
  this () {}

  @property bool isVoid () const nothrow @safe @nogc { return false; }
  @property bool isBool () const nothrow @safe @nogc { return false; }
  @property bool isInt () const nothrow @safe @nogc { return false; }
  @property bool isStr () const nothrow @safe @nogc { return false; }
  @property bool isArray () const nothrow @safe @nogc { return false; }
  @property bool isStruct () const nothrow @safe @nogc { return false; }
  @property bool isActor () const nothrow @safe @nogc { return false; }
  @property bool isEnum () const nothrow @safe @nogc { return false; }
  @property bool isFunc () const nothrow @safe @nogc { return false; }
  @property bool isMethod () const nothrow @safe @nogc { return false; }
  @property bool isPointer () const nothrow @safe @nogc { return false; }
  @property bool isFuncPtr () const nothrow @safe @nogc { return false; }
  @property bool isNull () const nothrow @safe @nogc { return false; }
  @property bool isCallable () const nothrow @safe @nogc { return false; }
  @property uint cellSize () const nothrow @safe @nogc { return 0; } // size in cells
  @property TypeBase base () nothrow @safe @nogc { return this; } // for arrays, structs and pointers

  abstract string toPrettyString () const;

  final @property bool isGoodForBool () const nothrow @safe @nogc {
    pragma(inline, true);
    return (isBool || isInt || isStr || isActor);
  }

  override bool opEquals (Object o) const nothrow @safe @nogc {
    if (auto t = cast(TypeBase)o) return same(t); else return false;
  }

  final bool same (in TypeBase t1) const nothrow @safe @nogc {
    alias t0 = this;
    if (t0 is null || t1 is null) return false;
    if (t0 is t1) return true;
    // functions?
    if (t0.isFunc || t1.isFunc) {
      if (auto f0 = cast(const(TypeFunc))t0) {
        if (auto f1 = cast(const(TypeFunc))t1) {
          if (f0.args.length == f1.args.length && f0.hasrest == f1.hasrest) {
            if (!f0.aux.same(f1.aux)) return false;
            foreach (immutable idx, const ref TypeFunc.Arg a0; f0.args) {
              if (!a0.type.same(f1.args[idx].type)) return false;
            }
            return true;
          }
        }
      }
      return false; // something is very wrong here
    }
    // pointers?
    if (t0.isPointer || t1.isPointer) {
      if (auto p0 = cast(const(TypePointer))t0) {
        if (auto p1 = cast(const(TypePointer))t1) {
          if (p0.isNull || p1.isNull) return true; // `null` is compatible with everything
          return p0.mBase.same(p1.mBase);
        }
      }
      return false; // something is very wrong here
    }
    // enums?
    if (t0.isEnum || t1.isEnum) {
      if (auto e0 = cast(const(TypeEnum))t0) {
        if (auto e1 = cast(const(TypeEnum))t1) {
          return (e0.name == e1.name); // && e0.constBase.same(e1.base));
        }
      }
      return false; // something is very wrong here
    }
    // arrays?
    if (t0.isArray || t1.isArray) {
      if (auto a0 = cast(const(TypeArray))t0) {
        if (auto a1 = cast(const(TypeArray))t1) {
          return (a0.mSize == a1.mSize && a0.mBase.same(a1.mBase));
        }
      }
      return false; // something is very wrong here
    }
    // oops
    return false;
  }

  // `tt` `hasrest` is ignored
  bool callCompatible (TypeBase tt, bool checkaux=true) nothrow @safe @nogc { return false; }
  final callCompatibleNoAux (TypeBase tt) nothrow @safe @nogc { return callCompatible(tt, false); }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TypePointer : TypeBase {
private:
  TypeBase mBase;

public:
  //this () { mBase = typeVoid; }
  this (TypeBase abase) { mBase = abase; }
  override @property bool isPointer () const nothrow @safe @nogc { return true; }
  override @property bool isFuncPtr () const nothrow @safe @nogc { return mBase.isFunc; }
  override @property bool isCallable () const nothrow @safe @nogc { return (mBase.isFunc && mBase.isCallable); } // pointer to pointer is not callable
  override @property uint cellSize () const nothrow @safe @nogc { return 1; }
  override @property TypeBase base () nothrow @safe @nogc { return mBase; }

  override string toPrettyString () const {
    //if (isFuncPtr) return mBase.toPrettyString;
    return "ptr to "~mBase.toPrettyString;
  }
}


public class TypeNull : TypePointer {
public:
  this () { super(typeVoid); }
  override @property bool isNull () const nothrow @safe @nogc { return true; }
  override @property TypeBase base () nothrow @safe @nogc { return this; }

  override string toPrettyString () const { return "null"; }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TypeArray : TypeBase {
private:
  TypeBase mBase;
  int mSize;

public:
  this (TypeBase abase, int asize) { mBase = abase; if (asize < 0) assert(0, "invalid size"); mSize = asize; }
  override @property bool isArray () const nothrow @safe @nogc { return true; }
  override @property TypeBase base () nothrow @safe @nogc { return mBase; }
  override @property uint cellSize () const nothrow @safe @nogc { return mSize*mBase.cellSize; } // size in cells
  final @property int size () const nothrow @safe @nogc { pragma(inline, true); return mSize; }
  // not a property -- by design
  final void setSize (int newsz) nothrow @safe @nogc { pragma(inline, true); if (newsz < 0) assert(0, "invalid size"); mSize = newsz; }

  override string toPrettyString () const {
    import std.format : format;
    return "%s[%d]".format(mBase.toPrettyString, mSize);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// typed enum
public class TypeEnum : TypeBase {
private:
  string mName;
  TypeBase mBase;
  int[string] members; // lucky me, strings can be encoded as integers too
  int mDefValue; // default value

public:
  this (string aname, TypeBase abase) { mName = aname; mBase = abase; }
  override @property bool isEnum () const nothrow @safe @nogc { return true; }
  override @property TypeBase base () nothrow @safe @nogc { return mBase; }
  override @property uint cellSize () const nothrow @safe @nogc { return mBase.cellSize; }
  final @property string name () const nothrow @safe @nogc { pragma(inline, true); return mName; }
  final int defval () const nothrow @safe @nogc { pragma(inline, true); return mDefValue; }
  final int minval () const nothrow @trusted @nogc {
    if (members.length == 0) return mDefValue; // just in case
    int res = int.max;
    foreach (immutable int v; members.byValue) if (res > v) res = v;
    return res;
  }
  final int maxval () const nothrow @trusted @nogc {
    if (members.length == 0) return mDefValue; // just in case
    int res = int.min;
    foreach (immutable int v; members.byValue) if (res < v) res = v;
    return res;
  }
  // returns `null` on missing member
  final string getMember (int val) const nothrow @trusted @nogc {
    foreach (const ref kv; members.byKeyValue) {
      if (kv.value == val) return (kv.key.length ? kv.key : ""); // just in case
    }
    return null;
  }

  override string toPrettyString () const { return (mName.length ? mName : "__anonymous_enum"); }
}


// ////////////////////////////////////////////////////////////////////////// //
// struct
public class TypeStruct : TypeBase {
public:
  static struct Member {
    string name;
    TypeBase type;
    int ofs;
    Loc defloc;

    @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (type !is null); }
  }

private:
  string mName;
  Member[] members;
  int mFullSize;

public:
  this (string aname) { mName = aname; }
  override @property bool isStruct () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return mFullSize; }
  final @property string name () const nothrow @safe @nogc { pragma(inline, true); return mName; }

  void appendMember() (string aname, TypeBase atype, in auto ref Loc adefloc) {
    assert(atype !is null);
    foreach (ref Member m; members) {
      if (m.name == aname) compileErrorAt(adefloc, "duplicate member name '"~aname~"'");
    }
    members ~= Member(aname, atype, mFullSize, adefloc);
    mFullSize += atype.cellSize;
  }

  // check `.valid` on return to see if this is valid member
  final Member getMember (const(char)[] aname) nothrow @safe @nogc {
    foreach (ref Member m; members) if (m.name == aname) return m;
    return Member.init;
  }

  override string toPrettyString () const { return (mName.length ? mName : "__anonymous_struct"); }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TypeVoid : TypeBase {
public:
  this () {}
  override @property bool isVoid () const nothrow @safe @nogc { return true; }

  override string toPrettyString () const { return "void"; }
}

public class TypeBool : TypeBase {
public:
  this () {}
  override @property bool isBool () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return 1; }

  override string toPrettyString () const { return "bool"; }
}

public class TypeInt : TypeBase {
public:
  this () {}
  override @property bool isInt () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return 1; }

  override string toPrettyString () const { return "int"; }
}

public class TypeStr : TypeBase {
public:
  this () {}
  override @property bool isStr () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return 1; }

  override string toPrettyString () const { return "string"; }
}

public class TypeActor : TypeBase {
public:
  this () {}
  override @property bool isActor () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return 1; }

  override string toPrettyString () const { return "Actor"; }
}


//WARNING: don't change this vars!
public __gshared TypeBase typeVoid;
public __gshared TypeBase typeBool;
public __gshared TypeBase typeInt;
public __gshared TypeBase typeStr;
public __gshared TypeBase typeActor;
public __gshared TypeBase typeNull;


shared static this () {
  typeVoid = new TypeVoid();
  typeBool = new TypeBool();
  typeInt = new TypeInt();
  typeStr = new TypeStr();
  typeActor = new TypeActor();
  typeNull = new TypeNull();
}


// ////////////////////////////////////////////////////////////////////////// //
public class TypeFunc : TypeBase {
public:
  static struct Arg {
    string name;
    TypeBase type;
    Value defval; // can be only literal of the corresponding type
    bool ptrref; // true: this argument is reference
    bool readonly;

    this (TypeBase t) pure nothrow @safe @nogc { pragma(inline, true); type = t; }
    this (string aname, TypeBase t, bool aref, bool ro=false) pure nothrow @safe @nogc { pragma(inline, true); name = aname; type = t; ptrref = aref; readonly = ro; }
  }

public:
  TypeBase aux; // return type
  Arg[] args;
  bool hasrest; // has '...'; rest args are in format: <type,value>*

public:
  this () {}
  override @property bool isFunc () const nothrow @safe @nogc { return true; }
  override @property bool isCallable () const nothrow @safe @nogc { return true; }
  override @property uint cellSize () const nothrow @safe @nogc { return 1; } // for function vars

  // method is anything with `Actor` as the first argument
  override @property bool isMethod () const nothrow @safe @nogc { return (args.length > 0 && args[0].type.isActor); }

  static final bool compatible (TypeBase t0, TypeBase t1) nothrow @safe @nogc {
    if (t0 is t1) return true;
    if (t0 is null || t1 is null) return false;
    if (t0.same(t1)) return true;
    // if t0 is null, nothing is compatible (FIXME?)
    if (t0.isNull) return false;
    // t0 is function type?
    if (t0.isFunc) {
      // t1 should be either null, or compatible function pointer
      return (t1.isNull || (t1.isFuncPtr && t1.base.same(t0)));
    }
    // t0 is funcptr?
    if (t0.isFuncPtr) {
      // t1 should be either null, or compatible function pointer
      return (t1.isNull || (t1.isFuncPtr && t1.base.same(t0.base)));
    }
    // t0 is pointer or actor?
    if (t0.isPointer || t0.isActor) return t1.isNull; // t1 should be null
    // alas
    return false;
  }

  // `tt` `hasrest` is ignored
  override bool callCompatible (TypeBase tt, bool checkaux=true) nothrow @safe @nogc {
    if (tt is null) return false;
    if (tt is this) return true;
    auto f1 = cast(TypeFunc)tt;
    if (!hasrest && f1.args.length > args.length) return false; // too many arguments
    if (checkaux) {
      if (!compatible(aux, f1.aux)) return false;
    }
    // check mandatory arguments
    assert(hasrest || f1.args.length <= args.length);
    foreach (immutable idx, ref TypeFunc.Arg a1; f1.args) {
      if (idx >= args.length) {
        if (!hasrest) return false;
        break;
      }
      if (!compatible(args[idx].type, a1.type)) return false;
    }
    // if we have more agruments, they should have default values
    if (f1.args.length < args.length) {
      foreach (ref TypeFunc.Arg a0; args[f1.args.length..$]) {
        if (!a0.defval.literal) return false;
      }
      //{ import core.stdc.stdio; stderr.fprintf("hasrest=%u; f1l=%u; al=%u\n", cast(uint)hasrest, cast(uint)f1.args.length, cast(uint)args.length); }
      return true;
    }
    // all mandatory args are ok; we can call this when:
    return (hasrest || args.length == f1.args.length);
  }

  override string toPrettyString () const {
    string res;
    if (aux !is null) res ~= aux.toPrettyString; else res ~= "auto";
    res ~= " function (";
    foreach (immutable idx, const ref arg; args) {
      if (idx != 0) res ~= ", ";
      if (arg.readonly) res ~= "in ";
      if (arg.ptrref) res ~= "ref ";
      res ~= arg.type.toPrettyString;
      res ~= " ";
      if (arg.name.length) res ~= arg.name; else res ~= "_";
    }
    res ~= ")";
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct VMCallFixup {
  Variable var;
  int ovidx; // overload index
  int vmpc;
  Loc loc;
  @property TypeFunc type () nothrow @safe @nogc { pragma(inline, true); return var.ovloads[ovidx].type; }
  @property int pc () nothrow @safe @nogc { pragma(inline, true); return var.ovloads[ovidx].pc; }
}

__gshared VMCallFixup[] vmcallfixes;


// returns `true` if there are some fixups left
//TODO: make this faster
bool fixupCalls () {
  if (vmcallfixes.length == 0) return false;
  int dpos = 0;
  int cpos = 0;
  while (cpos < vmcallfixes.length) {
    auto fix = vmcallfixes.ptr+cpos;
    if (fix.var.ovloads[fix.ovidx].pc == 0) {
      //compileErrorAt(fix.loc, "undefined function call");
      // move this up if necessary
      assert(cpos >= dpos);
      if (cpos != dpos) vmcallfixes.ptr[dpos++] = *fix;
    } else {
      assert(vmcode[fix.vmpc].opcode == OpCode.LitU16);
      vmcode[fix.vmpc] = VMOp.makeU16(OpCode.LitU16, vmcode[fix.vmpc].dest, cast(ushort)fix.var.ovloads[fix.ovidx].pc);
      //stderr.writefln("fixup at %04X: %04X %s", fix.vmpc, fix.var.fnpc[fix.ovidx], fix.var.name);
      //stderr.writefln("%04X: %s", fix.vmpc, decodeOpcode(fix.vmpc, vmcode[fix.vmpc]));
    }
    ++cpos;
  }
  if (vmcallfixes.length != dpos) {
    vmcallfixes.length = dpos;
    vmcallfixes.assumeSafeAppend;
  }
  return (dpos != 0);
}


void fixupDump (VFile fo) {
  if (vmcallfixes.length == 0) return;
  foreach (ref fix; vmcallfixes) {
    fo.writeln("undefined call to '", fix.var.name, "' at ", fix.loc);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private void unwindCodegen (int oldpc) {
  if (oldpc < 1 || oldpc > vmcode.length) assert(0, "internal compiler error");
  if (oldpc == vmcode.length) return; // nothing to do
  // remove fixups in invalid code area
  int dpos = 0;
  while (dpos < vmcallfixes.length && vmcallfixes[dpos].vmpc < oldpc) ++dpos;
  if (dpos < vmcallfixes.length) {
    // has something to remove
    assert(vmcallfixes[dpos].vmpc >= oldpc);
    int cpos = dpos+1;
    while (cpos < vmcallfixes.length) {
      if (vmcallfixes[cpos].vmpc < oldpc) {
        // shift this fixup (if necessary)
        if (cpos != dpos) {
          assert(cpos > dpos);
          vmcallfixes[dpos] = vmcallfixes[cpos];
        }
        ++dpos;
      }
      ++cpos;
    }
    vmcallfixes.length = dpos;
    vmcallfixes.assumeSafeAppend;
  }
  // remove generated code
  vmcode.length = oldpc;
  vmcode.assumeSafeAppend;
}


// ////////////////////////////////////////////////////////////////////////// //
struct JumpChain {
  int basepc = -1; // "base" PC (i.e. PC at the moment of `start*()` call
  int addr = -1;
  bool chain; // false: addr is real jump point, otherwise it is last chain member

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (addr >= 0 && basepc > 0); }

  // this starts backward jump point (all jumps will be directed to current PC)
  static JumpChain startHere () nothrow @trusted @nogc {
    JumpChain res;
    assert(vmcode.length);
    res.basepc = res.addr = cast(int)vmcode.length;
    res.chain = false;
    return res;
  }

  // this starts forward jump point (all jumps will be directed to some PC in the future)
  static JumpChain startForward () nothrow @trusted @nogc {
    JumpChain res;
    assert(vmcode.length);
    res.basepc = cast(int)vmcode.length;
    res.addr = 0; // end-of-chain
    res.chain = true;
    return res;
  }

  // call this when you'll unwind codegen
  void resetAndClear () nothrow @safe @nogc {
    basepc = addr = -1;
    chain = false;
  }

  // reset jump chain, unwind generated code
  void unwindToBase () {
    if (!valid) assert(0, "cannot unwind codegen with invalid jump chain");
    if (vmcode.length < basepc) assert(0, "internal compiler error");
    unwindCodegen(basepc);
    resetAndClear();
  }

  // convert forward jump chain to backward (i.e. direct all forward jumps to the current PC)
  // won' reset jump chain (i.e. it can be used after this call)
  void setHere () nothrow @trusted @nogc {
    if (!valid) assert(0, "cannot do anything with invalid jump chain");
    if (chain) {
      assert(addr >= 0);
      int jaddr = cast(int)vmcode.length;
      while (addr != 0) {
        int aprev = vmcode[addr].u16;
        assert(vmcode[addr].opcode == OpCode.Jump || vmcode[addr].opcode == OpCode.JTrue || vmcode[addr].opcode == OpCode.JFalse);
        int ofs = jaddr-(addr+1); // jump offset is "jump pc + 1"
        vmcode[addr].setS16(cast(short)ofs); // jump offset is "jump pc + 1"
        addr = aprev;
      }
      addr = jaddr;
      chain = false;
    } else {
      assert(basepc > 0);
      assert(addr > 0);
    }
  }

  // finish and reset jump chain; fill fix addresses for forward jumps
  void finishHere () nothrow @trusted @nogc {
    setHere();
    resetAndClear();
  }

  // put new jump into chain
  void putJump (OpCode opc, ubyte src=0) {
    assert(opc == OpCode.Jump || opc == OpCode.JTrue || opc == OpCode.JFalse);
    if (chain) {
      // remember, generate fake
      assert(addr >= 0);
      auto naddr = cast(int)vmcode.length;
      emitCode(VMOp.makeU16(opc, src, cast(ushort)addr));
      addr = naddr;
    } else {
      // generate real jump
      assert(addr > 0);
      int ofs = addr-(cast(int)vmcode.length+1); // jump offset is "jump pc + 1"
      emitCode(VMOp.makeS16(opc, src, cast(short)ofs));
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static struct FuncInfo {
  TypeFunc type;
  int pc; // 0: not known yet; <0: builtin
  string name; // may NOT be `null`! fully qualified name (`smth.func` for `method(smth) func ()`)
  string actorid; // for `method(smth) func ()`
  string shortname; // short name (`func` for `method(smth) func ()`)
  Loc loc;
  bool hasBody;

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (type !is null); }
  @property bool isMethod () const nothrow @safe @nogc { pragma(inline, true); return (type !is null && type.isMethod); }
}


class Variable {
  enum Kind {
    Global,
    Local,
    Reference, // always `Local`, but holds a reference (currently valid only for arguments)
  }

  int idx; // Local, and <0: argument
  Kind kind = Kind.Local;
  string name; // fully qualified
  TypeBase type; // has litle meaning for functions, 'cause real types are in overload set
  FuncInfo[] ovloads;
  Loc firstloc;
  bool readonly;

  final @property bool global () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Global); }
  final @property bool local () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Local); }
  final @property bool reference () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Reference); }

  enum NotFound = -1;
  enum Conflict = -2;

  // will set `targs.aux` on success
  int findOverload(bool showVariants) (TypeFunc targs) {
    if (targs is null) return NotFound;
    auto oldaux = targs.aux;
    // find "normal" call
    static if (showVariants) bool conflict = false;
    int normres = -1;
    foreach (immutable idx, ref FuncInfo fni; ovloads) {
      if (!fni.type.hasrest && fni.type.callCompatibleNoAux(targs)) {
        if (normres != -1) {
          targs.aux = oldaux;
          static if (showVariants) {
            if (!conflict) {
              compilerMessage("ERROR: conflicting overloads for '%s' found", name);
              compilerMessage("  %s", ovloads[normres].type.toPrettyString);
            }
            compilerMessage("  %s", fni.type.toPrettyString);
            conflict = true;
            continue;
          } else {
            return Conflict;
          }
        }
        normres = cast(int)idx;
        targs.aux = fni.type.aux;
      }
    }
    static if (showVariants) { if (conflict) return Conflict; }
    if (normres != -1) return normres;
    // no "normal" function found, try varargs
    int restres = -1;
    foreach (immutable idx, ref FuncInfo fni; ovloads) {
      if (fni.type.hasrest && fni.type.callCompatibleNoAux(targs)) {
        if (restres != -1) {
          targs.aux = oldaux;
          static if (showVariants) {
            if (!conflict) {
              compilerMessage("ERROR: conflicting overloads for '%s' found", name);
              compilerMessage("  %s", ovloads[restres].type.toPrettyString);
            }
            compilerMessage("  %s", fni.type.toPrettyString);
            conflict = true;
            continue;
          } else {
            return Conflict;
          }
        }
        restres = cast(int)idx;
        targs.aux = fni.type.aux;
      }
    }
    static if (showVariants) { if (conflict) return Conflict; }
    if (restres == -1) targs.aux = oldaux;
    return restres;
  }
}


class Constant {
  string name;
  TypeBase type;
  // for enums, `ival` and `sval` doesn't matter
  int ival;
  string sval;
}


// ////////////////////////////////////////////////////////////////////////// //
class Scope {
public:
  Scope parent;
  Variable[] vars;
  string[string] aliases;
}

__gshared Scope curscope = null;
__gshared int[128] curslots; // usecount for stack slots; <0: not temp (won't be freed)
__gshared FuncInfo curfunc;
__gshared int curmaxreg = 0;

__gshared JumpChain curcontpc; // current "continue" PC (<0: none)
__gshared JumpChain curbreakpc; // current "break" PC (<0: none)

__gshared Constant[string] gconsts; // global variables
__gshared TypeBase[string] gtypes; // for type aliases (not yet) and typed enums
__gshared Variable[string] globals; // global variables
__gshared Variable[string] funclist; // all known functions, fully qualified names
__gshared string[string] aliases;
__gshared string[] strlist; // list of all strings (searching is slow, so what?); [0] is always empty string


//FIXME: make this faster!
bool isKnownActorId (const(char)[] aid) nothrow @trusted @nogc {
  if (aid.length == 0) return false;
  foreach (Variable v; funclist.byValue) {
    if (v.ovloads[0].actorid == aid) return true;
  }
  return false;
}


//FIXME: make this faster!
Variable findFQMethod (const(char)[] aid, const(char)[] mname) nothrow @trusted @nogc {
  if (aid.length == 0 || mname.length == 0) return null;
  foreach (Variable v; funclist.byValue) {
    //stderr.writeln("looking for <", aid, "::", mname, ">: [", v.ovloads[0].actorid, "]:[", v.ovloads[0].shortname, "] -- [", v.ovloads[0].name, "]");
    if (v.ovloads[0].actorid == aid && v.ovloads[0].shortname == mname) return v;
  }
  return null;
}


// can return `null`
Variable findFunction (Value val) {
  if (!val.valid) return null;
  if (!val.type.isFunc) return null;
  auto fp = val.varname in funclist;
  if (fp is null) return null;
  return *fp;
}


shared static this () {
  strlist ~= ""; // predefined empty string
}


// ////////////////////////////////////////////////////////////////////////// //
void enterScope () {
  auto sc = new Scope();
  sc.parent = curscope;
  curscope = sc;
}


void leaveScope () {
  if (curscope is null) assert(0, "internal compiler error");
  // free allocated vars
  foreach (Variable v; curscope.vars) {
    if (v.idx > 0 && v.type.cellSize > 0) {
      foreach (immutable cidx; 0..v.type.cellSize) {
        if (v.idx+cidx >= curslots.length) assert(0, "internal compiler error");
        if (curslots[v.idx+cidx] != -1) assert(0, "internal compiler error");
        curslots[v.idx+cidx] = 0;
      }
    }
  }
  curscope = curscope.parent;
}


Variable enterVar(T:const(char)[]) (in auto ref Loc loc, T name, TypeBase type) {
  assert(curscope !is null);
  assert(type !is null && !type.isVoid);
  if (type.isArray && type.cellSize == 0) assert(0, "internal compiler error");
  if (name.length == 0) compileErrorAt(loc, "cannot create nameless variable");
  for (Scope sc = curscope; sc !is null; sc = sc.parent) {
    foreach (Variable var; sc.vars) {
      if (name == var.name) {
        if (sc is curscope) {
          compileErrorAt(loc, "duplicate variable name: '"~name~"'");
        } else {
          compileErrorAt(loc, "variable '"~name~"' shadows another variable with the same name at "~var.firstloc.toString);
        }
      }
    }
  }
  auto var = new Variable();
  var.firstloc = loc;
  if (type.cellSize == 1) {
    var.idx = Slot.makeTemp();
  } else {
    var.idx = Slot.makeTempBlock(type.cellSize);
  }
  curslots[var.idx..var.idx+type.cellSize] = -1; // mark as var
  var.kind = Variable.Kind.Local;
  static if (is(T == string)) var.name = name; else var.name = name.idup;
  var.type = type;
  curscope.vars ~= var;
  return var;
}


Variable findLocalVar (const(char)[] name) {
  for (Scope sc = curscope; sc !is null; sc = sc.parent) {
    foreach (Variable var; sc.vars) if (name == var.name) return var;
  }
  return null;
}


Variable findGlobalVar (const(char)[] name) {
  if (auto gv = name in globals) return *gv;
  if (auto fp = name in funclist) return *fp;
  return null;
}


Constant findConst (const(char)[] name) {
  if (auto cp = name in gconsts) return *cp;
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
// stack slot
struct Slot {
private:
  int ridx = -1;

private:
  static ubyte allocTempSlot () {
    foreach (immutable idx, int sc; curslots[]) {
      if (sc == 0) {
        curslots[idx] = 1;
        if (curmaxreg < idx) curmaxreg = cast(int)idx;
        //{ import core.stdc.stdio; stderr.fprintf("allocTempSlot(%d): %d\n", idx, curslots[idx]); }
        return cast(ubyte)idx;
      }
    }
    compileError("out of stack slots");
    assert(0, "out of stack slots");
  }

  static ubyte allocTempSlotBlock (int len) {
    if (len == 1) return allocTempSlot();
    if (len > 0 && len < curslots.length-1) {
      // check if we have empty block at the end
      int idx = cast(int)curslots.length-len;
      int n = 0;
      foreach (immutable int v; curslots[idx..idx+len]) n |= v;
      // no: alas, cannot allocate block
      if (n == 0) {
        // yes, there is empty block; now move it up
        while (idx > 0 && curslots[idx-1] == 0) --idx;
        assert(idx > 0); // slot 0 is always occupied
        assert(curslots[idx] == 0);
        curslots[idx] = 1;
        return cast(ubyte)idx;
      }
    }
    compileError("out of stack slots");
    assert(0, "out of stack slots");
  }

  static void refSlot (int idx) nothrow @nogc {
    if (idx >= 0 && idx < curslots.length) {
      //{ import core.stdc.stdio; stderr.fprintf("refSlot(%d): %d\n", idx, curslots[idx]); }
      //if (curslots[idx] == 0) assert(0, "wtf?!");
      if (curslots[idx] >= 0) ++curslots[idx];
    }
  }

  static void unrefSlot (int idx) nothrow @nogc {
    if (idx >= 0 && idx < curslots.length) {
      //{ import core.stdc.stdio; stderr.fprintf("unrefSlot(%d): %d\n", idx, curslots[idx]); }
      if (curslots[idx] == 0) {
        { import core.stdc.stdio; stderr.fprintf("trying to free unoccupied slot #%d\n", idx); }
        assert(0, "wtf?!");
      }
      if (curslots[idx] > 0) --curslots[idx];
    }
  }

public:
  alias idx this;

public:
  // direct ctor won't change refcount
  @disable this (int aidx);

  this (this) nothrow @nogc {
    pragma(inline, true);
    refSlot(ridx);
  }

  ~this () nothrow @nogc {
    pragma(inline, true);
    unrefSlot(ridx);
  }

  bool opCast(T) () const pure nothrow @safe @nogc if (is(T == bool)) {
    pragma(inline, true);
    return valid;
  }

  void opAssign() (in auto ref Slot v) nothrow @nogc {
    // order matters!
    refSlot(v.ridx);
    unrefSlot(ridx);
    ridx = v.ridx;
  }

  bool opEquals() (in auto ref Slot v) const pure nothrow @safe @nogc { pragma(inline, true); return (v.ridx == ridx); }

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (ridx >= 0 && ridx < 255); }
  @property ubyte idx () const pure nothrow @safe @nogc { pragma(inline, true); return cast(ubyte)(ridx); }

  // won't unref slot
  void reset () nothrow @nogc {
    pragma(inline, true);
    ridx = -1;
  }

  void clear () nothrow @nogc {
    pragma(inline, true);
    unrefSlot(ridx);
    ridx = -1;
  }

  static Slot makeTemp () {
    Slot res;
    res.ridx = allocTempSlot();
    return res;
  }

  static Slot makeTempBlock (int len) {
    Slot res;
    res.ridx = allocTempSlotBlock(len);
    return res;
  }

  static Slot acquire (int aidx) nothrow @nogc {
    if (aidx < 0 || aidx > 255) assert(0, "internal slot manager error");
    refSlot(aidx);
    Slot res;
    res.ridx = aidx;
    return res;
  }

  static Slot acquireNoRef (int aidx) nothrow @nogc {
    if (aidx < 0 || aidx > 255) assert(0, "internal slot manager error");
    if (curslots[aidx] == 0) assert(0, "internal slot manager error");
    Slot res;
    res.ridx = aidx;
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
//HACK: we need dynamically allocated values with deterministic destruction
//WARNING: don't create cycle references!
//NOTE: despite being called `*Ref`, this thing *copies* assigned `Value`
struct ValueRef {
private:
  static struct VPtr {
    Value v;
    int rc; // refcounter
  }
private:
  VPtr* vp;

public:
  this() (auto ref Value av) nothrow {
    if (av.valid) {
      // copy it
      vp = new VPtr();
      vp.rc = 1;
      vp.v = av;
    }
  }

  this (ValueRef avr) nothrow {
    if (avr.vp) {
      vp = avr.vp;
      ++vp.rc;
    }
  }

  this (this) nothrow @trusted @nogc {
    pragma(inline, true);
    if (vp !is null) ++vp.rc;
  }

  ~this () nothrow { clear(); }

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (vp !is null); }
  @property ref Value value () nothrow @safe @nogc { pragma(inline, true); if (vp is null) assert(0, "oops"); return vp.v; }

  bool opCast(T) () const pure nothrow @safe @nogc if (is(T == bool)) {
    pragma(inline, true);
    return (vp !is null);
  }

  void clear () nothrow {
    if (vp !is null) {
      if (--vp.rc == 0) {
        vp.v = Value.init; // free slots and other shit
        delete vp;
      }
      vp = null; // just in case
    }
  }

  void opAssign() (auto ref Value av) nothrow {
    clear();
    if (av.valid) {
      // copy it
      vp = new VPtr();
      vp.rc = 1;
      vp.v = av;
    }
  }

  void opAssign (ValueRef avr) nothrow {
    if (avr.vp is vp) return; // nothing to do
    if (avr.vp !is null) ++avr.vp.rc;
    clear();
    vp = avr.vp;
  }
}


struct Value {
public:
  enum Kind {
    Invalid,
    Global,
    Local,
    Argument,
    Literal,
    Stack,
    Field,
  }

public:
  TypeBase type;
  Kind kind = Kind.Invalid;
  string varname;
  int index; // global/fieldoffset/argumentidx (suitable for ALoad/AStore)
  Slot slot; // local/stack
  alias lit = index; // literal value
  Loc loc;
  ValueRef arridx; // array index is always int, we'll put it into a slot; invalid slot means no array offset
  int arrsize = -1; // this will be set in expression parser, and will be used to generate bound checking; -666: string indexing
  int elemsize; // for arrays
  ValueRef fldobj; // object (actor) for field
  bool ptrref; // `ref` for arguments
  int ptridx; // index for ref struct access
  bool readonly; // for arguments

private:
  @property bool isStrIndexing () const pure nothrow @safe @nogc { pragma(inline, true); return (arrsize == -666 && arridx.valid); }

public:
  bool opCast(T) () const pure nothrow @safe @nogc if (is(T == bool)) {
    pragma(inline, true);
    return valid;
  }

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (kind != Kind.Invalid && type !is null); }
  @property bool literal () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Literal); }
  @property bool stack () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Stack); }
  @property bool global () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Global); }
  @property bool local () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Local); }
  @property bool argument () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Argument); }
  @property bool field () const pure nothrow @safe @nogc { pragma(inline, true); return (kind == Kind.Field); }
  @property bool arrayaccess () const pure nothrow @safe @nogc { pragma(inline, true); return arridx.valid; }
  @property bool fieldaccess () const pure nothrow @safe @nogc { pragma(inline, true); return fldobj.valid; }

  static Value makeStack() (Slot aslot, TypeBase t, in auto ref Loc aloc) {
    if (!aslot.valid) assert(0, "internal compiler error");
    if (t is null) assert(0, "internal compiler error");
    Value res;
    res.type = t;
    res.kind = Kind.Stack;
    res.slot = aslot;
    res.loc = aloc;
    return res;
  }

  static Value makeStack() (Slot aslot, in auto ref Value v) {
    if (!aslot.valid) assert(0, "internal compiler error");
    if (v.type is null) assert(0, "internal compiler error");
    Value res;
    res.type = cast(TypeBase)v.type;
    res.kind = Kind.Stack;
    res.slot = aslot;
    res.loc = v.loc;
    return res;
  }

  static Value makeVoid() (in auto ref Loc aloc) {
    Value res;
    res.type = typeVoid;
    res.kind = Kind.Global;
    res.loc = aloc;
    return res;
  }

  static Value makeTemp() (in auto ref Value v) {
    Value res;
    res.type = cast(TypeBase)v.type;
    res.kind = Kind.Stack;
    res.slot = Slot.makeTemp();
    res.loc = v.loc;
    return res;
  }

  static Value makeTemp() (TypeBase t, in auto ref Loc aloc) {
    if (t is null) assert(0, "fatal internal error");
    Value res;
    res.type = t;
    res.kind = Kind.Stack;
    res.slot = Slot.makeTemp();
    res.loc = aloc;
    return res;
  }

  static Value makeTempBlock() (TypeBase t, int len, in auto ref Loc aloc) {
    if (t is null) assert(0, "fatal internal error");
    if (len < 1 || len > 128) assert(0, "fatal internal error");
    Value res;
    res.type = t;
    res.kind = Kind.Stack;
    res.slot = Slot.makeTempBlock(len); // first slot is refed, others aren't
    res.loc = aloc;
    return res;
  }

  static Value makeVar() (Variable var, in auto ref Loc aloc) {
    if (var is null) assert(0, "fatal internal error");
    Value res;
    res.type = var.type;
    if (var.global) {
      res.kind = Kind.Global;
      res.index = var.idx;
    } else if (var.idx >= 0 && var.idx <= 255) {
      // local
      assert(curslots[var.idx] == -1);
      res.kind = Kind.Local;
      res.slot = Slot.acquire(var.idx);
    } else {
      // argument
      assert(var.idx < 0);
      res.kind = Kind.Argument;
      res.index = -var.idx-1;
    }
    res.loc = aloc;
    res.ptrref = var.reference;
    res.readonly = var.readonly;
    res.varname = var.name;
    return res;
  }

  static Value makeNull() (in auto ref Loc aloc) {
    Value res;
    res.type = typeNull;
    res.kind = Kind.Literal;
    res.lit = 0;
    res.loc = aloc;
    return res;
  }

  static Value makeBool() (bool v, in auto ref Loc aloc) {
    Value res;
    res.type = typeBool;
    res.kind = Kind.Literal;
    res.lit = (v ? 1 : 0);
    res.loc = aloc;
    return res;
  }

  static Value makeInt() (int v, in auto ref Loc aloc) {
    Value res;
    res.type = typeInt;
    res.kind = Kind.Literal;
    res.lit = v;
    res.loc = aloc;
    return res;
  }

  static Value makeActor() (uint v, in auto ref Loc aloc) {
    Value res;
    res.type = typeActor;
    res.kind = Kind.Literal;
    res.lit = v;
    res.loc = aloc;
    return res;
  }

  static Value makeEnum() (TypeBase tp, int val, in auto ref Loc aloc) {
    auto etp = cast(TypeEnum)tp;
    if (etp is null) assert(0, "cannot make enum literal from non-enum type");
    Value res;
    res.type = tp;
    res.kind = Kind.Literal;
    res.lit = val;
    res.loc = aloc;
    return res;
  }

  static Value makeEnumDefault() (TypeBase tp, in auto ref Loc aloc) {
    auto etp = cast(TypeEnum)tp;
    if (etp is null) assert(0, "cannot make enum literal from non-enum type");
    Value res;
    res.type = tp;
    res.kind = Kind.Literal;
    res.lit = etp.defval;
    res.loc = aloc;
    return res;
  }

  static Value makeStr(T:const(char)[]) (T v, in auto ref Loc aloc) {
    Value res;
    res.type = typeStr;
    res.kind = Kind.Literal;
    if (v.length == 0) {
      res.index = 0;
    } else {
      int sidx = -1;
      foreach (immutable idx, string s; strlist) if (s == v) { sidx = cast(int)idx; break; }
      if (sidx < 0) {
        if (strlist.length == ushort.max) assert(0, "too many strings");
        sidx = cast(int)strlist.length;
        static if (is(T == string)) strlist ~= v; else strlist ~= v.idup;
      }
      res.index = sidx;
    }
    res.loc = aloc;
    return res;
  }

  static Value makeField() (Variable actid, const ref ActorField fld, in auto ref Loc aloc) {
    assert(actid !is null);
    assert(actid.type is typeActor);
    Value res;
    res.type = cast(TypeBase)fld.type;
    res.kind = Kind.Field;
    res.index = fld.ofs;
    res.fldobj = Value.makeVar(actid, aloc);
    res.loc = aloc;
    return res;
  }

  static Value makeField() (auto ref Value actid, const ref ActorField fld, in auto ref Loc aloc) {
    assert(actid.valid);
    assert(actid.type is typeActor);
    Value res;
    res.type = cast(TypeBase)fld.type;
    res.kind = Kind.Field;
    res.index = fld.ofs;
    res.fldobj = actid;
    res.loc = aloc;
    return res;
  }

  // this generates bounds checking, if necessary
  // it will normalize `arridx` too, and will set `elemsize` to `1`
  private Value arridxLoad(bool forcelit) () {
    if (!arrayaccess) assert(0, "internal compiler error");
    if (elemsize <= 0) assert(0, "internal compiler error");
    if (!arridx.value.type.isInt) compileErrorAt(arridx.value.loc, "invalid array index type");
    // array size is negative: no bounds checking, literal offset is fixed
    if (arrsize < 0) {
      assert(elemsize == 1);
      static if (!forcelit) {
        if (arridx.value.literal) return arridx.value;
      }
      if (!arridx.value.stack) arridx = arridx.value.load;
      return arridx.value;
    }
    // bounds checking
    if (arridx.value.literal) {
      if (arridx.value.lit >= arrsize) compileErrorAt(arridx.value.loc, "array index out of bounds");
      arridx.value.lit *= elemsize;
      elemsize = 1;
      arrsize = -1; // don't generate bounds checking anymore
      static if (forcelit) arridx = arridx.value.load;
      return arridx.value;
    } else {
      // generate bounds checking
      if (arrsize > ushort.max) assert(0, "internal compiler error");
      auto res = arridx.value.load;
      emitCode(VMOp.makeU16(OpCode.Bounds, res.slot, cast(ushort)arrsize));
      // multiply offset by element size
      if (elemsize > 1) {
        assert(elemsize <= ushort.max);
        auto tmpslot = Slot.makeTemp();
        emitILit(tmpslot, elemsize);
        emitCode(VMOp(OpCode.Mul, res.slot, res.slot, tmpslot));
        elemsize = 1;
      }
      // next loads will reuse this one
      arridx = res;
      arrsize = -1; // don't generate bounds checking anymore
      return res;
    }
  }

  // this caches loaded value
  private Value fldobjLoad(bool forcelit) () {
    if (!fieldaccess) assert(0, "internal compiler error");
    if (!fldobj.value.type.isActor) compileErrorAt(fldobj.value.loc, "invalid object type");
    if (fldobj.value.literal) {
      static if (forcelit) fldobj = fldobj.value.load;
      return fldobj.value;
    } else if (!fldobj.value.stack) {
      // next loads will reuse this one
      fldobj = fldobj.value.load;
    }
    // and do nothing for already loaded values
    return fldobj.value;
  }

  void cachePostponedValues () {
    if (arrayaccess && !arridx.value.literal) arridxLoad!false();
    if (fieldaccess && !fldobj.value.literal) fldobjLoad!false();
  }

  // ////////////////////////////////////////////////////////////////////// //
  // store pointer to variable into slot
  Value storePtrToSlot(string mode="move") (Slot slot) {
    if (ptrref) {
      // already a reference, just copy a pointer
      assert(argument);
      assert(!arrayaccess);
      assert(!fieldaccess);
      emitCode(VMOp(OpCode.ALoad, slot, cast(ubyte)index));
      // has offset?
      if (ptridx) {
        //FIXME: it is not safe to add to pointer this way!
        auto tmpidxslot = Slot.makeTemp();
        emitILit(tmpidxslot, ptridx);
        emitCode(VMOp(OpCode.Add, slot, slot, tmpidxslot));
      }
      return Value.makeStack(slot, typeVoid, loc); // arbitrary type
    }
    // create a pointer
    if (global) {
      // pointer to global
      assert(!fieldaccess);
      auto tmpidxslot = Slot.makeTemp();
      emitILit(tmpidxslot, index);
      emitCode(VMOp(OpCode.MakeGPtr, slot, tmpidxslot));
    } else if (argument) {
      // pointer to argument (the thing that cannot be)
      assert(!fieldaccess);
      auto tmpidxslot = Slot.makeTemp();
      emitILit(tmpidxslot, index);
      emitCode(VMOp(OpCode.MakeAPtr, slot, tmpidxslot));
      assert(0, "internal error in compiler");
    } else if (local) {
      // pointer to local
      assert(!fieldaccess);
      emitCode(VMOp(OpCode.MakeLPtr, slot, this.slot));
    } else if (field) {
      // pointer to field
      assert(fieldaccess);
      fldobjLoad!true;
      assert(fldobj.value.slot);
      auto tmpidxslot = Slot.makeTemp();
      emitILit(tmpidxslot, index);
      emitCode(VMOp(OpCode.MakeFPtr, slot, fldobj.value.slot, tmpidxslot));
    } else {
      assert(0, "wtf?!");
    }
    // if array access, offset pointer
    //FIXME: unsafe!
    if (arrayaccess) {
      if (isStrIndexing) compileError("cannot load pointer to string");
      bool dogen = true;
      if (arridx.value.literal && arridx.value.lit == 0) {
        arridxLoad!false; // this does bounds checking
        dogen = false;
      } else {
        arridxLoad!true;
      }
      if (dogen) {
        assert(arridx.value.slot);
        emitCode(VMOp(OpCode.Add, slot, slot, arridx.value.slot));
      }
    }
    return Value.makeStack(slot, typeVoid, loc); // arbitrary type
  }

  // ////////////////////////////////////////////////////////////////////// //
  // mode=move: just Mov
  // move=bool: use LogBNorm to create normalized boolean (and return boolean Value)
  // WARNING: won't increase refcount for `slot`!
  Value storeToSlot(string mode="move") (Slot slot) {
    static assert(mode == "move" || mode == "bool");
    alias v = this;
    if (!slot.valid) assert(0, "cannot store value to invalid slot");
    // check types
    if (v.type.isVoid) assert(0, "cannot store void value");
    // typed enums
    if (v.type.isEnum) {
      if (!v.type.base.isInt && !v.type.base.isBool && !v.type.base.isStr) {
        assert(0, "cannot store value of type "~typeid(v.type.base).name);
      }
      Value xv = v;
      //HACK
      xv.type = v.type.base;
      return xv.storeToSlot!mode(slot);
    }
    //HACK: make pointers great again!
    if (!v.type.isBool && !v.type.isInt && !v.type.isStr && !v.type.isActor && !v.type.isPointer) {
      assert(0, "cannot store value of type "~typeid(v.type).name);
    }
    // temporary stack slot?
    if (v.stack) {
      // this cannot have array index
      assert(v.slot.valid);
      assert(!v.arrayaccess);
      assert(!v.fieldaccess);
      static if (mode == "bool") {
        // normalized bool
        if (!v.type.isBool) {
          // not a boolean, normalize
          emitCode(VMOp(OpCode.LogBNorm, slot, v.slot));
          return v.makeStack(slot, typeBool, v.loc);
        } else if (v.slot != slot) {
          emitCode(VMOp(OpCode.Mov, slot, v.slot));
          return v.makeStack(slot, v);
        }
        assert(v.type.isBool);
      } else {
        // move
        if (v.slot != slot) {
          emitCode(VMOp(OpCode.Mov, slot, v.slot));
          return v.makeStack(slot, v);
        }
      }
      return v;
    }
    // literal?
    if (v.literal) {
      // this cannot have array index
      assert(!v.arrayaccess);
      assert(!v.fieldaccess);
      static if (mode == "bool") {
        // normalized bool
        if (!v.type.isBool) {
          emitILit(slot, (v.lit ? 1 : 0));
          return v.makeStack(slot, typeBool, v.loc);
        }
      }
      // move
      emitILit(slot, v.lit);
      return v.makeStack(slot, v);
    }
    // global?
    if (v.global) {
      assert(!v.fieldaccess);
      if (v.arrayaccess) {
        // array indexing
        if (!v.isStrIndexing) {
          // global array indexing
          auto gitemp = Value.makeInt(v.index, v.loc).load;
          emitCode(VMOp(OpCode.GArrLoad, slot, gitemp.slot, v.arridxLoad!true.slot));
        } else {
          // string indexing
          // load global value
          auto tmpslot = Slot.makeTemp();
          emitCode(VMOp.makeU16(OpCode.GLoad, tmpslot, cast(ushort)v.index));
          // index string
          emitCode(VMOp(OpCode.StrLoadChar, slot, tmpslot, v.arridxLoad!true.slot));
        }
      } else {
        emitCode(VMOp.makeU16(OpCode.GLoad, slot, cast(ushort)v.index));
      }
      static if (mode == "bool") {
        // normalized bool
        if (!v.type.isBool) {
          emitCode(VMOp(OpCode.LogBNorm, slot, slot));
          return v.makeStack(slot, typeBool, v.loc);
        }
      }
      return v.makeStack(slot, v);
    }
    // argument?
    if (v.argument) {
      assert(!v.fieldaccess);
      // array access?
      if (v.arrayaccess) {
        if (!v.isStrIndexing) compileErrorAt(v.loc, "array arguments aren't supported yet");
        if (v.ptrref) compileErrorAt(v.loc, "string references aren't supported yet");
        // string indexing
        auto tmpslot = Slot.makeTemp();
        emitCode(VMOp(OpCode.ALoad, tmpslot, cast(ubyte)v.index));
        emitCode(VMOp(OpCode.StrLoadChar, slot, tmpslot, v.arridxLoad!true.slot));
        static if (mode == "bool") {
          emitCode(VMOp(OpCode.LogBNorm, slot, slot));
          return v.makeStack(slot, typeBool, v.loc);
        } else {
          return v.makeStack(slot, v);
        }
      } else {
        emitCode(VMOp(OpCode.ALoad, slot, cast(ubyte)v.index));
        if (v.ptrref) {
          // load value via pointer
          auto tmpidxslot = Slot.makeTemp();
          emitILit(tmpidxslot, v.ptridx);
          emitCode(VMOp(OpCode.PtrLoad, slot, slot, tmpidxslot));
        }
        static if (mode == "bool") {
          // normalized bool
          if (!v.type.isBool) {
            emitCode(VMOp(OpCode.LogBNorm, slot, slot));
            return v.makeStack(slot, typeBool, v.loc);
          }
        }
        return v.makeStack(slot, v);
      }
    }
    // local?
    if (v.local) {
      assert(!v.fieldaccess);
      assert(v.slot.valid);
      assert(!v.ptrref); // not yet
      // array access?
      if (v.arrayaccess) {
        if (v.isStrIndexing) {
          // string indexing
          emitCode(VMOp(OpCode.StrLoadChar, slot, v.slot, v.arridxLoad!true.slot));
          static if (mode == "bool") {
            emitCode(VMOp(OpCode.LogBNorm, slot, slot));
            return v.makeStack(slot, typeBool, v.loc);
          } else {
            return v.makeStack(slot, v);
          }
        } else {
          // local array indexing
          if (v.arridx.value.literal) {
            v.arridxLoad!false; // bounds checking, literal normalization
            assert(v.arridx.value.literal);
            static if (mode == "bool") {
              emitCode(VMOp(OpCode.LogBNorm, slot, cast(ubyte)(v.slot.idx+v.arridx.value.lit*v.elemsize)));
              return v.makeStack(slot, typeBool, v.loc);
            } else {
              emitCode(VMOp(OpCode.Mov, slot, cast(ubyte)(v.slot.idx+v.arridx.value.lit*v.elemsize)));
            }
          } else {
            emitCode(VMOp(OpCode.LArrLoad, slot, v.slot, v.arridxLoad!true.slot));
          }
          static if (mode == "bool") {
            emitCode(VMOp(OpCode.LogBNorm, slot, slot));
            return v.makeStack(slot, typeBool, v.loc);
          }
          return v.makeStack(slot, v);
        }
        assert(0);
      }
      // normal local
      if (v.slot == slot) return v; // nothing to do
      // move
      static if (mode == "bool") {
        emitCode(VMOp(OpCode.LogBNorm, slot, v.slot));
        return v.makeStack(slot, typeBool, v.loc);
      } else {
        emitCode(VMOp(OpCode.Mov, slot, v.slot));
        return v.makeStack(slot, v);
      }
    }
    // field?
    if (v.field) {
      //FLoad, // dest (slot), actid, fldidx -- load field value to dest
      //HACK: don't load literal offset into register
      if (v.arrayaccess) {
        if (!v.arridx.value.type.isInt) compileErrorAt(v.arridx.value.loc, "invalid array index type");
        if (v.arridx.value.literal) {
          if (!v.isStrIndexing) {
            v.arridxLoad!false; // bounds checking, literal normalization
            assert(v.arridx.value.literal);
          }
        } else {
          v.arridxLoad!true; // this generates bound checking code
        }
      }
      auto actid = v.fldobjLoad!true;
      auto tmp = Value.makeInt(v.index, v.loc);
      // if this is an array, fix field offset
      if (v.arrayaccess) {
        if (!v.isStrIndexing) {
          // array indexing
          if (v.arridx.value.literal) {
            // hack
            tmp.lit += v.arridx.value.lit*v.elemsize;
            tmp = tmp.load;
          } else {
            tmp = tmp.load;
            assert(v.arridx.value.slot); // it should be it
            emitCode(VMOp(OpCode.Add, tmp.slot, tmp.slot, v.arridx.value.slot));
          }
        } else {
          // string indexing; later
        }
      } else {
        tmp = tmp.load;
      }
      assert(tmp.slot.valid);
      assert(actid.slot.valid);
      emitCode(VMOp(OpCode.FLoad, slot, actid.slot, tmp.slot));
      // string indexing?
      if (v.arrayaccess && v.isStrIndexing) {
        emitCode(VMOp(OpCode.StrLoadChar, slot, slot, v.arridx.value.load.slot));
      }
      static if (mode == "bool") {
        emitCode(VMOp(OpCode.LogBNorm, slot, slot));
        return v.makeStack(slot, typeBool, v.loc);
      } else {
        return v.makeStack(slot, v);
      }
    }
    // oops
    assert(0, "internal compiler error: storeToSlot cannot load something");
  }

  // mode=move: just Mov
  // move=bool: use LogBNorm to create normalized boolean (and return boolean Value)
  Value load(string mode="move") () {
    static assert(mode == "move" || mode == "bool");
    alias v = this;
    // check types
    if (v.type.isVoid) assert(0, "cannot load void value");
    if (v.type.isEnum) return v.storeToSlot!mode(Slot.makeTemp);
    if (!v.type.isBool && !v.type.isInt && !v.type.isStr && !v.type.isActor && !v.type.isPointer) {
      assert(0, "cannot load value of type "~typeid(v.type).name);
    }
    // temporary stack slot?
    if (v.stack) return v.storeToSlot!mode(v.slot);
    // other kinds
    return v.storeToSlot!mode(Slot.makeTemp);
  }

  // returns `false` if type conversion failed, or `this` is not a variable
  bool assign (Value src) {
    alias dest = this;
    if (dest.literal) return false;
    // pointer assign?
    if (src.type.isPointer) {
      // no array access here
      if (src.arrayaccess) return false;
      if (dest.type.isPointer) {
        if (!dest.type.same(src.type)) return false; // oops, bad pointers
      } else if (src.type.isNull) {
        if (!dest.type.isPointer && !dest.type.isActor) return false; // two special cases
      } else {
        if (!dest.type.same(src.type.base)) return false; // oops, bad type
      }
    } else {
      // other assigns
      if (!dest.type.same(src.type)) {
        // convert to and from bool
        if (dest.type.isBool) {
          // to bool
          if (src.literal) {
            src = Value.makeBool((src.lit != 0), src.loc);
          } else {
            src = src.load!"bool";
          }
        } else if (src.type.isBool) {
          // from bool to int
          if (!dest.type.isInt) return false;
          // ints and bools are the same for VM
        } else {
          return false;
        }
      }
    }
    if (dest.stack) {
      // stack slot
      // no array access here
      if (dest.arrayaccess) return false;
      src.storeToSlot(dest.slot);
    } else if (dest.global) {
      // global
      src = src.load;
      // array access?
      if (dest.arrayaccess) {
        if (dest.isStrIndexing) return false;
        auto gitemp = Value.makeInt(dest.index, dest.loc).load;
        emitCode(VMOp(OpCode.GArrStore, src.slot, gitemp.slot, dest.arridxLoad!true.slot));
      } else {
        emitCode(VMOp.makeU16(OpCode.GStore, src.slot, cast(ushort)dest.index));
      }
    } else if (dest.argument) {
      // function argument
      //FIXME: no array access here (yet)
      if (dest.arrayaccess) return false;
      // argument
      src = src.load;
      if (dest.ptrref) {
        // load pointer
        auto tmpptrslot = Slot.makeTemp();
        emitCode(VMOp(OpCode.ALoad, tmpptrslot, cast(ubyte)dest.index));
        // store value via pointer
        auto tmpidxslot = Slot.makeTemp();
        emitILit(tmpidxslot, dest.ptridx);
        emitCode(VMOp(OpCode.PtrStore, src.slot, tmpptrslot, tmpidxslot));
      } else {
        // normal `by-value` argument
        emitCode(VMOp(OpCode.AStore, cast(ubyte)dest.index, src.slot));
      }
    } else if (dest.local) {
      // local
      assert(dest.slot.valid);
      // array access?
      if (dest.arrayaccess) {
        //TODO: optimize this
        if (dest.isStrIndexing) return false;
        src = src.load;
        if (dest.arridx.value.literal) {
          dest.arridxLoad!false; // bounds checking, literal normalization
          assert(dest.arridx.value.literal);
          emitCode(VMOp(OpCode.Mov, cast(ubyte)(dest.slot+dest.arridx.value.lit*dest.elemsize), src.slot));
        } else {
          emitCode(VMOp(OpCode.LArrStore, src.slot, dest.slot, dest.arridxLoad!true.slot));
        }
      } else {
        src.storeToSlot(dest.slot);
      }
    } else if (dest.field) {
      // field
      //FStore, // src (slot), actid, fldidx -- store field value from src
      //HACK: don't load literal offset into register
      if (dest.arrayaccess) {
        if (dest.isStrIndexing) return false;
        if (!dest.arridx.value.type.isInt) compileErrorAt(dest.arridx.value.loc, "invalid array index type");
        if (dest.arridx.value.literal) {
          dest.arridxLoad!false; // bounds checking, literal normalization
          assert(dest.arridx.value.literal);
        } else {
          dest.arridxLoad!true; // this generates bound checking code
        }
      }
      auto actid = dest.fldobjLoad!true;
      assert(actid.slot.valid);
      auto tmp = Value.makeInt(dest.index, dest.loc);
      // if this is an array, fix field offset
      if (dest.arrayaccess) {
        if (dest.isStrIndexing) return false;
        if (dest.arridx.value.literal) {
          // hack
          tmp.lit += dest.arridx.value.lit*dest.elemsize;
          tmp = tmp.load;
        } else {
          tmp = tmp.load;
          assert(dest.arridx.value.slot); // it should be it
          emitCode(VMOp(OpCode.Add, tmp.slot, tmp.slot, dest.arridx.value.slot));
        }
      } else {
        tmp = tmp.load;
      }
      assert(tmp.slot.valid);
      src = src.load;
      emitCode(VMOp(OpCode.FStore, src.slot, actid.slot, tmp.slot));
    } else {
      assert(0, "internal compiler error");
    }
    return true;
  }
}


// ////////////////////////////////////////////////////////////////////// //
//TODO: multidimensional arrays
TypeBase parseTypeDecl(bool convertFuncToFuncPtr) () {
  TypeBase t = null;
       if (token.kw(Token.T.Bool)) t = typeBool;
  else if (token.kw(Token.T.Int)) t = typeInt;
  else if (token.kw(Token.T.String)) t = typeStr;
  else if (token.kw(Token.T.Actor)) t = typeActor;
  else if (token.kw(Token.T.Void)) t = typeVoid;
  else if (token.kw(Token.T.Struct)) t = parseStructDecl(false);
  else if (token.id) {
    if (auto etp = token.sval in gtypes) {
      t = *etp;
    }
  }
  if (t is null) return null;
  // skip type name
  skipToken();
  // check for function type
  if (token.kw(Token.T.Function) || token.kw(Token.T.Method)) {
    bool ismethod = token.kw(Token.T.Method);
    // skip keyword
    skipToken();
    auto tf = parseFuncArgs(ismethod);
    tf.aux = t;
    static if (convertFuncToFuncPtr) return new TypePointer(tf); else return tf;
  }
  // check for array type
  if (token.delim("[")) {
    if (t.isVoid) compileError("cannot create void array");
    if (t.isFunc) t = new TypePointer(t); // convert `.isFunc` to funcptr
    skipToken();
    // `[]`?
    if (token.delim("]")) {
      // size==0 means "automatically calculate"
      auto ta = new TypeArray(t, 0);
      skipToken();
      return ta;
    }
    // get array size
    auto szloc = token.loc;
    auto vsz = parseExpr();
    if (!token.delim("]")) compileError("']' expected");
    skipToken();
    // check size
    if (!vsz.literal && !vsz.type.isInt) compileErrorAt(szloc, "integer literal expected");
    if (vsz.lit < 1 || vsz.lit > 16384) compileErrorAt(szloc, "invalid array size");
    // looks like an array
    return new TypeArray(t, vsz.lit);
  }
  // ordinary type
  return t;
}


// should be at `struct` keyword
// returns `null` if not
TypeStruct parseStructDecl (bool mustBeNamed) {
  if (!token.kw(Token.T.Struct)) return null;
  auto stloc = token.loc;
  skipToken();
  string name;
  if (mustBeNamed && !token.id) compileError("identifier expected");
  if (token.id) {
    name = token.sval;
    skipToken();
  }
  if (!token.delim("{")) compileError("'{' expected");
  skipToken();
  auto stp = new TypeStruct(name);
  while (!token.delim("}")) {
    auto tploc = token.loc;
    auto tp = parseTypeDecl!true();
    if (tp is null) compileErrorAt(tploc, "type declaration expected");
    if (!token.id) compileError("identifier expected");
    while (token.id) {
      stp.appendMember(token.sval, tp, token.loc);
      skipToken();
      if (!token.delim(",")) break;
      skipToken();
    }
    if (!token.delim(";")) compileError("';' expected");
    skipToken();
  }
  if (!token.delim("}")) compileError("'}' expected");
  skipToken();
  return stp;
}


// ////////////////////////////////////////////////////////////////////// //
// expression parser

// `me` ends with 'R': right-associative
Value parseAnyBinOp(string me, string upfunc, T...) () {
  auto lhs = mixin(upfunc~"()");
  mainloop: while (!token.empty) {
    foreach (immutable idx, immutable _; T) {
      static if (idx%2 == 0) {
        if (token.delim(T[idx])) {
          skipToken(); // skip operator
          static if (T[idx] == "||" || T[idx] == "&&") {
            lhs = T[idx+1](lhs);
            continue mainloop;
          } else {
            static if (me[$-1] == 'R') {
              auto rhs = mixin(me~"()");
            } else {
              auto rhs = mixin(upfunc~"()");
            }
            lhs = T[idx+1](lhs, rhs);
            continue mainloop;
          }
        }
      }
    }
    break;
  }
  return lhs;
}


Value parseExprPrimary () {
  // literals and id
  if (token.num) { auto res = Value.makeInt(token.ival, token.loc); skipToken(); return res; }
  if (token.str) { auto res = Value.makeStr(token.sval, token.loc); skipToken(); return res; }
  if (token.kw) {
    switch (token.type) {
      case Token.T.True:
      case Token.T.False:
        auto res = Value.makeBool(token.kw(Token.T.True), token.loc);
        skipToken();
        return res;
      case Token.T.Null:
        auto res = Value.makeNull(token.loc);
        skipToken();
        return res;
      case Token.T.New: // new ObjId[(args)]
        // compile this to:
        //   Actor temp = spawn();
        //   temp.ctor(args);
        // result is temp
        auto newloc = token.loc;
        skipToken();
        if (!token.id) compileError("identifier expected");
        string aid = token.sval;
        string fullname = aid~"::ctor";
        auto fp = fullname in funclist;
        if (fp is null) compileError("`ctor` for `"~aid~"` not found");
        auto res = Value.makeVar(*fp, newloc);
        skipToken();
        // compile `Actor temp = spawn()`
        auto spfn = "spawn" in funclist;
        if (spfn is null) compileErrorAt(newloc, "`spawn` not found");
        Value tempact;
        assert(!tempact.valid);
        // find `Actor spawn ()`, and generate the call
        foreach (immutable fnidx, ref FuncInfo fif; spfn.ovloads) {
          if (fif.type.aux.isActor && fif.type.args.length == 0) {
            // i found her!
            FunCallInfo fci;
            fci.start();
            // use result slot as tempact
            tempact = cgFunCall(Value.makeVar(*spfn, newloc), fci);
            assert(tempact.slot);
            assert(tempact.type.isActor);
            break;
          }
        }
        if (!tempact.valid) compileErrorAt(newloc, "`Actor spawn ()` not found");
        // call ctor
        res = parseFunCall!false(res, tempact);
        if (!res.type.isVoid) compileErrorAt(newloc, "`"~aid~"::ctor` must be void");
        // return resulting actor
        return tempact;
      case Token.T.Cast:
        auto loc = token.loc;
        skipToken();
        if (!token.delim("(")) compileError("'(' expected");
        skipToken();
        TypeBase tp = parseTypeDecl!true();
        if (tp is null) compileError("type name expected");
        if (!tp.isInt && !tp.isBool && !tp.isEnum && !tp.isVoid) compileErrorAt(loc, "invalid cast destination type");
        if (!token.delim(")")) compileError("')' expected");
        skipToken();
        auto eloc = token.loc;
        auto val = parseExprUnary();
        // cast to void
        if (tp.isVoid) return Value.makeVoid(loc);
        // cast to enum
        if (tp.isEnum) {
          if (!val.type.isInt) compileErrorAt(eloc, "integer expected");
          if (val.literal) {
            //??? do sanity checks
            return Value.makeEnum(tp, val.lit, eloc);
          } else {
            //HACK
            val.type = tp;
            return val;
          }
          assert(0, "wtf?!");
        }
        // need any conversion?
        if (!val.type.same(tp)) {
          // possible conversions:
          // anything -> bool
          // bool -> int
          // enum -> int
          if (tp.isBool) {
            // anything -> bool
            if (val.literal) {
              val = Value.makeBool((val.lit != 0), val.loc);
            } else {
              val = val.load!"bool";
            }
          } else if (tp.isInt) {
            if (val.type.isEnum) {
              // enum -> int
              if (val.literal) {
                //HACK
                val.type = val.type.base;
              } else {
                auto btp = val.type.base;
                val = val.load;
                assert(val.stack);
                //HACK
                val.type = btp;
              }
            } else if (val.type.isBool) {
              // bool -> int
              if (val.literal) {
                val = Value.makeInt((val.lit ? 1 : 0), val.loc);
              } else {
                val = val.load;
                assert(val.stack);
                //HACK
                val.kind = Value.Kind.Stack;
                val.type = typeBool;
              }
            } else {
              compileErrorAt(loc, "invalid type conversion");
            }
          } else {
            compileErrorAt(loc, "invalid type conversion");
          }
        }
        return val;
      case Token.T.Abs:
        auto loc = token.loc;
        skipToken();
        if (!token.delim("(")) compileError("'(' expected");
        skipToken();
        auto res = parseExpr();
        if (!token.delim(")")) compileError("')' expected");
        skipToken();
        if (!res.type.isInt) compileErrorAt(loc, "integer argument expected");
        if (res.literal) {
          if (res.lit == 0x8000_0000) compileErrorAt(loc, "cannot negate this integer");
          if (res.lit < 0) res.lit = -res.lit;
        } else {
          res = res.load;
          emitCode(VMOp(OpCode.IntAbs, res.slot, res.slot));
        }
        return res;
      default:
    }
    compileError("unexpected keyword");
    assert(0);
  }

  if (token.id) {
    auto loc = token.loc;
    string idname = token.sval;
    // max alias chain length
    foreach (immutable _; 0..64) {
      // try variable
      //stderr.writeln("step: ", _, "; id: <", idname, ">");
      if (auto var = findLocalVar(idname)) {
        auto res = Value.makeVar(var, token.loc);
        skipToken();
        return res;
      }
      // if we're in method, look for field first
      if (curfunc.isMethod) {
        auto fld = actorFindField(idname);
        if (fld.valid) {
          // load `this`
          auto xthis = findLocalVar("this");
          if (xthis is null) compileError("`this` not found");
          auto res = Value.makeField(xthis, fld, token.loc);
          skipToken();
          return res;
        }
      }
      // try variable
      if (auto var = findGlobalVar(idname)) {
        auto res = Value.makeVar(var, token.loc);
        skipToken();
        return res;
      }
      //HACK: try typed enum
      // this should be done in `parseDot()`, and we should have a special `Value` type for this, but meh...
      if (auto etp = idname in gtypes) {
        // typed enum access should be followed by dot, always
        if (auto etype = cast(TypeEnum)(*etp)) {
          skipToken();
          if (!token.delim(".")) compileError("'.' expected");
          skipToken();
          if (!token.id && !token.kw(Token.T.Default)) compileError("identifier expected");
          if (token.id("min")) {
            skipToken();
            return Value.makeEnum(etype, etype.minval, loc);
          } else if (token.id("max")) {
            skipToken();
            return Value.makeEnum(etype, etype.maxval, loc);
          } else if (token.kw(Token.T.Default) || token.id("init")) {
            skipToken();
            return Value.makeEnumDefault(etype, loc);
          } else if (auto mv = token.sval in etype.members) {
            skipToken();
            Value res;
                 if (etype.base.isInt) res = Value.makeInt(*mv, loc);
            else if (etype.base.isStr) res = Value.makeStr(strlist[*mv], loc);
            else assert(0, "wtf?!");
            //HACK:
            res.type = etype;
            return res;
          } else {
            compileError("undefined member '"~token.sval~"' in enum '"~etype.name~"'");
          }
        } else {
          compileError("cannot dot-access non-enum types");
        }
        assert(0);
      }
      // try constant
      if (auto c = findConst(idname)) {
        skipToken();
        if (c.type.isBool) return Value.makeBool((c.ival != 0), loc);
        if (c.type.isInt) return Value.makeInt(c.ival, loc);
        if (c.type.isStr) return Value.makeStr(c.sval, loc);
        compileError("invalid constant type");
      }
      // try actorid
      if (isKnownActorId(idname)) {
        auto aidloc = token.loc;
        skipToken();
        string aid = idname;
        if (!token.delim(".")) compileError("'.' expected");
        skipToken();
        if (!token.id) compileError("identifier expected");
        string xname = aid~"::"~token.sval;
        skipToken();
        if (auto var = findGlobalVar(xname)) {
          auto res = Value.makeVar(var, token.loc);
          return res;
        } else {
          compileErrorAt(aidloc, "undefined identifier '"~token.sval~"' in class '"~aid~"'");
          assert(0);
        }
      }
      // try alias
      bool found = false;
      for (Scope sc = curscope; sc !is null; sc = sc.parent) {
        if (auto asp = idname in sc.aliases) { idname = *asp; found = true; break; }
      }
      if (!found) {
        if (auto asp = idname in aliases) { idname = *asp; found = true; } // alias chain loop
      }
      // if nothing fits, try CurActorId.id
      if (!found && curfunc.actorid.length) {
        string xname = curfunc.actorid~"::"~idname;
        if (auto var = findGlobalVar(xname)) {
          auto res = Value.makeVar(var, token.loc);
          skipToken();
          return res;
        }
      }
      if (!found) break;
    }
    compileErrorAt(loc, "undefined identifier '"~token.sval~"'");
  }

  // "(...)"
  if (token.delim("(")) {
    auto loc = token.loc;
    skipToken();
    auto res = parseExpr();
    if (!token.delim(")")) compileError("`)` expected for `(` at "~loc.toString);
    skipToken();
    return res;
  }

  // global scope
  if (token.delim(".")) compileError("no global scope access is supported yet");

  // prefix
  if (token.delim("++") || token.delim("--")) {
    bool isPP = token.delim("++");
    auto loc = token.loc;
    skipToken();
    auto val = parseExprUnary();
    if (!val.local && !val.argument && !val.global && !val.field) compileErrorAt(val.loc, "cannot perform prefix inc/dec on non-var");
    if (val.readonly) compileError("cannot assign to read-only variable");
    if (!val.type.isInt) compileErrorAt(val.loc, "cannot perform prefix inc/dec on non-int");
    // little optimiations
    if (val.local && val.slot.valid && !val.arrayaccess && !val.fieldaccess && !val.ptrref) {
      // do direct inc/dec, but change `val` kind to `stack`
      emitCode(VMOp((isPP ? OpCode.Inc : OpCode.Dec), val.slot, val.slot));
      //HACK
      val.kind = Value.Kind.Stack;
      return val;
    } else {
      auto rhs = val.storeToSlot(Slot.makeTemp());
      emitCode(VMOp((isPP ? OpCode.Inc : OpCode.Dec), rhs.slot, rhs.slot));
      if (!val.assign(rhs)) compileErrorAt(loc, "cannot perform prefix inc/dec");
      return rhs;
    }
  }

  compileError("primary expression expected");
  assert(0);
}


struct FunCallInfo {
  enum MaxArgCount = 64;
  TypeFunc targs;
  Value[MaxArgCount] argv;
  int argc;

  @disable this (this); // no copies

  void start () {
    if (targs !is null) assert(0, "duplicate call to `FunCallInfo.start()`");
    targs = new TypeFunc();
  }

  void putArg (Value a) {
    //stderr.writeln("putarg(", argc, "): ", a.type.toPrettyString);
    if (targs is null) assert(0, "forgotten call to `FunCallInfo.start()`");
    if (!a.valid) assert(0, "fatal internal error");
    if (a.type.isVoid || a.type.isFunc) compileErrorAt(a.loc, "invalid argument type");
    if (argc >= argv.length) compileErrorAt(a.loc, "too many arguments");
    targs.args ~= TypeFunc.Arg(a.type);
    argv[argc++] = a;
  }

  void prependArg (Value a) {
    if (targs is null) assert(0, "forgotten call to `FunCallInfo.start()`");
    if (!a.valid) assert(0, "fatal internal error");
    if (a.type.isVoid || a.type.isFunc) compileErrorAt(a.loc, "invalid argument type");
    if (argc >= argv.length) compileErrorAt(a.loc, "too many arguments");
    targs.args.length += 1;
    foreach_reverse (immutable idx; 1..targs.args.length) targs.args[idx] = targs.args[idx-1];
    targs.args[0] = TypeFunc.Arg(a.type);
    ++argc;
    foreach_reverse (immutable idx; 1..argc) argv[idx] = argv[idx-1];
    argv[0] = a;
  }
}


// `val` should be `.isFunc` or `.isFuncPtr`
Value cgFunCall (Value val, ref FunCallInfo fci) {
  assert(val.valid);
  assert(val.type.isCallable);
  assert(fci.targs !is null);

  TypeFunc cftype = null; // exact type of the function we'll call
  Value rv; // loaded function address
  bool hasrest = false;

  bool prependThisOrActId () {
    // for `act.func` use `act` as `this`
    if (val.fieldaccess) {
      assert(val.fieldaccess);
      val.fldobjLoad!true;
      assert(val.fldobj.value.type.isActor);
      assert(val.fldobj.value.slot);
      fci.prependArg(Value.makeStack(val.fldobj.value.slot, val.fldobj.value.type, val.loc));
      return true;
    }
    // for methods, use `this` arg
    if (curfunc.isMethod) {
      auto xthis = findLocalVar("this");
      if (xthis is null) compileErrorAt(val.loc, "`this` not found for method '"~val.varname~"'");
      assert(xthis.type);
      fci.prependArg(Value.makeVar(xthis, val.loc));
      return true;
    }
    // otherwise fail
    return false;
  }

  // try to resolve overloads if `val` is `.isFunc`
  if (val.type.isFunc) {
    auto fp = val.varname in funclist;
    if (fp is null) compileErrorAt(val.loc, "'"~val.varname~"' is not a function");
    Variable var = *fp;
    // find function
    auto fnidx = var.findOverload!false(fci.targs);
    if (fnidx < 0 && prependThisOrActId()) fnidx = var.findOverload!false(fci.targs);
    if (fnidx == Variable.NotFound) compileErrorAt(val.loc, "cannot find suitable overloaded function for '"~val.varname~"'");
    if (fnidx == Variable.Conflict) {
      var.findOverload!true(fci.targs);
      compileErrorAt(val.loc, "overload conflict for '"~val.varname~"'");
    }
    assert(fnidx >= 0 && fnidx < var.ovloads.length);
    // load function address
    auto fif = &var.ovloads[fnidx];
    if (fif.pc >= 0) {
      if (fif.type.hasrest) assert(0, "internal compiler error");
      // vm call
      if (fif.pc > ushort.max) compileErrorAt(val.loc, "invalid function address");
      rv = Value.makeInt(fif.pc, val.loc).load;
      // add fixup
      if (fif.pc == 0) vmcallfixes ~= VMCallFixup(var, fnidx, cast(uint)vmcode.length-1, val.loc);
    } else {
      // builtin call
      rv = Value.makeInt(fif.pc, val.loc).load;
      hasrest = fif.type.hasrest;
    }
    // real function type
    cftype = cast(TypeFunc)fif.type;
    assert(cftype !is null);
  } else {
    // this must be funcptr
    if (!val.type.isFuncPtr) compileErrorAt(val.loc, "trying to call something strange");
    cftype = cast(TypeFunc)val.type.base;
    assert(cftype !is null);
    // check if we can call this
    if (!cftype.callCompatibleNoAux(fci.targs)) {
      // try to prepend `this`
      bool error = true;
      if (prependThisOrActId()) error = !cftype.callCompatibleNoAux(fci.targs);
      if (error) {
        compilerMessage("ERROR args: %s", fci.targs.toPrettyString);
        compileErrorAt(val.loc, "invalid arguments for funcptr: "~cftype.toPrettyString);
      }
    }
    // load address
    rv = val.load;
  }

  assert(rv.slot);

  // append defaults
  if (fci.targs.args.length < cftype.args.length) {
    foreach (ref TypeFunc.Arg a; cftype.args[fci.targs.args.length..$]) {
      if (!a.defval.literal) assert(0, "internal compiler error");
      fci.putArg(a.defval);
    }
  }
  fci.targs.aux = cftype.aux;

  // generate call
  Value fargv; // result will be placed here
  if (hasrest) {
    // builtin with varargs
    if (fci.argc == 0) {
      // we need this to store return value anyway
      fargv = Value.makeTempBlock(fci.targs.aux, 1, val.loc);
      emitCode(VMOp(OpCode.Call, rv.slot, fargv.slot, 0));
    } else {
      int extra = fci.argc-cast(int)cftype.args.length;
      int rqslots = cast(int)cftype.args.length+extra*2;
      if (rqslots > 128) compileErrorAt(val.loc, "too many varargs");
      // allocate slots
      fargv = Value.makeTempBlock(fci.targs.aux, rqslots, val.loc);
      // `fargv` slot is refed, others aren't
      int slotpos = fargv.slot+rqslots-1;
      // copy args
      Slot[FunCallInfo.MaxArgCount*2] sst; // array access can require additional temporary slots, so store all used slots here
      foreach (immutable aidx, ref Value a; fci.argv[0..fci.argc]) {
        assert(aidx*2+1 < sst.length);
        if (aidx >= cftype.args.length) {
          // put type, then arg
          assert(slotpos >= fargv.slot);
          assert(curslots[slotpos] == 0);
          // reserve slot
          sst[aidx*2] = Slot.acquire(slotpos--);
          Value.makeInt(putVAType(a.type), a.loc).storeToSlot(sst[aidx*2]);
        }
        assert(slotpos >= fargv.slot);
        assert((slotpos == fargv.slot.idx ? (curslots[slotpos] == 1) : (curslots[slotpos] == 0)));
        sst[aidx*2+1] = Slot.acquire(slotpos--);
        a = a.storeToSlot(sst[aidx*2+1]); // store a, so argument slots will be occupied
      }
      assert(slotpos == fargv.slot-1);
      emitCode(VMOp(OpCode.Call, rv.slot, fargv.slot, cast(ubyte)rqslots));
    }
  } else if (fci.argc == 0) {
    // we need this to store return value anyway
    fargv = Value.makeTempBlock(fci.targs.aux, 1, val.loc);
    emitCode(VMOp(OpCode.Call, rv.slot, fargv.slot, 0));
  } else {
    // allocate slots
    fargv = Value.makeTempBlock(fci.targs.aux, fci.argc, val.loc);
    // `fargv` slot is refed, others aren't
    // copy args
    foreach (immutable aidx, ref Value a; fci.argv[0..fci.argc]) {
      if (a.type.isStruct) {
        // structs always passed by reference
        a = a.storePtrToSlot(Slot.acquire(cast(int)(fargv.slot+fci.argc-aidx-1))); // store a, so argument slots will be occupied
      } else {
        a = a.storeToSlot(Slot.acquire(cast(int)(fargv.slot+fci.argc-aidx-1))); // store a, so argument slots will be occupied
      }
    }
    emitCode(VMOp(OpCode.Call, rv.slot, fargv.slot, cast(ubyte)fci.argc));
  }
  // result
  if (fci.targs.aux.isVoid) return Value.makeVoid(val.loc);
  assert(fargv.stack);
  assert(fargv.type.same(fci.targs.aux));
  return fargv;
}


// `val.var is null` means "calling function field"; ufcs0 is Actor
Value parseFunCall(bool allowIsAssign=true) (Value val, Value ufcs0=Value.init) {
  // `is` or `!is`?
  auto isloc = token.loc;

  static if (allowIsAssign) {
    if (token.delim("!") || token.kw(Token.T.Is)) {
      // null check
      if (ufcs0.valid) compileError("invalid 'is' check");
      //TODO: add `a is b` support
      bool isnull = token.kw(Token.T.Is);
      if (token.delim("!")) {
        skipToken();
        if (!token.kw(Token.T.Is)) compileError("'is' expected");
      }
      assert(token.kw(Token.T.Is));
      skipToken();
      if (!token.kw(Token.T.Null)) compileError("'null' expected");
      skipToken();
      // generate check
      if (val.type.isFunc) {
        // assume that function is never `null`
        return Value.makeBool(!isnull, val.loc);
      }
      if (!val.type.isPointer) compileErrorAt(val.loc, "pointer expected for `null` checks");
      val = val.load!"bool";
      assert(val.type.isBool);
      if (isnull) emitCode(VMOp(OpCode.LogNot, val.slot, val.slot));
      return val;
    }
  }

  FunCallInfo fci;
  fci.start();

  // put UFCS arguments
  if (ufcs0.valid) fci.putArg(ufcs0);

  // a.b = c
  static if (allowIsAssign) {
    if (token.delim("=")) {
      auto loc = token.loc;
      skipToken();
      auto a2 = parseExpr();
      if (a2.type.isFuncPtr || a2.type.isNull) {
        // assign function pointer to something
        if (!val.assign(a2)) compileErrorAt(loc, "cannot assign");
        return val;
      } else if (a2.type.isPointer) {
        compileErrorAt(loc, "invalid pointer type");
        assert(0);
      }
      fci.putArg(a2);
    }
  }

  // parse other args
  if (token.delim("(")) {
    skipToken();
    while (!token.delim(")")) {
      auto aloc = token.loc;
      auto a = parseExpr();
      fci.putArg(a);
      if (!token.delim(",")) break;
      skipToken();
    }
    if (!token.delim(")")) compileError("')' expected");
    skipToken();
  }

  return cgFunCall(val, fci);
}


// UFCS and field access support
Value parseDot (Value val, bool allowufcs) {
  if (!token.delim(".")) return val; // just in case
  skipToken();
  // builtin properties for strings
  if (val.type.isStr) {
    if (token.id("length")) {
      skipToken();
      if (val.literal) return Value.makeInt(cast(int)strlist[val.lit].length, val.loc);
      val = val.load;
      auto res = Value.makeTemp(typeInt, val.loc);
      emitCode(VMOp(OpCode.StrLen, res.slot, val.slot));
      return res;
    }
  }
  // builtin properties for arrays
  //TODO: dynamic arrays?
  if (val.type.isArray) {
    if (token.id("length")) {
      skipToken();
      auto atp = cast(TypeArray)val.type;
      assert(atp !is null);
      return Value.makeInt(atp.size, val.loc);
    }
  }
  // builtin properties for enums
  if (val.type.isEnum) {
    if (token.id("min")) {
      skipToken();
      auto etp = cast(TypeEnum)val.type;
      assert(etp !is null);
      return Value.makeEnum(etp, etp.minval, val.loc);
    }
    if (token.id("max")) {
      skipToken();
      auto etp = cast(TypeEnum)val.type;
      assert(etp !is null);
      return Value.makeEnum(etp, etp.maxval, val.loc);
    }
    if (token.kw(Token.T.Default) || token.id("init")) {
      skipToken();
      auto etp = cast(TypeEnum)val.type;
      assert(etp !is null);
      return Value.makeEnumDefault(etp, val.loc);
    }
  }
  // dot access for struct?
  if (val.type.isStruct || (val.type.isArray && val.type.base.isStruct)) {
    if (val.type.isArray && !val.arrayaccess) compileError("did you forgot to index your array?");
    if (!token.id) compileError("identifier expected");
    auto xloc = token.loc;
    auto stp = cast(TypeStruct)(val.type.isStruct ? val.type : val.type.base);
    assert(stp !is null);
    auto mbr = stp.getMember(token.sval);
    //if (!mbr.valid) compileError("undefined struct field '"~token.sval~"'");
    //UFCS
    if (mbr.valid) {
      skipToken();
      if (val.argument) {
        // argument
        assert(val.ptrref); // structs can be passed only by reference for now
        assert(!val.arrayaccess);
        assert(!val.fieldaccess);
        val.ptridx += mbr.ofs;
        val.type = mbr.type;
        return val;
      } else if (val.fieldaccess) {
        // field: struct or array of structs
        val.index += mbr.ofs;
        val.type = mbr.type;
        return val;
      } else {
        assert(!val.ptrref);
        assert(mbr.ofs >= 0);
        if (mbr.ofs != 0) {
          if (val.local || val.stack) {
            // shift slot
            assert(val.slot);
            val.slot = Slot.acquire(val.slot+mbr.ofs);
          } else if (val.global) {
            // shift index
            val.index += mbr.ofs;
          } else {
            assert(0, "compiler doesn't know what to do");
          }
        }
        //HACK: force field type
        val.type = mbr.type;
        return val;
      }
    }
  }
  // other
  if (!token.id) compileError("identifier expected");
  Variable fvar = null;
  if (auto fld = actorFindFieldPtr(token.sval)) {
    // field access
    if (!val.type.isActor) compileErrorAt(val.loc, "cannot access fields of non-actor");
    auto res = Value.makeField(val, *fld, val.loc);
    skipToken();
    if (allowufcs && res.type.isCallable) return parseFunCall(res);
    return res;
  } else if (auto fp = token.sval in funclist) {
    // UFCS function call
    fvar = *fp;
  } else {
    // function variable
    fvar = findLocalVar(token.sval);
    if (fvar is null) fvar = findGlobalVar(token.sval);
    if (fvar !is null) {
      if (!fvar.type.isCallable) compileError("variable '"~token.sval~"' is not a function");
    }
  }
  if (fvar is null) compileError("function '"~token.sval~"' not found");
  // should be fcall or null check
  Value fnv = Value.makeVar(fvar, token.loc);
  skipToken();
  return (allowufcs ? parseFunCall(fnv, val) : fnv);
}


Value parseIndexing (Value val, bool allowufcs) {
  auto stloc = token.loc;
  if (!val.type.isArray && !val.type.isStr) compileError("trying to index something that is not an array");
  if (val.arrayaccess) compileError("arrays of arrays aren't supported yet");
  skipToken();
  auto vidx = parseExpr();
  if (!token.delim("]")) compileError("']' expected");
  skipToken();
  if (!vidx.type.isInt) compileErrorAt(stloc, "integer index expected");
  // generate bounds checking here, 'cause why not?
  if (val.type.isArray) {
    int arrsize = val.type.cellSize/val.type.base.cellSize;
    assert(arrsize > 0 && arrsize <= ushort.max);
    if (vidx.literal) {
      if (vidx.lit < 0 || vidx.lit >= arrsize) {
        compileErrorAt(stloc, "array index out of bounds");
      }
    }
    //HACK: transform into array access
    val.elemsize = val.type.base.cellSize;
    val.arridx = vidx;
    val.type = val.type.base;
    val.arrsize = arrsize;
    return val;
  } else {
    assert(val.type.isStr);
    //HACK: transform into array access
    val.elemsize = 1;
    val.arridx = vidx;
    val.type = typeInt; // oops
    val.arrsize = -666; //HACK: special flag
    return val;
  }
}


Value parseExprPostfix (Value val, bool allowufcs=true) {
  //stderr.writeln(allowufcs, ":val=", val, "; type=", val.type.toPrettyString);
  // paren-less function call
  if (val.type.isCallable && allowufcs && !token.delim("(")) val = parseFunCall(val);
  // other similar things
  for (;;) {
    if (!token.delim) break;
         if (token.sval == ".") val = parseDot(val, allowufcs);
    else if (token.sval == "(") val = parseFunCall(val);
    else if (token.sval == "[") val = parseIndexing(val, allowufcs);
    else break;
  }
  // postfix inc/dec
  if (token.delim("++") || token.delim("--")) {
    bool isPP = token.delim("++");
    auto loc = token.loc;
    skipToken();
    if (!val.local && !val.argument && !val.global && !val.field) compileErrorAt(val.loc, "cannot perform postfix inc/dec on non-var");
    if (val.readonly) compileError("cannot assign to read-only variable");
    if (!val.type.isInt) compileErrorAt(val.loc, "cannot perform postfix inc/dec on non-int");
    auto res = Value.makeTemp(val);
    res = val.storeToSlot(res.slot);
    if (val.local && val.slot.valid && !val.arrayaccess && !val.fieldaccess && !val.ptrref) {
      // do direct inc/dec
      emitCode(VMOp((isPP ? OpCode.Inc : OpCode.Dec), val.slot, val.slot));
    } else {
      auto res1 = Value.makeTemp(val);
      emitCode(VMOp((isPP ? OpCode.Inc : OpCode.Dec), res1.slot, res.slot));
      if (!val.assign(res1)) compileErrorAt(val.loc, "cannot perform postfix inc/dec");
    }
    val = res;
  }
  return val;
}


Value parseExprUnary (bool allowufcs=true) {
  Value res;
  if (token.delim) {
    if (token.sval == "+") {
      auto loc = token.loc;
      skipToken();
      res = parseExprUnary(allowufcs);
      if (!res.type.isInt) compileErrorAt(loc, "invalid unary math");
    } else if (token.sval == "-") {
      auto loc = token.loc;
      skipToken();
      res = parseExprUnary(allowufcs);
      if (!res.type.isInt) compileErrorAt(loc, "invalid unary math");
      if (res.literal) {
        res.lit = -res.lit;
      } else {
        // generate neg
        res = res.load;
        auto zero = Value.makeInt(0, loc).load;
        emitCode(VMOp(OpCode.Sub, res.slot, zero.slot, res.slot));
      }
    } else if (token.sval == "!") {
      auto loc = token.loc;
      skipToken();
      res = parseExprUnary(allowufcs);
      if (!res.type.isInt && !res.type.isBool && !res.type.isStr && !res.type.isActor) compileErrorAt(loc, "invalid unary math");
      if (res.literal) {
        res = Value.makeBool((res.lit == 0), res.loc);
      } else {
        // generate lognot
        res = res.load;
        emitCode(VMOp(OpCode.LogNot, res.slot, res.slot));
      }
    } else if (token.sval == "&") {
      auto loc = token.loc;
      skipToken();
      res = parseExprUnary(false);
      //stderr.writeln("func:", res.type.isFunc, "; funcptr:", res.type.isFuncPtr, "; ", res.type.toPrettyString, " -- ", res);
      if (!res.type.isCallable) compileErrorAt(loc, "cannot take address of non-function");
      // &fnvar means "take address of containing function" (i.e. just load variable value)
      if (res.type.isFuncPtr) {
        //TODO: "&&func" should fail
        //res.type = new TypePointer(res.type); //HACK
      } else {
        assert(res.type.isFunc);
        Variable var = findFunction(res);
        if (var is null) compileErrorAt(loc, "undefined function '"~res.varname~"'");
        if (var.ovloads.length == 0) assert(0, "something is very wrong in the compiler");
        if (var.ovloads.length != 1) compileErrorAt(loc, "cannot take address of overload set '"~var.name~"'");
        // load function address
        enum fnidx = 0;
        if (var.ovloads[fnidx].pc >= 0) {
          if (var.ovloads[fnidx].type.hasrest) compileErrorAt(loc, "cannot take address of vararg builtin '"~var.name~"'");
          // vm
          if (var.ovloads[fnidx].pc > ushort.max) compileErrorAt(loc, "invalid function address");
          //FIXME: leave it as literal, and genetate fixups in `load`
          res = Value.makeInt(var.ovloads[fnidx].pc, loc).load;
          // add fixup
          if (var.ovloads[fnidx].pc == 0) vmcallfixes ~= VMCallFixup(var, fnidx, cast(uint)vmcode.length-1, loc);
        } else {
          // builtin
          res = Value.makeInt(var.ovloads[fnidx].pc, loc).load;
        }
        //HACK
        res.type = new TypePointer(var.type);
      }
      allowufcs = false; // don't call function pointer
      assert(res.type.isPointer);
    } else {
      res = parseExprPrimary();
    }
    // bitnot, bitneg, etc.
  } else {
    res = parseExprPrimary();
  }

  return parseExprPostfix(res, allowufcs);
}


// ////////////////////////////////////////////////////////////////////////// //
Value cgNumMath(OpCode opc, string mathop) (Value lhs, Value rhs) {
  if (!lhs.type.isInt) compileErrorAt(lhs.loc, "number expected");
  if (!rhs.type.isInt) compileErrorAt(rhs.loc, "number expected");
  static if (opc == OpCode.Div || opc == OpCode.Mod) {
    if (rhs.literal && rhs.lit == 0) compileErrorAt(rhs.loc, "division by zero");
  }
  // inline math
  if (lhs.literal && rhs.literal) {
    Value res = lhs;
    res.lit = mixin("lhs.lit"~mathop~"rhs.lit");
    return res;
  }
  // check for other math optimizations
  if (rhs.literal) {
    static if (opc == OpCode.Mul) {
      if (rhs.lit == 1) return lhs;
      if (rhs.lit == 0) return Value.makeInt(0, lhs.loc);
    }
    static if (opc == OpCode.Add || opc == OpCode.Sub) {
      if (rhs.lit == 0) return lhs;
    }
    static if (opc == OpCode.Div) {
      if (rhs.lit == 1) return lhs;
    }
    static if (opc == OpCode.Mod) {
      if (rhs.lit == 1) return Value.makeInt(0, lhs.loc);
    }
  } else if (lhs.literal) {
    static if (opc == OpCode.Mul) {
      if (lhs.lit == 1) return rhs;
      if (lhs.lit == 0) return Value.makeInt(0, lhs.loc);
    }
    static if (opc == OpCode.Add) {
      if (lhs.lit == 0) return lhs;
    }
    static if (opc == OpCode.Div) {
      if (lhs.lit == 0) return Value.makeInt(0, lhs.loc);
    }
    static if (opc == OpCode.Mod) {
      if (lhs.lit == 0) return Value.makeInt(0, lhs.loc);
    }
  }
  // generate mathop
  lhs = lhs.load;
  rhs = rhs.load;
  auto res = Value.makeTemp(lhs);
  emitCode(VMOp(opc, res.slot, lhs.slot, rhs.slot));
  return res;
}

alias cgIAdd = cgNumMath!(OpCode.Add, "+");
alias cgISub = cgNumMath!(OpCode.Sub, "-");
alias cgIMul = cgNumMath!(OpCode.Mul, "*");
alias cgIDiv = cgNumMath!(OpCode.Div, "/");
alias cgIMod = cgNumMath!(OpCode.Mod, "%");


// ////////////////////////////////////////////////////////////////////////// //
Value cgIShlShr(OpCode opc) (Value lhs, Value rhs) {
  if (!lhs.type.isInt) compileErrorAt(lhs.loc, "number expected");
  if (!rhs.type.isInt) compileErrorAt(rhs.loc, "number expected");
  if (rhs.literal) {
    if (rhs.lit < 0) compileErrorAt(rhs.loc, "negative shift is not defined");
    if (rhs.lit == 0) return lhs; // nothing to do
    if (rhs.lit > 31) compileErrorAt(rhs.loc, "allowed shift range is [0..31]");
  }
  // inline math
  if (lhs.literal && rhs.literal) {
         static if (opc == OpCode.Shl) lhs.lit <<= rhs.lit;
    else static if (opc == OpCode.Shr) lhs.lit >>= rhs.lit;
    else static assert(0, "wtf?!");
    return lhs;
  }
  // generate mathop
  lhs = lhs.load;
  rhs = rhs.load;
  auto res = Value.makeTemp(lhs);
  emitCode(VMOp(opc, res.slot, lhs.slot, rhs.slot));
  return res;
}

alias cgIShl = cgIShlShr!(OpCode.Shl);
alias cgIShr = cgIShlShr!(OpCode.Shr);


// ////////////////////////////////////////////////////////////////////////// //
Value cgBitOp(OpCode opc) (Value lhs, Value rhs) {
  if (!lhs.type.isInt) compileErrorAt(lhs.loc, "number expected");
  if (!rhs.type.isInt) compileErrorAt(rhs.loc, "number expected");
  if (rhs.literal) {
    if (rhs.lit == 0) {
      static if (opc == OpCode.BitOr || opc == OpCode.BitXor) {
        return lhs;
      } else {
        static assert(opc == OpCode.BitAnd);
        return Value.makeInt(0, lhs.loc);
      }
    } else if (rhs.lit == -1) {
      static if (opc == OpCode.BitOr || opc == OpCode.BitXor) {
        return Value.makeInt(-1, lhs.loc);
      } else {
        static assert(opc == OpCode.BitAnd);
        return lhs;
      }
    }
  } else if (lhs.literal) {
    if (lhs.lit == 0) {
      static if (opc == OpCode.BitOr || opc == OpCode.BitXor) {
        return rhs;
      } else {
        static assert(opc == OpCode.BitAnd);
        return Value.makeInt(0, lhs.loc);
      }
    } else if (lhs.lit == -1) {
      static if (opc == OpCode.BitOr || opc == OpCode.BitXor) {
        return Value.makeInt(-1, lhs.loc);
      } else {
        static assert(opc == OpCode.BitAnd);
        return rhs;
      }
    }
  }
  // inline math
  if (lhs.literal && rhs.literal) {
         static if (opc == OpCode.BitAnd) lhs.lit &= rhs.lit;
    else static if (opc == OpCode.BitOr) lhs.lit |= rhs.lit;
    else static if (opc == OpCode.BitXor) lhs.lit ^= rhs.lit;
    else static assert(0, "wtf?!");
    return lhs;
  }
  // generate mathop
  lhs = lhs.load;
  rhs = rhs.load;
  auto res = Value.makeTemp(lhs);
  emitCode(VMOp(opc, res.slot, lhs.slot, rhs.slot));
  return res;
}

alias cgBitAnd = cgBitOp!(OpCode.BitAnd);
alias cgBitOr = cgBitOp!(OpCode.BitOr);
alias cgBitXor = cgBitOp!(OpCode.BitXor);


// ////////////////////////////////////////////////////////////////////////// //
Value cgEquals(OpCode opc) (Value lhs, Value rhs) if (opc == OpCode.Equ || opc == OpCode.NotEqu) {
  if (lhs.literal && rhs.literal) {
    static if (opc == OpCode.Equ) {
      return Value.makeBool(lhs.lit == rhs.lit, lhs.loc);
    } else {
      return Value.makeBool(lhs.lit != rhs.lit, lhs.loc);
    }
  }
  // generate code
  lhs = lhs.load;
  rhs = rhs.load;
  auto res = Value.makeTemp(typeBool, lhs.loc);
  emitCode(VMOp(opc, res.slot, lhs.slot, rhs.slot));
  return res;
}

alias cgEqu = cgEquals!(OpCode.Equ);
alias cgNotEqu = cgEquals!(OpCode.NotEqu);


Value cgCompare(string op) (Value lhs, Value rhs) if (op == "<" || op == ">" || op == "<=" || op == ">=") {
  if (!lhs.type.isInt && !lhs.type.isStr) compileErrorAt(lhs.loc, "invalid type");
  if (!rhs.type.isInt && !rhs.type.isStr) compileErrorAt(rhs.loc, "invalid type");
  if (!lhs.type.same(rhs.type)) compileErrorAt(lhs.loc, "cannot compare different types");
  // literals
  if (lhs.literal && rhs.literal) {
    if (lhs.type.isInt) {
      return Value.makeBool(mixin("lhs.lit"~op~"rhs.lit"), lhs.loc);
    } else {
      return Value.makeBool(mixin("strlist[lhs.lit]"~op~"strlist[rhs.lit]"), lhs.loc);
    }
  }
  // generate code
  lhs = lhs.load;
  rhs = rhs.load;
  auto res = Value.makeTemp(typeBool, lhs.loc);
  OpCode opc;
       static if (op == "<") opc = (lhs.type.isInt ? OpCode.ILess : OpCode.SLess);
  else static if (op == ">") opc = (lhs.type.isInt ? OpCode.IGreat : OpCode.SGreat);
  else static if (op == "<=") opc = (lhs.type.isInt ? OpCode.ILessEqu : OpCode.SLessEqu);
  else static if (op == ">=") opc = (lhs.type.isInt ? OpCode.IGreatEqu : OpCode.SGreatEqu);
  else static assert(0, "wtf?!");
  emitCode(VMOp(opc, res.slot, lhs.slot, rhs.slot));
  return res;
}

alias cgLess = cgCompare!"<";
alias cgGreat = cgCompare!">";
alias cgLEq = cgCompare!"<=";
alias cgGEq = cgCompare!">=";


// ////////////////////////////////////////////////////////////////////////// //
Value cgLogOp(OpCode opc) (Value lhs) if (opc == OpCode.LogAnd || opc == OpCode.LogOr) {
  // &&: parseExprCmp()
  // ||: parseExprLogAnd()
  static if (opc == OpCode.LogAnd) alias nextExpr = parseExprCmp; else alias nextExpr = parseExprLogAnd;
  static if (opc == OpCode.LogAnd) alias JumpOp = OpCode.JFalse; else alias JumpOp = OpCode.JTrue;
  // check lhs type
  if (!lhs.type.isGoodForBool) compileErrorAt(lhs.loc, "invalid type");
  // optimize some known cases
  if (lhs.literal) {
    enum LLMode { Nop, DropRHS, DropLHS }
    LLMode llmode = LLMode.Nop;
    static if (opc == OpCode.LogAnd) {
      // &&
      if (!lhs.lit) {
        // false && smth
        lhs = Value.makeBool(false, lhs.loc);
        llmode = LLMode.DropRHS;
      } else {
        // true && smth
        llmode = LLMode.DropLHS;
      }
    } else {
      // ||
      if (!lhs.lit) {
        // false || smth
        llmode = LLMode.DropLHS;
      } else {
        // true || smth
        lhs = Value.makeBool(true, lhs.loc);
        llmode = LLMode.DropRHS;
      }
    }
    auto cgmark = JumpChain.startHere(); // we'll unwind to this point
    auto rhs = parseExprCmp();
    // check rhs type
    if (!rhs.type.isGoodForBool) compileErrorAt(rhs.loc, "invalid type");
    final switch (llmode) {
      case LLMode.Nop:
        assert(0, "internal compiler error");
      case LLMode.DropRHS:
        cgmark.unwindToBase();
        return lhs;
      case LLMode.DropLHS:
        if (rhs.type.isBool) return rhs;
        return (rhs.literal ? Value.makeBool(rhs.lit != 0, rhs.loc) : rhs.load!"bool");
    }
  }
  // mark current code point, so we can rewind if RHS is literal
  auto rewindpc = JumpChain.startHere();
  // create storage for result
  auto res = Value.makeTemp(typeBool, lhs.loc);
  assert(res.stack);
  // load value
  lhs.storeToSlot!"bool"(res.slot);
  // generate jump and do rhs
  auto jumpc = rewindpc.startForward();
  jumpc.putJump(JumpOp, res.slot);
  // get RHS
  auto rhs = nextExpr();
  // check rhs type
  if (!rhs.type.isGoodForBool) compileErrorAt(rhs.loc, "invalid type");
  // optimize literal case
  if (rhs.literal) {
    enum RLMode { Nop, RewindAll, RewindJump }
    RLMode rlmode = RLMode.Nop;
    static if (opc == OpCode.LogAnd) {
      // &&
      if (!rhs.lit) {
        // smth && false: rewind everything
        rlmode = RLMode.RewindAll;
        rhs = Value.makeBool(false, rhs.loc);
      } else {
        // smth && true: rewind jump
        rlmode = RLMode.RewindJump;
      }
    } else {
      // ||
      if (!lhs.lit) {
        // smth || false: rewind jump
        rlmode = RLMode.RewindJump;
      } else {
        // smth || true: rewind everything
        rlmode = RLMode.RewindAll;
        rhs = Value.makeBool(true, rhs.loc);
      }
    }
    final switch (rlmode) {
      case RLMode.Nop:
        assert(0, "internal compiler error");
      case RLMode.RewindAll: // and return rhs
        rewindpc.unwindToBase();
        return rhs;
      case RLMode.RewindJump: // and return res
        jumpc.unwindToBase();
        return res;
    }
  }
  // load rhs to result slot
  rhs.storeToSlot!"bool"(res.slot);
  // patch jump
  jumpc.finishHere();
  // done
  return res;
}

alias cgLogAnd = cgLogOp!(OpCode.LogAnd);
alias cgLogOr = cgLogOp!(OpCode.LogOr);


// ////////////////////////////////////////////////////////////////////////// //
string BuildExprBinOp(string me, string upfunc, T...) () {
  string res = `Value parseExpr`~me~` () {`;
  res ~= `return parseAnyBinOp!("parseExpr`~me~`", "parseExpr`~upfunc~`"`;
  foreach (immutable idx, string s; T[]) {
    res ~= ",";
    static if (idx%2 == 0) {
      res ~= "`"~s~"`";
    } else {
      res ~= s;
    }
  }
  res ~= ")();";
  res ~= "}";
  return res;
}


// name ends with 'R': right-associative
//                     name      upfunc     delim,func
mixin(BuildExprBinOp!("Mul",    "Unary",   "*", "cgIMul", "/", "cgIDiv", "%", "cgIMod"));
mixin(BuildExprBinOp!("Add",    "Mul",     "+", "cgIAdd", "-", "cgISub")); // binop `~` is here too, but we don't have it
mixin(BuildExprBinOp!("Shift",  "Add",     "<<", "cgIShl", ">>", "cgIShr"));
mixin(BuildExprBinOp!("BitAnd", "Shift",   "&", "cgBitAnd"));
mixin(BuildExprBinOp!("BitOr",  "BitAnd",  "|", "cgBitOr", "^", "cgBitXor"));
mixin(BuildExprBinOp!("Cmp",    "BitOr",   "<", "cgLess", ">", "cgGreat", "==", "cgEqu", "!=", "cgNotEqu", "<=", "cgLEq", ">=", "cgGEq")); // `a is b`, `a in b` are here too
mixin(BuildExprBinOp!("LogAnd", "Cmp",     "&&", "cgLogAnd"));
mixin(BuildExprBinOp!("LogOr",  "LogAnd",  "||", "cgLogOr"/*, "^^", "cgLogXor"*/));


Value parseExpr () {
  // we'll do ternaries here
  auto eloc = token.loc;
  auto res = parseExprLogOr();
  if (token.delim("?")) {
    if (!res.type.isGoodForBool) compileErrorAt(eloc, "boolean-compatible expression expected");
    skipToken(); // skip '?'
    auto jumptofalse = JumpChain.startForward();
    // dead code elimination
    if (res.literal) {
      if (res.lit) {
        // ignore false part
        if (token.delim(":")) {
          // "?:"
          skipToken(); // skip ':'
          auto efloc = token.loc;
          auto ef = parseExpr(); // parse false part
          if (!TypeFunc.compatible(res.type, ef.type) && !TypeFunc.compatible(ef.type, res.type)) compileErrorAt(efloc, "incompatible types");
          // unwind, and return literal
          jumptofalse.unwindToBase();
          return res;
        } else {
          auto etloc = token.loc;
          auto et = parseExpr();
          jumptofalse = JumpChain.startHere(); // mark unwind point
          if (!token.delim(":")) compileError("':' expected");
          skipToken();
          auto efloc = token.loc;
          auto ef = parseExpr(); // parse false part
          if (!TypeFunc.compatible(et.type, ef.type) && !TypeFunc.compatible(ef.type, et.type)) compileErrorAt(efloc, "incompatible types");
          // unwind, and return true part
          jumptofalse.unwindToBase();
          return et;
        }
      } else {
        // ignore true part
        if (token.delim(":")) {
          // "?:"
          skipToken(); // skip ':'
          auto efloc = token.loc;
          return parseExpr(); // parse false part
        } else {
          auto etloc = token.loc;
          auto et = parseExpr();
          if (!token.delim(":")) compileError("':' expected");
          skipToken();
          jumptofalse.unwindToBase(); // unwind true part
          auto efloc = token.loc;
          auto ef = parseExpr(); // parse false part
          if (!TypeFunc.compatible(et.type, ef.type) && !TypeFunc.compatible(ef.type, et.type)) compileErrorAt(efloc, "incompatible types");
          return ef; // return false part
        }
      }
    }
    // load condition
    if (!res.stack) res = res.storeToSlot(Slot.makeTemp);
    assert(res.slot);
    if (token.delim(":")) {
      // '?:'
      skipToken();
      // generate jump over false (sorry for misleading name)
      jumptofalse.putJump(OpCode.JTrue, res.slot);
      // compile false part
      auto efloc = token.loc;
      auto ef = parseExpr(); // parse false part
      if (!TypeFunc.compatible(res.type, ef.type) && !TypeFunc.compatible(ef.type, res.type)) compileErrorAt(efloc, "incompatible types");
      // put this to res
      ef.storeToSlot(res.slot);
      jumptofalse.finishHere();
    } else {
      // generate jump to false part
      jumptofalse.putJump(OpCode.JFalse, res.slot);
      // compile true part
      auto etloc = token.loc;
      auto et = parseExpr();
      if (!token.delim(":")) compileError("':' expected");
      skipToken();
      // put this to res
      auto newret = et.storeToSlot(res.slot);
      // generate jump to end
      auto jumpexit = JumpChain.startForward();
      jumpexit.putJump(OpCode.Jump);
      // compile false part
      jumptofalse.finishHere();
      auto efloc = token.loc;
      auto ef = parseExpr(); // parse false part
      if (!TypeFunc.compatible(et.type, ef.type) && !TypeFunc.compatible(ef.type, et.type)) compileErrorAt(efloc, "incompatible types");
      // put this to res
      ef.storeToSlot(res.slot);
      jumpexit.finishHere();
      res = newret;
    }
  }
  return res;
}


// parse function call or assign
Value parseAssExpr () {
  auto unwindmark = JumpChain.startHere();
  auto lhs = parseExpr();
  if (token.delim("=")) {
    if (lhs.literal || lhs.stack) compileError("cannot assign to non-variable");
    if (lhs.readonly) compileError("cannot assign to read-only variable");
    lhs.cachePostponedValues(); // to avoid side effects
    skipToken();
    auto val = parseExpr();
    if (!lhs.assign(val)) compileError("cannot assign");
  } else if (token.delim && token.sval.length == 2 && token.sval[1] == '=') {
    OpCode opc;
    switch (token.sval[0]) {
      case '+': opc = OpCode.Add; break;
      case '-': opc = OpCode.Sub; break;
      case '*': opc = OpCode.Mul; break;
      case '/': opc = OpCode.Div; break;
      case '%': opc = OpCode.Mod; break;
      case '&': opc = OpCode.BitAnd; break;
      case '|': opc = OpCode.BitOr; break;
      case '^': opc = OpCode.BitXor; break;
      default: return lhs;
    }
    if (lhs.literal || lhs.stack) compileError("cannot assign to non-variable");
    if (lhs.readonly) compileError("cannot assign to read-only variable");
    if (!lhs.type.isInt) compileErrorAt(lhs.loc, "integer variable expected");
    lhs.cachePostponedValues(); // to avoid side effects
    skipToken();
    auto val = parseExpr();
    if (!val.type.isInt) compileErrorAt(val.loc, "integer value expected");
    // optimize simple cases
    if (val.literal) {
      if (val.lit == 0 && (opc == OpCode.Div || opc == OpCode.Mod)) compileErrorAt(val.loc, "division by zero");
      if (val.lit == 0 && (opc == OpCode.Add || opc == OpCode.Sub || opc == OpCode.BitOr || opc == OpCode.BitXor)) {
        if (!lhs.arrayaccess && !lhs.fieldaccess) unwindmark.unwindToBase();
        return lhs;
      }
      if (val.lit == 1 && (opc == OpCode.Mul || opc == OpCode.Div)) {
        if (!lhs.arrayaccess && !lhs.fieldaccess) unwindmark.unwindToBase();
        return lhs;
      }
    }
    //TODO: optimize for more literal cases
    if (val.literal && opc == OpCode.Add && val.lit == 1 && lhs.local && lhs.slot.valid && !lhs.arrayaccess && !lhs.fieldaccess && !lhs.ptrref) {
      // +1 for local
      emitCode(VMOp(OpCode.Inc, lhs.slot, lhs.slot));
    } else if (val.literal && opc == OpCode.Add && val.lit == -1 && lhs.local && lhs.slot.valid && !lhs.arrayaccess && !lhs.fieldaccess && !lhs.ptrref) {
      // -1 for local
      emitCode(VMOp(OpCode.Dec, lhs.slot, lhs.slot));
    } else if (val.literal && val.lit == 0 && (opc == OpCode.Add || opc == OpCode.Sub || opc == OpCode.BitOr || opc == OpCode.BitXor) && !lhs.arrayaccess && !lhs.fieldaccess && !lhs.ptrref) {
      // do nothing
    } else if (lhs.local && lhs.slot.valid && !lhs.arrayaccess && !lhs.fieldaccess && !lhs.ptrref) {
      val = val.load;
      emitCode(VMOp(opc, lhs.slot, lhs.slot, val.slot));
    } else {
      auto tmp = lhs.load;
      assert(tmp.slot);
      val = val.load;
      assert(val.slot);
      emitCode(VMOp(opc, tmp.slot, tmp.slot, val.slot));
      if (!lhs.assign(tmp)) compileErrorAt(lhs.loc, "assign failed");
    }
  }
  return lhs;
}


// ////////////////////////////////////////////////////////////////////////// //
TypeFunc parseFuncArgs (bool method) {
  auto stloc = token.loc;
  if (!token.delim("(")) compileError("'(' expected");
  skipToken();
  auto tf = new TypeFunc();
  if (method) {
    // add hidden `this`
    tf.args ~= TypeFunc.Arg("this", typeActor, false);
  }
  while (!token.delim(")")) {
    if (token.empty) compileErrorAt(stloc, "unterminated argument list");
    if (token.delim("...")) { tf.hasrest = true; skipToken(); break; } // dotdotdot must be last one
    bool hasIn = false, hasRef = false;
    for (;;) {
      if (token.kw(Token.T.In)) {
        if (hasIn) compileError("duplicate 'in'");
        hasIn = true;
      } else if (token.kw(Token.T.Ref)) {
        if (hasRef) compileError("duplicate 'ref'");
        hasRef = true;
      } else {
        break;
      }
      skipToken();
    }
    //if (hasIn) compileError("`in` arguments aren't supported yet");
    //TODO: `auto` type (kind of generics)
    TypeBase tp = parseTypeDecl!true();
    if (tp is null) compileError("type name expected");
    if (tp.isVoid) compileError("`void` is not allowed here");
    if (tp.isArray) compileError("arrays are not allowed here");
    if (!tp.isStruct && hasRef) compileError("only structs can be `ref` for now");
    if (tp.isStruct && !hasRef) compileError("struct arguments must be `ref`");
    if (tf.args.length >= 64) compileError("too many function arguments");
    if (!token.id) compileError("identifier expected");
    foreach (const ref arg; tf.args) if (arg.name == token.sval) compileError("duplicate argument name '"~token.sval~"'");
    // convert `isFunc` to funcptr
    if (tp.isFunc) tp = new TypePointer(tp);
    tf.args ~= TypeFunc.Arg(token.sval, tp, hasRef, hasIn);
    skipToken(); // skip argument name
    // has default value?
    if (token.delim("=")) {
      // parse default value
      skipToken();
      auto dvloc = token.loc;
      auto val = parseExpr();
      if (!val.literal) compileErrorAt(dvloc, "default argument must be literal");
      if (val.type.isNull) {
        if (!tp.isActor && !tp.isPointer) compileErrorAt(dvloc, "invalid default argument type");
      } else {
        if (!tp.same(val.type)) compileErrorAt(dvloc, "invalid default argument type");
      }
      tf.args[$-1].defval = val;
    }
    if (!token.delim(",")) break;
    skipToken();
  }
  if (!token.delim(")")) compileError("')' expected");
  skipToken();
  return tf;
}


// ////////////////////////////////////////////////////////////////////////// //
// this also used in `for (auto n = ...` parser
// won't eat terminator (so it should be explicitly checked)
bool parseVarDecl (bool allowMulti, out Value lastres) {
  TypeBase tp = parseTypeDecl!true();
  if (tp is null) return false;
  if (tp.isVoid) compileError("`void` is not allowed here");
  if (tp.isArray && tp.cellSize == 0) compileError("dynamic arrays aren't supported yet");
  if (!token.id) compileErrorAt(token.loc, "identifier expected");
  for (;;) {
    if (!token.id) compileErrorAt(token.loc, "identifier expected");
    auto var = enterVar(token.loc, token.sval, tp);
    bool isfirst = !lastres.valid;
    lastres = Value.makeVar(var, token.loc);
    skipToken();
    // initializer
    Value val;
    bool doStore = true;
    if (token.delim("=")) {
      if (tp.isArray) compileError("local array initializers aren't supported yet");
      skipToken();
      auto iloc = token.loc;
      val = parseExpr();
      if (!lastres.assign(val)) compileErrorAt(iloc, "invalid initializer type");
      if (isfirst && val.literal) lastres = val; // so dead code elimination can work
      doStore = false;
    } else {
           if (tp.isBool) val = Value.makeBool(false, token.loc);
      else if (tp.isInt) val = Value.makeInt(0, token.loc);
      else if (tp.isStr) val = Value.makeStr("", token.loc);
      else if (tp.isActor) val = Value.makeActor(0, token.loc);
      else if (tp.isEnum) val = Value.makeEnumDefault(tp, token.loc);
      else if (tp.isPointer) val = Value.makeNull(token.loc);
      else if (tp.isArray) {
        // local array
        doStore = false;
        // clear slots
        emitCode(VMOp.makeU16(OpCode.LArrClear, Slot.acquire(var.idx), cast(ushort)tp.cellSize));
      } else if (tp.isStruct) {
        // create local struct
        doStore = false;
        // clear slots
        if (tp.cellSize == 0) compileError("cannot create empty struct");
        emitCode(VMOp.makeU16(OpCode.LArrClear, Slot.acquire(var.idx), cast(ushort)tp.cellSize));
      }
      else assert(0, "wtf?!");
    }
    if (doStore) {
      if (!lastres.assign(val)) compileErrorAt(lastres.loc, "invalid initializer type");
      if (isfirst && val.literal) lastres = val; // so dead code elimination can work
    }
    // next one
    if (!allowMulti) break;
    if (!token.delim(",")) break;
    skipToken();
  }
  return true;
}


// this also used in `for (auto n = ...` parser
// won't eat terminator (so it should be explicitly checked)
bool parseAutoDecl (bool allowMulti, out Value lastres) {
  if (!token.kw(Token.T.Auto)) return false;
  skipToken();
  if (!token.id) compileErrorAt(token.loc, "identifier expected");
  for (;;) {
    if (!token.id) compileErrorAt(token.loc, "identifier expected");
    auto vartoken = token;
    skipToken();
    // initializer
    if (!token.delim("=")) compileErrorAt(token.loc, "initializer expected");
    skipToken();
    auto iloc = token.loc;
    Value val = parseExpr();
    if (val.type.isBool || val.type.isInt || val.type.isStr || val.type.isActor || val.type.isEnum || val.type.isFuncPtr) {
      //stderr.writeln("auto (", val.type.cellSize, "): ", val.type.toPrettyString);
      auto var = enterVar(vartoken.loc, vartoken.sval, val.type);
      bool isfirst = !lastres.valid;
      lastres = Value.makeVar(var, vartoken.loc);
      if (!lastres.assign(val)) compileErrorAt(iloc, "invalid initializer type");
      if (isfirst && val.literal) lastres = val; // so dead code elimination can work
    } else {
      compileErrorAt(iloc, "invalid initializer type");
    }
    // next one
    if (!allowMulti) break;
    if (!token.delim(",")) break;
    skipToken();
  }
  return true;
}


// parse condition expression with possible declaration
Value parseExprWithPossibleDecl(string mode) () {
  static assert(mode == "for" || mode == "if", "invalid mode");
  static if (mode == "for") enum allowMulti = true; else enum allowMulti = false;
  Value res;
  // auto or vardecl?
  if (!parseAutoDecl(allowMulti, res) && !parseVarDecl(allowMulti, res)) {
    // not a declaration, parse expression
    static if (mode == "for") {
      res = parseAssExpr();
    } else {
      res = parseExpr();
    }
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// keyword parsers

// we may want to have more info later, so let's create structure now
struct ParseInfo {
  bool returnSeen;
  bool breakSeen;
  bool contSeen;

  void merge() (in auto ref ParseInfo pi) pure nothrow @safe @nogc {
    returnSeen = returnSeen && pi.returnSeen;
    breakSeen = breakSeen || pi.breakSeen;
    contSeen = contSeen || pi.contSeen;
  }

  static ParseInfo makeReturn () nothrow @safe @nogc { pragma(inline, true); return ParseInfo(true); }
  static ParseInfo makeBreak () nothrow @safe @nogc { pragma(inline, true); return ParseInfo(false, true); }
  static ParseInfo makeContinue () nothrow @safe @nogc { pragma(inline, true); return ParseInfo(false, false, true); }
}


ParseInfo parseReturn (in ref Loc statloc) {
  auto eloc = token.loc;
  if (curfunc.type !is null && !curfunc.type.aux.isVoid) {
    auto val = parseExpr();
    if (!curfunc.type.aux.same(val.type)) {
      // return type conversion
      if (curfunc.type.aux.isBool) {
        // any -> bool
        val.storeToSlot!"bool"(Slot.acquire(0));
      } else if (val.type.isBool) {
        if (!curfunc.type.aux.isInt) compileErrorAt(eloc, "invalid return type");
        val.storeToSlot(Slot.acquire(0));
      } else if (val.type.isNull) {
        if (!curfunc.type.aux.isPointer && !curfunc.type.aux.isActor) compileErrorAt(eloc, "invalid return type");
        val = Value.makeInt(0, val.loc);
        //HACK
        val.type = curfunc.type.aux;
        val.storeToSlot(Slot.acquire(0));
      }
    } else {
      val.storeToSlot(Slot.acquire(0));
    }
  }
  emitCode(VMOp(OpCode.Ret));
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  return ParseInfo.makeReturn();
}


ParseInfo parseIf (in ref Loc statloc) {
  // parse condition
  ParseInfo pires;
  auto eloc = token.loc;
  if (!token.delim("(")) compileError("'(' expected");
  skipToken();
  enterScope(); // why not? it is easier this way
  scope(exit) leaveScope();
  auto val = parseExprWithPossibleDecl!"if"();
  if (!token.delim(")")) compileError("')' expected");
  skipToken();
  if (!val.type.isGoodForBool) compileErrorAt(eloc, "boolean expression expected");
  // dead code elimination
  if (val.literal) {
    // true branch
    auto rewindpc = JumpChain.startHere();
    pires = parseStatement();
    // rewind true branch if condition is false
    if (!val.lit) {
      rewindpc.unwindToBase();
      // ...and reset flags
      pires = ParseInfo.init;
    }
    // has `else`?
    if (token.kw(Token.T.Else)) {
      // false branch
      rewindpc = JumpChain.startHere();
      skipToken();
      auto pri = parseStatement();
      // rewind false branch if condition is false
      if (val.lit) {
        rewindpc.unwindToBase();
      } else {
        // ...or use it's ParserInfo as result
        pires = pri;
      }
    }
  } else {
    val = val.load;
    assert(val.stack);
    // generate jump over true branch
    auto jovertrue = JumpChain.startForward();
    jovertrue.putJump(OpCode.JFalse, val.slot);
    pires = parseStatement();
    // has `else`?
    if (token.kw(Token.T.Else)) {
      skipToken();
      // yep; gen jump over else
      auto joverfalse = JumpChain.startForward();
      joverfalse.putJump(OpCode.Jump);
      // patch jump over true
      jovertrue.finishHere();
      // `else` branch
      pires.merge(parseStatement());
      // patch jump over else
      joverfalse.finishHere();
    } else {
      // nope, no `else`; patch jump over true here
      jovertrue.finishHere();
      // reset ParserInfo, 'cause else branch is empty
      pires = ParseInfo.init;
    }
  }
  return pires;
}


ParseInfo parseWhile (in ref Loc statloc) {
  ParseInfo pires;
  auto oldcontpc = curcontpc;
  auto oldbreakpc = curbreakpc;
  scope(exit) { curcontpc = oldcontpc; curbreakpc = oldbreakpc; }
  // mark code start (so we can rewind codegen on dead code elimination step)
  // also, this is "continue" jump point
  curcontpc = JumpChain.startHere();
  curbreakpc = JumpChain.startForward();
  // parse condition
  auto condloc = token.loc;
  if (!token.delim("(")) compileError("'(' expected");
  skipToken();
  auto cond = parseExpr();
  if (!token.delim(")")) compileError("')' expected");
  skipToken();
  if (!cond.type.isGoodForBool) compileErrorAt(condloc, "boolean expression expected");
  // dead code elimination
  bool infiniloop = false;
  if (cond.literal && !cond.lit) {
    // the while loop can be ommited (and it doesn't matter what statement we've seen there)
    parseStatement();
    curcontpc.unwindToBase(); // remove the whole loop
  } else {
    // generate condition check and "break" (if condition is not known)
    if (!cond.literal) {
      cond = cond.load;
      curbreakpc.putJump(OpCode.JFalse, cond.slot);
    } else {
      assert(cond.lit != 0);
      infiniloop = true;
    }
    pires = parseStatement();
    // jump back to start
    curcontpc.putJump(OpCode.Jump);
    // finish `break` and `continue` chains
    curcontpc.finishHere();
    curbreakpc.finishHere();
  }
  // (rough) check if loop is infinite
  if (!infiniloop || pires.breakSeen) {
    // it doesn't matter if we had `return` in loop, as we had `break` too
    pires.returnSeen = false;
  }
  pires.breakSeen = false;
  pires.contSeen = false;
  return pires;
}


ParseInfo parseBreak (in ref Loc statloc) {
  if (!curbreakpc.valid) compileError("`break` outside of loop/switch");
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  curbreakpc.putJump(OpCode.Jump);
  return ParseInfo.makeBreak();
}


ParseInfo parseContinue (in ref Loc statloc) {
  if (!curcontpc.valid) compileError("`continue` outside of loop");
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  curcontpc.putJump(OpCode.Jump);
  return ParseInfo.makeContinue();
}


ParseInfo parseFor (in ref Loc statloc) {
  ParseInfo pires;
  if (!token.delim("(")) compileError("'(' expected");
  skipToken();
  auto oldcontpc = curcontpc;
  auto oldbreakpc = curbreakpc;
  enterScope(); // why not? it is easier this way
  scope(exit) { curcontpc = oldcontpc; curbreakpc = oldbreakpc; leaveScope(); }
  // compile init expression
  if (!token.delim(";")) parseExprWithPossibleDecl!"for"();
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  // mark code start (so we can rewind codegen on dead code elimination step)
  // also, this is "continue" jump point
  auto forstartpc = JumpChain.startHere(); // if condition is false, we'll unwind the whole loop
  curcontpc = JumpChain.startForward();
  curbreakpc = JumpChain.startForward();
  auto jumpToCond = JumpChain.startHere(); // address of condition: we should jump here after "next"
  bool docondition = true;
  // compile condition
  auto condloc = token.loc;
  Value cond = (token.delim(";") ? Value.makeBool(true, condloc) : parseExpr());
  if (!cond.type.isGoodForBool) compileErrorAt(condloc, "boolean expression expected");
  bool infiniloop = false;
  // dead code elimination
  if (cond.literal) {
    // expression evaluation can be ommited
    curcontpc.unwindToBase(); // rewind it, just in case
    curcontpc.startHere();
    docondition = false;
    infiniloop = (cond.lit != 0);
  } else {
    // get out of here if condition is false
    cond = cond.load;
    // emit break
    curbreakpc.putJump(OpCode.JFalse, cond.slot);
  }
  // do we have "next" expression?
  if (!token.delim(")")) {
    // yep, compile it
    if (!token.delim(";")) compileError("';' expected");
    skipToken();
    // gnerate jump over next, and code for next
    if (!token.delim(")")) {
      auto overnext = JumpChain.startForward();
      overnext.putJump(OpCode.Jump);
      curcontpc.setHere(); // "continue" will jump here
      auto xpc = vmcode.length;
      // allow several expressions, delimited by comma
      for (;;) {
        parseAssExpr();
        if (!token.delim(",")) break;
        skipToken();
      }
      if (xpc == vmcode.length) {
        // no interesting code was generated, remove jump
        overnext.unwindToBase();
        // no "next", so "continue" is the same as "cond"
        curcontpc = jumpToCond;
      } else {
        // jump to condition
        if (docondition) jumpToCond.putJump(OpCode.Jump);
        // fix jump over "next"
        overnext.finishHere();
      }
    } else {
      // no "next", so "continue" is the same as "cond"
      curcontpc = jumpToCond;
    }
  } else {
    // no "next", so "continue" is the same as "cond"
    curcontpc = jumpToCond;
  }
  if (!token.delim(")")) compileError("')' expected");
  skipToken();
  pires = parseStatement();
  // jump back to start
  curcontpc.putJump(OpCode.Jump);
  // finish `break` and `continue` chains
  curcontpc.finishHere();
  curbreakpc.finishHere();
  // eliminate dead code
  if (cond.literal && !cond.lit) {
    forstartpc.unwindToBase();
    pires = ParseInfo.init;
  } else {
    // (rough) check if loop is infinite
    if (!infiniloop || pires.breakSeen) {
      // it doesn't matter if we had `return` in loop, as we had `break` too
      pires.returnSeen = false;
    }
  }
  pires.breakSeen = false;
  pires.contSeen = false;
  return pires;
}


ParseInfo parseAlias (in ref Loc statloc) {
  if (!token.id) compileError("identifier expected");
  while (!token.delim(";")) {
    if (!token.id) compileError("identifier expected");
    string aname = token.sval;
    skipToken();
    if (!token.delim("=")) compileError("'=' expected");
    skipToken();
    if (!token.id) compileError("identifier expected");
    string dstname = token.sval;
    skipToken();
    // create new alias
    curscope.aliases[aname] = dstname;
    if (!token.delim(",")) break;
    skipToken();
  }
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  return ParseInfo.init;
}


ParseInfo parseStatement () {
  Value lres; // dummy
  // empty statements aren't allowed
  if (token.delim(";")) compileError("use '{}' as empty statement");
  if (token.delim("{")) return parseCodeBlock();
  // auto or vardecl?
  if (!parseAutoDecl(true, lres) && !parseVarDecl(true, lres)) {
    // do you love D metaprogramming? i do!
    foreach (string mname; __traits(allMembers, Token.T)) {
      static if (__traits(compiles, () => mixin("&parse"~mname))) {
        if (token.kw(__traits(getMember, Token.T, mname))) {
          auto loc = token.loc;
          skipToken();
          mixin("return parse"~mname~"(loc);");
        }
      }
    }
    // parse expression
    auto eloc = token.loc;
    auto val = parseAssExpr();
  }
  if (!token.delim(";")) compileError("';' expected");
  skipToken();
  return ParseInfo.init;
}


ParseInfo parseCodeBlock(bool openScope=true) () {
  ParseInfo pires;
  if (token.delim("{")) {
    auto stloc = token.loc;
    skipToken();
    static if (openScope) {
      enterScope();
      scope(exit) leaveScope();
    }
    bool first;
    for (;;) {
      if (token.empty) compileErrorAt(stloc, "unterminated code block");
      if (token.delim("}")) { skipToken(); break; }
      auto pri = parseStatement();
      pires.returnSeen = pires.returnSeen || pri.returnSeen;
    }
  } else {
    pires = parseStatement();
  }
  return pires;
}


// `CheckStack` already generated (always generated)
void parseFuncBody () {
  auto fstloc = token.loc;
  auto startpc = cast(int)vmcode.length; // for later checks
  if (!token.delim("{")) compileErrorAt(fstloc, "'{' expected");
  ParseInfo pires = parseCodeBlock!false();
  // check if we have `return` for non-void function
  if (!curfunc.type.aux.isVoid) {
    if (!pires.returnSeen) compileErrorAt(fstloc, "non-void function has undefined return value");
  }
  // if previous instruction is `Ret`, and we have no jumps here...
  // WARNING: this assumes that no weird code was generated
  bool genret = true;
  if (vmcode[$-1].opcode == OpCode.Ret) {
    genret = false;
    checkloop: foreach (immutable ofs, const ref VMOp opc; vmcode[startpc..$]) {
      switch (opc.opcode) {
        case OpCode.Jump:
        case OpCode.JTrue:
        case OpCode.JFalse:
          int dest = startpc+cast(int)ofs+1+opc.s16;
          if (dest > vmcode.length) assert(0, "internal compiler error");
          if (dest == vmcode.length) { genret = true; break checkloop; }
          break;
        default: break;
      }
    }
  }
  if (genret) emitCode(VMOp(OpCode.Ret));
}


// this function should be called after `scanTopLevelDecls()`, 'cause alot of syntax checking is done there
void compileSource () {
  // delimiter is skipped too
  void skipUntilDelim (string dm) {
    while (!token.delim(dm)) {
      if (token.empty) compileError("unexpected end of file");
      skipToken();
    }
    skipToken();
  }

  if (vmcode.length == 0) {
    vmcode.reserve(65536); // why not? we have plenty of memory anyway
    vmcode ~= VMOp(OpCode.Nop); // 0 is reserved
  }

  while (!token.empty) {
    if (token.delim(";")) { skipToken(); continue; }
    auto stloc = token.loc;
    // top-level declarations should be removed by `scanTopLevelDecls()`
    if (token.kw(Token.T.Alias) || token.kw(Token.T.Enum) || token.kw(Token.T.Const) || token.id("field")) assert(0, "internal compiler error");
    bool ismethod = token.kw(Token.T.Method);
    string methodactid;
    if (ismethod) skipToken();
    // parse `method(actorid)`
    if (ismethod && token.delim("(")) {
      skipToken();
      if (!token.id) compileError("identifier expected");
      methodactid = token.sval;
      skipToken();
      if (!token.delim(")")) compileError("')' expected");
      skipToken();
    }
    // get type
    TypeBase tp = parseTypeDecl!false();
    if (tp is null) compileError("type name expected");
    if (!token.id) compileError("identifier expected");
    string name = token.sval;
    skipToken();
    // it must be function (everything else was processed and removed in `scanTopLevelDecls()`
    auto tf = parseFuncArgs(ismethod);
    tf.aux = tp;
    if (!token.delim("{")) compileError("'{' expected");
    // function body
    {
      scope(exit) curfunc = FuncInfo.init;
      // find function
      string fullname = (methodactid.length ? methodactid~"::"~name : name);
      Variable var = null;
      int fnidx = -1;
      if (auto fp = fullname in funclist) {
        foreach (immutable idx, ref fni; fp.ovloads) if (tf.same(fni.type)) { fnidx = cast(int)idx; break; }
        if (fnidx >= 0) var = *fp;
      }
      if (var is null) compileErrorAt(stloc, "function '"~fullname~"' not found");
      curfunc = var.ovloads[fnidx];
      if (curfunc.pc < 0) compileErrorAt(stloc, "cannot implement builtins");
      if (curfunc.pc != 0) compileErrorAt(stloc, "duplicate function definition");
      curmaxreg = 0;
      // setup compiler and compile function body
      auto codeofs = cast(int)vmcode.length;
      {
        enterScope();
        scope(exit) leaveScope();
        curslots[] = 0; // no used slots
        curslots[0] = -1; // return value
        // enter arguments
        foreach (immutable aidx, ref arg; curfunc.type.args) {
          auto av = new Variable();
          av.idx = -(cast(int)aidx+1);
          av.name = arg.name;
          av.type = arg.type;
          if (arg.ptrref) av.kind = Variable.Kind.Reference;
          av.readonly = arg.readonly;
          curscope.vars ~= av;
        }
        //stderr.writeln("*********** ", curfunc.name, " *********** ", curslots);
        emitCode(VMOp(OpCode.CheckStack)); // will be patched later
        parseFuncBody();
      }
      if (curmaxreg >= short.max-1) compileErrorAt(stloc, "too many stacks slots used");
      vmcode[codeofs].setU16(cast(ushort)(curmaxreg+1));
      version(mesdbg_dump_compiled_code) {
        if (!MESDisableDumps) {
          stderr.writeln("==== ", curfunc.name, " ==== (", curmaxreg+1, ")");
          foreach (immutable pc; codeofs..vmcode.length) {
            stderr.writefln("%04X: %s", pc, decodeOpcode(cast(int)pc, vmcode[pc]));
          }
          stderr.writeln("----");
        }
      }
      if (vmcode.length >= ushort.max) compileErrorAt(stloc, "vm code section too big");
      var.ovloads[fnidx].pc = codeofs;
    }
  }
  //fixupDump(stderr);
  fixupCalls(); // why not
}


// ////////////////////////////////////////////////////////////////////////// //
// this collects source code tokens, and parses top-level declarations
void scanTopLevelDecls (ParserBase curparser) {
  if (curparser.tk.empty) return; // nothing to do

  auto origstpos = curtokidx; // we'll rewind here, so `compileSource()` can process it all again
  scope(exit) {
    curtokidx = origstpos;
    cursource.assumeSafeAppend;
  }

  // collect and remember all tokens
  // remember this, so we'll know the starting point
  while (!curparser.tk.empty) {
    cursource ~= curparser.tk;
    curparser.next();
  }

  static int markTokenPos () nothrow @trusted @nogc { return cast(int)curtokidx; }

  static void removeTokensFromMark (int pos) /*nothrow*/ {
    assert(pos >= 0 && pos <= cursource.length);
    assert(curtokidx >= 0 && curtokidx <= cursource.length);
    if (pos == cursource.length) return;

    // easy case?
    if (curtokidx >= cursource.length) {
      cursource.length = pos;
      curtokidx = cast(int)cursource.length;
      return;
    }
    // ok, move tokens
    auto dest = pos;
    foreach (immutable cidx; curtokidx..cursource.length) cursource.ptr[dest++] = cursource.ptr[cidx];
    assert(dest <= cursource.length);
    cursource.length = dest;
    curtokidx = pos;
  }

  while (!token.empty) {
    auto tokmark = markTokenPos();
    if (token.delim(";")) {
      skipToken();
      removeTokensFromMark(tokmark);
      continue;
    }
    auto stloc = token.loc;
    // alias?
    if (token.kw(Token.T.Alias)) {
      skipToken();
      if (!token.id) compileError("identifier expected");
      while (!token.delim(";")) {
        if (!token.id) compileError("identifier expected");
        string aname = token.sval;
        skipToken();
        if (!token.delim("=")) compileError("'=' expected");
        skipToken();
        if (!token.id) compileError("identifier expected");
        string dstname = token.sval;
        skipToken();
        // create new alias
        aliases[aname] = dstname;
        if (!token.delim(",")) break;
        skipToken();
      }
      if (!token.delim(";")) compileError("';' expected");
      skipToken();
      removeTokensFromMark(tokmark);
      continue;
    }
    // enum ?
    if (token.kw(Token.T.Enum)) {
      auto defloc = token.loc;
      skipToken();
      // typed enum ?
      TypeEnum etype = null;
      if (token.id) {
        if (token.sval in gconsts) compileErrorAt(stloc, "duplicate constant");
        if (token.sval in globals) compileErrorAt(stloc, "constant/global variable conflict");
        if (token.sval in funclist) compileErrorAt(stloc, "constant/function conflict");
        etype = new TypeEnum(token.sval, typeInt); // the only possible enum type for now
        skipToken();
        auto c = new Constant();
        c.name = etype.name;
        c.type = etype;
        gconsts[c.name] = c;
      }
      if (!token.delim("{")) compileError("'{' expected");
      skipToken();
      int curval = 0;
      string membername = null;
      Constant c = null;
      while (!token.delim("}")) {
        if (!token.id) compileError("identifier expected");
        // get member name, check for conflicts
        if (etype is null) {
          if (token.sval in gconsts) compileError("duplicate constant");
          if (token.sval in globals) compileError("constant/global variable conflict");
          if (token.sval in funclist) compileError("constant/function conflict");
          c = new Constant();
          c.name = token.sval;
          c.type = typeInt;
        } else {
          if (token.sval in etype.members) compileError("duplicate enum member");
          membername = token.sval;
          if (membername == "min" || membername == "max") compileError("cannot redefine enum properties");
        }
        skipToken();
        // parse initializer
        if (token.delim("=")) {
          skipToken();
          auto eloc = token.loc;
          auto v = parseExpr();
          if (!v.literal) compileErrorAt(eloc, "literal expected");
          if (!v.type.isInt) compileErrorAt(eloc, "integer literal expected");
          curval = v.lit;
        }
        // introduce new constant or member
        if (etype is null) {
          // constant
          c.ival = curval++;
          gconsts[c.name] = c;
        } else {
          // member
          // if enum has no members, first one will become default
          if (etype.members.length == 0) etype.mDefValue = curval;
          etype.members[membername] = curval++;
        }
        if (!token.delim(",")) break;
        skipToken();
      }
      if (!token.delim("}")) compileError("'}' expected");
      skipToken();
      // register new typed enum
      if (etype !is null) {
        if (etype.name in gtypes) compileErrorAt(defloc, "duplicate type declaration");
        gtypes[etype.name] = etype;
      }
      removeTokensFromMark(tokmark);
      continue;
    }
    // struct?
    if (token.kw(Token.T.Struct)) {
      auto tpos = curtokidx;
      skipToken();
      if (token.id) {
        // rollback
        curtokidx = tpos;
        auto defloc = token.loc;
        auto stp = parseStructDecl(true);
        if (stp.name in gconsts) compileError("struct/constant conflict");
        if (stp.name in globals) compileError("struct/global variable conflict");
        if (stp.name in funclist) compileError("struct/function conflict");
        // register new struct type
        if (stp.name in gtypes) compileErrorAt(defloc, "duplicate type declaration");
        gtypes[stp.name] = stp;
        removeTokensFromMark(tokmark);
        continue;
      } else {
        // rollback, so we'll parse type
        curtokidx = tpos;
      }
    }
    // others
    bool isfield = token.id("field");
    bool isconst = token.kw(Token.T.Const);
    bool ismethod = token.kw(Token.T.Method);
    string methodactid;
    if (isfield || isconst || ismethod) skipToken();
    // parse `method(actorid)`
    if (ismethod && token.delim("(")) {
      skipToken();
      if (!token.id) compileError("identifier expected");
      methodactid = token.sval;
      skipToken();
      if (!token.delim(")")) compileError("')' expected");
      skipToken();
    }
    // get type
    TypeBase tp = parseTypeDecl!false();
    if (tp is null) compileError("type name expected");
    if (!token.id) compileError("identifier expected");
    string name = token.sval;
    skipToken();
    // actor field? add it here, why not
    if (isfield) {
      if (tp.isVoid) compileError("`void` is not allowed here");
      if (tp.isFunc) tp = new TypePointer(tp); // convert `.isFunc` to funcptr
      //if (tp.isArray) compileError("array fields aren't supported yet");
      //if (tp.isStruct) compileError("struct fields aren't supported yet");
      for (;;) {
        if (auto fld = actorFindFieldPtr(name)) {
          // should be of exactly the same type
          if (!fld.type.same(tp)) compileErrorAt(stloc, "conflicting field type, previous definition at '"~fld.loc.fname.idup~"':"~fld.loc.toString);
        } else {
          // new field
          auto af = actorCreateField(name, tp, stloc);
          if (!af.valid) compileError("cannot create field '"~name~"'");
        }
        if (!token.delim(",")) break;
        skipToken();
        if (!token.id) compileError("identifier expected");
        name = token.sval;
        skipToken();
      }
      if (!token.delim(";")) compileError("';' expected");
      skipToken();
      removeTokensFromMark(tokmark);
      continue;
    }
    // constant?
    if (isconst) {
      if (name in gconsts) compileErrorAt(stloc, "duplicate constant");
      if (name in globals) compileErrorAt(stloc, "constant/global variable conflict");
      if (name in funclist) compileErrorAt(stloc, "constant/function conflict");
      if (!token.delim("=")) compileError("'=' expected");
      skipToken();
      auto eloc = token.loc;
      auto v = parseExpr();
      if (!token.delim(";")) compileError("';' expected");
      skipToken();
      if (!v.literal) compileErrorAt(eloc, "literal expected");
      if (!v.type.isBool && !v.type.isInt && !v.type.isStr) compileErrorAt(eloc, "invalid constant type");
      if (!v.type.same(tp)) compileErrorAt(eloc, "invalid constant type");
      auto c = new Constant();
      c.name = name;
      c.type = v.type;
      if (v.type.isStr) c.sval = strlist[v.lit]; else c.ival = v.lit;
      gconsts[c.name] = c;
      removeTokensFromMark(tokmark);
      continue;
    }
    // global var?
    if (!ismethod && !token.delim("(")) {
      // name already eaten
      if (tp.isArray) {
        if (!tp.base.isBool && !tp.base.isInt && !tp.base.isStr && !tp.base.isActor && !tp.base.isFuncPtr && !tp.base.isStruct && !tp.base.isEnum) compileErrorAt(stloc, "invalid type for global variable");
      } else {
        if (!tp.isBool && !tp.isInt && !tp.isStr && !tp.isActor && !tp.isFunc && !tp.isStruct && !tp.isEnum) compileErrorAt(stloc, "invalid type for global variable");
      }
      if (tp.isFunc) tp = new TypePointer(tp); // convert `.isFunc` to funcptr
      for (;;) {
        if (name in gconsts) compileErrorAt(stloc, "global variable/constant conflict");
        if (name in globals) compileErrorAt(stloc, "duplicate global variable");
        if (name in funclist) compileErrorAt(stloc, "global variable/function conflict");
        Variable var;
        //TODO: initializers for arrays with fixed length
        if (tp.isArray && tp.cellSize == 0) {
          // array with initializer
          if (!token.delim("=")) compileError("array initializer expected");
          skipToken();
          if (!token.delim("[")) compileError("'[' expected");
          skipToken();
          var = new Variable();
          var.idx = cast(ushort)vmglobals.length;
          auto atp = new TypeArray(tp.base, 0);
          while (!token.delim("]")) {
            auto exprloc = token.loc;
            auto val = parseExpr();
            if (!val.literal) compileErrorAt(exprloc, "literal expected");
            if (!val.type.same(atp.base)) compileErrorAt(exprloc, "invalid initializer type");
            if (vmglobals.length+atp.cellSize >= short.max-1) compileError("too many global variables");
            vmglobals ~= val.lit;
            atp.setSize(atp.size+1);
            if (!token.delim(",")) break;
            skipToken();
          }
          if (!token.delim("]")) compileError("']' expected");
          skipToken();
          var.type = atp;
        } else {
          if (vmglobals.length >= short.max || vmglobals.length+tp.cellSize > short.max) compileError("too many global variables");
          var = new Variable();
          var.idx = cast(ushort)vmglobals.length;
          var.type = tp;
          vmglobals.length += tp.cellSize;
          vmglobals[var.idx..$] = 0; // just in case
        }
        var.name = name;
        var.kind = Variable.Kind.Global;
        globals[var.name] = var;
        if (!token.delim(",")) break;
        skipToken();
        if (!token.id) compileError("identifier expected");
        name = token.sval;
        skipToken();
      }
      if (!token.delim(";")) compileError("';' expected");
      skipToken();
      removeTokensFromMark(tokmark);
      continue;
    }
    // function/method
    if (tp.isArray) compileError("array return values aren't supported yet");
    if (tp.isStruct) compileError("struct return values aren't supported yet");
    auto tf = parseFuncArgs(ismethod);
    tf.aux = tp;
    int oldfound = -1;
    string fullname = (methodactid.length ? methodactid~"::"~name : name);
    auto fp = fullname in funclist;
    if (fp !is null) {
      foreach (immutable idx, ref fni; fp.ovloads) {
        if (fni.type.callCompatibleNoAux(tf)) { oldfound = cast(int)idx; break; }
      }
    }
    // skip prototypes
    if (oldfound >= 0) {
      if (!fp.ovloads[oldfound].type.aux.same(tf.aux) || !fp.ovloads[oldfound].type.same(tf)) compileError("found declaration with different return type, previous one is at "~fp.ovloads[oldfound].loc.toFullString);
      if (token.id("builtin")) {
        if (fp.ovloads[oldfound].pc >= 0) compileError("conflicting declaration, previous one is at "~fp.ovloads[oldfound].loc.toFullString);
        skipToken();
        if (!token.delim(";")) compileError("';' expected");
        skipToken();
        removeTokensFromMark(tokmark);
        continue;
      } else {
        if (fp.ovloads[oldfound].pc < 0) compileError("conflicting declaration, previous one is at "~fp.ovloads[oldfound].loc.toFullString);
        if (fp.ovloads[oldfound].hasBody && !token.delim(";")) compileError("';' expected");
        if (token.delim(";")) {
          skipToken();
          removeTokensFromMark(tokmark);
          continue;
        }
      }
    }
    // new function
    Variable var;
    if (fp is null) {
      if (fullname in gconsts) compileErrorAt(stloc, "function/constant conflict");
      if (fullname in globals) compileErrorAt(stloc, "function/global variable conflict");
      if (funclist.length >= ushort.max) compileErrorAt(stloc, "too many functions");
      var = new Variable();
      var.idx = 0;
      var.kind = Variable.Kind.Global;
      var.name = fullname;
      var.type = tf;
      var.firstloc = stloc;
      funclist[fullname] = var;
    } else {
      var = *fp;
    }
    if (oldfound < 0) {
      FuncInfo fi;
      fi.type = tf;
      fi.pc = 0;
      fi.name = fullname;
      fi.actorid = methodactid;
      fi.shortname = name;
      fi.loc = stloc;
      var.ovloads ~= fi;
      oldfound = cast(int)var.ovloads.length-1;
    }
    if (token.id("builtin")) {
      if (var.ovloads[oldfound].hasBody) compileError("buildins cannot have body");
      var.ovloads[$-1].pc = fixBuiltin(var.ovloads[oldfound].name, var.ovloads[oldfound].type);
      skipToken();
      if (!token.delim(";")) compileError("';' expected");
      skipToken();
      removeTokensFromMark(tokmark);
      continue;
    } else {
      if (tf.hasrest) compileErrorAt(stloc, "scriped vararg functions aren't supported yet");
    }
    // prototype?
    if (token.delim(";")) {
      skipToken();
      removeTokensFromMark(tokmark);
    } else {
      if (var.ovloads[oldfound].hasBody) compileError("duplicate body");
      // function with body, skip the body
      if (!token.delim("{")) compileError("'{' expected");
      int level = 0;
      for (;;) {
        if (token.empty) compileErrorAt(stloc, "unterminated function declaration");
             if (token.delim("{")) { ++level; }
        else if (token.delim("}")) { if (--level == 0) break; }
        skipToken();
      }
      skipToken();
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void mesIntroduceConstant(CT, NT:const(char)[]) (NT name, CT val)
if (is(CT == int) || is(CT == bool) || is(CT : const(char)[]))
{
  if (name in gconsts) compileError("duplicate constant");
  if (name in globals) compileError("constant/global variable conflict");
  if (name in funclist) compileError("constant/function conflict");
  auto c = new Constant();
  static if (is(NT == string)) c.name = name; else c.name = name.idup;
       static if (is(CT == int)) { c.type = typeInt; c.ival = val; }
  else static if (is(CT == bool)) { c.type = typeBool; c.ival = (val ? 1 : 0); }
  else static if (is(CT : const(char)[])) {
    c.type = typeStr;
    if (val.length) {
      static if (is(CT == string)) c.sval = val; else c.sval = val.idup;
    } else {
      c.sval = "";
    }
  } else {
    static assert(0, "oops");
  }
  gconsts[c.name] = c;
}


public void mesIntroduceEnum(ET, NT:const(char)[]) (NT ename=null)
if (is(ET == enum) && is(typeof(ET.min) : int))
{
  static if (is(NT == typeof(null))) {
    string xname = ET.stringof;
  } else {
    if (ename.length == 0) ename = ET.stringof;
    alias xname = ename;
  }
  // typed enum
  if (xname in gconsts) compileError("duplicate constant");
  if (xname in globals) compileError("constant/global variable conflict");
  if (xname in funclist) compileError("constant/function conflict");
  TypeEnum etype = new TypeEnum(xname, typeInt); // the only possible enum type for now
  auto c = new Constant();
  c.name = etype.name;
  c.type = etype;
  gconsts[c.name] = c;
  // register members
  foreach (string mname; __traits(allMembers, ET)) {
    etype.members[mname] = __traits(getMember, ET, mname);
  }
  // register new typed enum
  gtypes[etype.name] = etype;
}


// ////////////////////////////////////////////////////////////////////////// //
private string buildCacheStr(RT, A...) () {
  char[1024] res;
  int rpos = 0;
  enum rn = fullyQualifiedName!RT;
  res[rpos..rpos+rn.length] = rn[];
  rpos += cast(int)rn.length;
  res[rpos..rpos+1] = "|";
  ++rpos;
  foreach (/*immutable*/ ptype; A) {
    enum tn = fullyQualifiedName!ptype;
    res[rpos..rpos+tn.length] = tn[];
    rpos += cast(int)tn.length;
  }
  return res[0..rpos].idup;
}


// ////////////////////////////////////////////////////////////////////////// //
public struct VMIFaceId {
private:
  static struct FuncCache {
    int fnidx;
    Variable var;
  }

  __gshared FuncCache[][string] fcache;

private:
  Variable var;

public:
  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (var !is null); }

  void opCall(A...) (A args) { call(args); }

  auto call(RT=void, A...) (A args) {
    if (var is null) throw new OverloadNotFound("cannot call nothing");
    if (!var.type.isFunc) throw new OverloadNotFound("cannot call non-function '"~var.name~"'");
    enum cacheStr = buildCacheStr!(RT, A);
    //pragma(msg, cacheStr);
    mainloop: for (;;) {
      if (auto fcp = cacheStr in fcache) {
        foreach (ref FuncCache fc; (*fcp)[]) {
          if (fc.var is var) {
            //stderr.writeln("CACHED: '", cacheStr, "' : <", var.name, ">");
            static if (is(RT == void)) {
              vmExec(var.ovloads[fc.fnidx].pc, args);
              return;
            } else {
              auto res = vmExec(var.ovloads[fc.fnidx].pc, args);
                   static if (is(RT == Unqual!int)) return res;
              else static if (is(RT == Unqual!bool)) return (res != 0);
              else static if (is(RT == ActorId)) return ActorId(res);
              else static if (is(Unqual!RT == enum)) return cast(RT)res;
              else static if (is(RT == string) || is(RT == const(char)[])) return strlist[res];
              else static assert(0, "wtf?!");
            }
          }
        }
      }
      // no cached function, try to find one
      auto tf = buildFuncType!(false, false, RT, A);
      if (tf is null) throw new OverloadNotFound("cannot find suitable overload of '"~var.name~"' to call");
      assert(tf !is null);
      scope(exit) delete tf;
      // try exact
      foreach (immutable fidx, ref FuncInfo fi; var.ovloads) {
        if (fi.type.callCompatible(tf)) {
          FuncCache[] fc;
          if (auto fcp = cacheStr in fcache) fc = *fcp;
          fc ~= FuncCache(cast(int)fidx, var);
          fcache[cacheStr] = fc;
          continue mainloop;
        }
      }
      static if (is(RT == void)) {
        // try inexact
        foreach (immutable fidx, ref FuncInfo fi; var.ovloads) {
          if (fi.type.callCompatibleNoAux(tf)) {
            FuncCache[] fc;
            if (auto fcp = cacheStr in fcache) fc = *fcp;
            fc ~= FuncCache(cast(int)fidx, var);
            fcache[cacheStr] = fc;
            continue mainloop;
          }
        }
      }
      throw new OverloadNotFound("cannot find suitable overload of '"~var.name~"' to call");
    }
    assert(0);
  }

  auto get(RT=int) () if (is(RT == bool) || is(RT == int) || is(RT == string) || is(RT == ActorId)) {
    if (var is null) throw new OverloadNotFound("cannot get nothing");
    if (!var.global) assert(0, "wtf?!");
    static if (is(RT == bool)) {
      if (var.type.isBool || var.type.isInt || var.type.isStr) return (vmglobals[var.idx] != 0);
      if (var.type.isActor) return ActorId(vmglobals[var.idx]).alive;
      assert(0, "oops");
    } else static if (is(RT == int)) {
      if (var.type.isBool || var.type.isInt) return vmglobals[var.idx];
      assert(0, "oops");
    } else static if (is(RT == string)) {
      if (var.type.isStr) return strlist[vmglobals[var.idx]];
      assert(0, "oops");
    } else static if (is(RT == ActorId)) {
      if (var.type.isActor) return ActorId(vmglobals[var.idx]);
      assert(0, "oops");
    } else {
      static assert(0, "wtf?!");
    }
  }

  void set(VT) (VT v) if (is(VT == bool) || is(VT == int) || is(VT == ActorId) || is(VT == enum)) {
    if (var is null) throw new OverloadNotFound("cannot get nothing");
    if (!var.global) assert(0, "wtf?!");
    static if (is(VT == bool)) {
      if (var.type.isBool || var.type.isInt) vmglobals[var.idx] = (v ? 1 : 0);
      else assert(0, "oops");
    } else static if (is(VT == int)) {
           if (var.type.isBool) vmglobals[var.idx] = (v ? 1 : 0);
      else if (var.type.isInt) vmglobals[var.idx] = v;
      else assert(0, "oops");
    } else static if (is(VT == ActorId)) {
      if (var.type.isActor) vmglobals[var.idx] = v.id;
      else assert(0, "oops");
    } else static if (is(VT == enum)) {
      if (auto tpp = (Unqual!VT).stringof in gtypes) {
        if (auto et = cast(TypeEnum)(*tpp)) {
          //FIXME: more checks
          if (var.type.isInt) { vmglobals[var.idx] = cast(int)v; return; }
          if (var.type.isEnum && var.type.same(et)) { vmglobals[var.idx] = cast(int)v; return; }
        }
      }
      assert(0, "unknown enum '"~(Unqual!VT).stringof~"'");
    } else {
      static assert(0, "wtf?!");
    }
  }
}


public struct VMIFace {
  static VMIFaceId opIndex (string fld) {
    VMIFaceId res;
         if (auto fp = fld in funclist) res.var = *fp;
    else if (auto vp = fld in globals) res.var = *vp;
    else throw new OverloadNotFound("identifier '"~fld~"' not found");
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public string mesGetString (int idx) nothrow @trusted @nogc {
  return (idx >= 0 && idx < strlist.length ? strlist.ptr[idx] : null);
}


// ////////////////////////////////////////////////////////////////////////// //
public void mesCompileFile (VFile fl, void delegate (const(char)[] fname) aOnInclude=null) {
  scope(exit) forgetSource();
  {
    auto curparser = new FileParser(fl, aOnInclude);
    scope(exit) delete curparser;
    curparser.next(); // warm it up
    scanTopLevelDecls(curparser);
  }

  //foreach (const ref Token tk; cursource) stderr.write(tk, " "); stderr.writeln;

  compileSource();
}


/// call this after you compiled all the files, to check if some unresolved function calls lefs
/// returns `true` if everything is ok
public bool mesCheckUndefined () {
  return !fixupCalls();
}


public void mesDumpUndefined (VFile fo) {
  fixupDump(fo);
}
