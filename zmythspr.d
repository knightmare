/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module zmythspr;

import arsd.simpledisplay;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
immutable Color[16] egapal = [
  Color(0x00, 0x00, 0x00),
  Color(0x00, 0x00, 0xAA),
  Color(0x00, 0xAA, 0x00),
  Color(0x00, 0xAA, 0xAA),
  Color(0xAA, 0x00, 0x00),
  Color(0xAA, 0x00, 0xAA),
  Color(0xAA, 0x55, 0x00),
  Color(0xAA, 0xAA, 0xAA),
  Color(0x55, 0x55, 0x55),
  Color(0x55, 0x55, 0xFF),
  Color(0x55, 0xFF, 0x55),
  Color(0x55, 0xFF, 0xFF),
  Color(0xFF, 0x55, 0x55),
  Color(0xFF, 0x55, 0xFF),
  Color(0xFF, 0xFF, 0x55),
  Color(0xFF, 0xFF, 0xFF),
];


// ////////////////////////////////////////////////////////////////////////// //
final class MythSprite {
public:
  int wdt, hgt;
  Color[] pix;

public:
  this (const(char)[] fname, bool masked) { load(VFile(fname), masked); }
  this (VFile fl, bool masked) { load(fl, masked); }

  final @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (wdt > 0 && hgt > 0); }

  final @property int width () const pure nothrow @safe @nogc { pragma(inline, true); return wdt; }
  final @property int height () const pure nothrow @safe @nogc { pragma(inline, true); return hgt; }

  final void clear () {
    wdt = hgt = 0;
    delete pix;
  }

  void load (VFile fl, bool masked) {
    clear();
    scope(failure) clear();

    wdt = fl.readNum!ubyte;
    hgt = fl.readNum!ubyte;

    ubyte[][5] planes;
    scope(exit) foreach (ref p; planes[]) delete p;

    foreach (immutable pidx, ref p; planes[]) {
      p.length = wdt*hgt;
      if (pidx == 4 && !masked) {
        p[] = 0xff;
      } else {
        fl.rawReadExact(p[]);
      }
    }

    ubyte getbit (int x, int y, uint pnum) {
      auto p = planes[pnum];
      return (planes[pnum][y*wdt+x/8]>>(7-(x&7)))&1;
    }

    pix.length = (wdt*8)*hgt;

    foreach (immutable dy; 0..hgt) {
      foreach (immutable dx; 0..wdt*8) {
        Color c = Color.transparent;
        if (getbit(dx, dy, 4)) {
          c = egapal[getbit(dx, dy, 3)|(getbit(dx, dy, 2)<<1)|(getbit(dx, dy, 1)<<2)|(getbit(dx, dy, 0)<<3)];
        }
        pix[dy*(wdt*8)+dx] = c;
      }
    }

    wdt *= 8;
  }

  final @property Color getpix (int x, int y) const nothrow @trusted @nogc {
    if (x < 0 || y < 0 || x >= wdt*8 || y >= hgt) return Color.transparent;
    return pix[y*wdt+x];
  }
}


// ////////////////////////////////////////////////////////////////////////// //
MythSprite[] loadSprites (const(char)[] fname, bool masked) {
  MythSprite[] res;
  auto fl = VFile(fname);
  while (fl.tell < fl.size) res ~= new MythSprite(fl, masked);
  return res;
}
