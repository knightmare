/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module keybinds;

import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.keybinds;


// ////////////////////////////////////////////////////////////////////////// //
enum ActionKey {
  Up,
  Down,
  Left,
  Right,
  Fire,
  //Pause,
  //Abort,
}


__gshared ubyte[ActionKey.max+1] keystate = 0;
__gshared ActionKey[Key] keybinds;


void setupDefaultKeyBindings () {
  keybinds[findKeyByName("up")] = ActionKey.Up;
  keybinds[findKeyByName("down")] = ActionKey.Down;
  keybinds[findKeyByName("left")] = ActionKey.Left;
  keybinds[findKeyByName("right")] = ActionKey.Right;
  keybinds[findKeyByName("ctrl")] = ActionKey.Fire;
  keybinds[findKeyByName("z")] = ActionKey.Fire;
  keybinds[findKeyByName("x")] = ActionKey.Fire;
  keybinds[findKeyByName("alt")] = ActionKey.Fire;
  keybinds[findKeyByName("shift")] = ActionKey.Fire;
}


// true: press/release registered
bool doActionBind (KeyEvent event, ActionKey* kk=null) {
  if (!event.key) return false;
  if (auto akp = event.key in keybinds) {
    if (kk !is null) *kk = *akp;
    if (event.pressed) {
      keystate[*akp] |= 0b011;
    } else {
      keystate[*akp] &= 0b101;
    }
    return true;
  }
  return false;
}


void actionBindFrameEnd () {
  foreach (ref ubyte b; keystate[]) {
    if (b&0b10) b = 0b11; else b = 0;
  }
}


void actionBindClear () {
  keystate[] = 0;
}


bool actionPressed (ActionKey key) nothrow @nogc { pragma(inline, true); return ((keystate[key]&1) != 0); }
bool actionWasRelease (ActionKey key) nothrow @nogc { pragma(inline, true); return ((keystate[key]&0b100) != 0); }
