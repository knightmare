/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module mesactor;
private:

import iv.cmdcon;
import iv.strex;
import iv.vfs.io;
import mesengine : Loc, TypeBase, TypeEnum, typeBool, typeInt, typeActor;


// ////////////////////////////////////////////////////////////////////////// //
public struct ActorId {
  uint id; // 0: empty; low word is index, high word is counter
  @property bool empty () const pure nothrow @safe @nogc { pragma(inline, true); return ((id&0xffff) == 0); }
  @property bool alive () const nothrow @trusted @nogc { pragma(inline, true); return ((id&0xffff) > 0 && (id&0xffff) < actors.length && id == actors.ptr[id&0xffff][0]); }

  // this is how MES does it
  //@property bool opCast(T) () const nothrow @safe @nogc if (is(T == bool)) { pragma(inline, true); return (id != 0); }
}


public struct ActorField {
  TypeBase type;
  int ofs;
  string name;
  Loc loc;

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (type !is null); }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int[][] actors; // first is always index; if bit 15 is set, this slot is free
__gshared int[string] actfields; // in actfieldlist
__gshared ActorField[] actfieldlist;
__gshared int actfieldsused = 1; // cells
__gshared int aliveActorCount = 0;


// ////////////////////////////////////////////////////////////////////////// //
public int NumObjects () nothrow @trusted @nogc { pragma(inline, true); return aliveActorCount; }


// ////////////////////////////////////////////////////////////////////////// //
public int[] actorGetById() (in ActorId aid) nothrow @trusted @nogc {
  pragma(inline, true);
  return (aid.alive ? actors.ptr[aid.id&0xffff] : null);
}


public int[] actorGetByIndex() (in int idx) nothrow @trusted @nogc {
  pragma(inline, true);
  return (idx >= 0 && idx < actors.length && (actors.ptr[idx].ptr[0]&0x8000) == 0 ? actors.ptr[idx] : null);
}


public ActorField actorFindField (const(char)[] name) {
  if (auto aip = name in actfields) return actfieldlist[*aip];
  return ActorField.init;
}

public ActorField* actorFindFieldPtr (const(char)[] name) {
  if (auto aip = name in actfields) return &actfieldlist[*aip];
  return null;
}


public ActorField actorCreateField(T:const(char)[]) (T name, TypeBase atype, in auto ref Loc loc) {
  assert(atype !is null);
  assert(atype.cellSize > 0);
  assert(name.length);
  if (auto aip = name in actfields) {
    if (actfieldlist[*aip].type.same(atype)) return actfieldlist[*aip];
    return ActorField.init;
  }
  ActorField af;
  af.type = atype;
  af.ofs = actfieldsused;
  af.loc = loc;
  static if (is(T == string)) af.name = name; else af.name = name.idup;
  actfieldsused += atype.cellSize;
  auto aidx = cast(int)actfieldlist.length;
  actfieldlist ~= af;
  actfields[af.name] = aidx;
  return af;
}


// ////////////////////////////////////////////////////////////////////////// //
public ActorId spawnActor () {
  // no actors, create actor pool
  if (actors.length == 0) {
    // [0] is reserved
    actors.reserve(32768);
    actors.length = 80; // arbitrary number
    foreach (immutable idx, ref int[] aa; actors) {
      aa = new int[](actfieldsused);
      // set index
      aa[0] = (cast(int)idx)|0x8000; // "free flag"
    }
    actors[0][0] = 0xffff_0000; // just in case
  }
  //TODO: use some kind of list or hash table instead of linear search
  foreach (immutable idx, int[] aa; actors) {
    if (idx && (aa.ptr[0]&0x8000) != 0) {
      if ((aa.ptr[0]>>16) == 0xffff) aa.ptr[0] &= 0xffff; else aa.ptr[0] += 0x0001_0000;
      aa.ptr[0] &= 0xffff_7fff; // clear "free" flag
      aa[1..$] = 0; // clear instance
      ++aliveActorCount;
      return ActorId(aa[0]);
    }
  }
  // add new actor
  if (actors.length >= ushort.max/2-1) throw new Exception("too many alive actors");
  int[] aa = new int[](actfieldsused);
  aa[] = 0;
  // set index
  aa[0] = (cast(int)actors.length)|0x0001_0000;
  actors ~= aa;
  ++aliveActorCount;
  return ActorId(aa[0]);
}


// mark actor as dead
public void killActor (ActorId aid) nothrow @trusted @nogc {
  if (aid.alive) {
    assert(aliveActorCount > 0);
    int[] aa = actors.ptr[aid.id&0xffff];
    assert(aa.length > 0);
    assert((aa.ptr[0]&0x8000) == 0);
    aa.ptr[0] |= 0x8000;
    --aliveActorCount;
  }
}


// move all alive actors to top, reset indicies
public void actorsPack () {
  int dpos = 1;
  foreach (immutable aidx, int[] aa; actors) {
    if (aidx == 0) continue;
    if ((aa.ptr[0]&0x8000) != 0) {
      // set index
      aa[0] = cast(int)aidx|0x8000; // "free flag"
      continue;
    }
    // move alive actor to top
    assert(dpos <= aidx);
    aa[0] = dpos|0x0001; // reset index
    if (dpos != aidx) {
      actors.ptr[dpos][] = aa[]; // move data
      aa[0] = cast(int)aidx|0x8000; // "free flag"
    }
    ++dpos;
  }
  assert(dpos-1 == aliveActorCount);
  if (actors.length > 80 && dpos < 80) {
    actors.length = 80;
    actors.assumeSafeAppend;
  }
}


public void actorsGC () {
  int lastUsedActor = 0;
  foreach (immutable aidx, int[] aa; actors) {
    if (aidx == 0 || (aa.ptr[0]&0x8000) != 0) continue;
    lastUsedActor = cast(int)aidx;
  }
  if (actors.length > 80 && lastUsedActor < 80) {
    actors.length = 80;
    actors.assumeSafeAppend;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// must return `ActorId.init` when there are no more actors to iterate
public ActorId actorIterStart () nothrow @trusted @nogc {
  foreach (immutable aidx, int[] aa; actors) {
    if (aidx == 0 || (aa.ptr[0]&0x8000) != 0) continue;
    return ActorId(aa.ptr[0]);
  }
  return ActorId.init;
}


// must return `ActorId.init` when there are no more actors to iterate
public ActorId actorIterNext (ActorId aid) nothrow @trusted @nogc {
  if (aid.empty) return ActorId.init;
  int aidx = cast(int)(aid.id&0xffff)+1;
  while (aidx < actors.length && (actors.ptr[aidx].ptr[0]&0x8000) != 0) ++aidx;
  if (aidx >= actors.length) return ActorId.init;
  return ActorId(actors.ptr[aidx].ptr[0]);
}
