/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * Based on the DOS Knightmare source code by Andrew Zabolotny
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module monsters;

import arsd.color;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.cmdcon.keybinds;
import iv.glbinds.util;
import iv.prng.pcg32;
import iv.strex;
import iv.vfs.io;

import tatlas;
import zmythspr;
import keybinds;
import levelmap;
import mesactor;
import mesengine;
import music;


// ////////////////////////////////////////////////////////////////////////// //
__gshared int[512] batganginfo; // key: gangid; value: # of bats not killed
__gshared int nextenemy = 0;

__gshared bool xoptContinuousFire = false;


// ////////////////////////////////////////////////////////////////////////// //
enum PrizeType {
  Rook,
  Knight,
  Queen,
  King,
  Exit,
}

shared static this () {
  conRegFunc!((ConString type) {
    try {
           if (type == "rook") VMIFace["conhookGivePrize"](PrizeType.Rook);
      else if (type == "knight") VMIFace["conhookGivePrize"](PrizeType.Knight);
      else if (type == "queen") VMIFace["conhookGivePrize"](PrizeType.Queen);
      else if (type == "king") VMIFace["conhookGivePrize"](PrizeType.King);
      else if (type == "exit") VMIFace["conhookGivePrize"](PrizeType.Exit);
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("prize", "gain prize effect");

  conSetArgCompleter("prize", delegate (ConCommand self) {
    import std.range;
    static immutable string[5] names = ["rook", "knight", "queen", "king", "exit"];
    concmdRangeCompleter(self, names[]);
  });

  static immutable string[17] spawnEntities = [
    "sphere",
    "cloud",
    "vampir",
    "vampir1",
    "knight",
    "gun",
    "prot",
    "carlson",
    "daemon",
    "hag",
    "jball",
    "skeleton",
    "chaos",
    "mine",
    "walkingguy",
    "monk",
    "ghost",
  ];

  conRegFunc!((ConString type, int power=60) {
    foreach (immutable eidx, string ename; spawnEntities[]) {
      if (type == ename) {
        cheatSpawnType = cast(int)(eidx+1);
        cheatSpawnPower = (power < 0 ? 0 : power);
        return;
      }
    }
    cheatSpawnType = 0;
    cheatSpawnPower = 0;
    conwriteln("unknown entity: ", type);
  })("spawn", "spawn non-devil enemy (name [power])");

  conSetArgCompleter("spawn", delegate (ConCommand self) {
    import std.range;
    concmdRangeCompleter(self, spawnEntities[]);
  });


  conRegFunc!(() {
    try {
      VMIFace["conhookRemoveAllEnemies"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("remove_enemies", "remove all enemy entities");
}


// ////////////////////////////////////////////////////////////////////////// //
// actor queues; used to speedup rendering
private __gshared ActorId[][int] actorQueues;

private void aq_reset_all () {
  foreach (ref v; actorQueues.byValue) {
    v.length = 0;
    v.assumeSafeAppend;
  }
}

private void aq_append (ActorId act, int qidx) {
  if (!act.alive) return; // anyway
  if (auto ap = qidx in actorQueues) {
    (*ap) ~= act;
  } else {
    ActorId[] qq;
    qq ~= act;
    actorQueues[qidx] = qq;
  }
}

private void aq_reset (int qidx) {
  if (auto ap = qidx in actorQueues) {
    (*ap).length = 0;
    (*ap).assumeSafeAppend;
  }
}

private int aq_count (int qidx) {
  if (auto ap = qidx in actorQueues) return cast(int)((*ap).length);
  return 0;
}

private ActorId aq_get (int qidx, int idx) {
  if (idx >= 0) {
    if (auto ap = qidx in actorQueues) return (idx < (*ap).length ? (*ap)[idx] : ActorId.init);
  }
  return ActorId.init;
}


// ////////////////////////////////////////////////////////////////////////// //
private void doConWrite (const(int)[] vargs) {
  foreach (immutable aidx; 0..vargs.mesVAGetArgCount) {
    auto tp = vargs.mesVAGetArgType(aidx);
    if (tp is null) assert(0);
    // avoid needless allocations
         if (tp.isInt) conwrite(vargs.mesVAGetArgValue!int(aidx));
    else if (tp.isActor) conwrite("{", vargs.mesVAGetArgValue!ActorId(aidx).id, "}");
    else if (tp.isEnum) {
      auto et = cast(TypeEnum)tp;
      string mname = et.getMember(vargs.mesVAGetArgValue!int(aidx));
      if (mname !is null) conwrite(et.name, ".", mname); else conwrite(et.name, "(", vargs.mesVAGetArgValue!int(aidx), ")");
    } else conwrite(vargs.mesVAGetArgValue!string(aidx));
  }
}


// [0]: sin; [1]: cos
private immutable int[2][360] sincostbl = () {
  import std.math : cos, sin, PI;
  int[2][360] sincostbl;
  foreach (immutable angle; 0..360) {
    immutable rang = angle*PI/180;
    sincostbl[angle][0] = cast(int)(1024*sin(rang));
    sincostbl[angle][1] = cast(int)(1024*cos(rang));
  }
  return sincostbl;
}();


public void loadScripts () {
  import std.functional : toDelegate;

  //MESDisableDumps = true;

  mesOnCompilerMessage = delegate (string s) { conwriteln(s); };

  mesIntroduceConstant("StageCount", StageCount);
  mesIntroduceConstant("ScrTilesH", ScrTilesH);
  mesIntroduceConstant("MapWidth", MapWidth);
  mesIntroduceConstant("MapHeight", MapHeight);

  mesIntroduceEnum!ActionKey;
  mesIntroduceEnum!PrizeType;

  mesRegisterBuiltin("abort", delegate (string msg) { assert(0, msg); });

  mesRegisterBuiltin("isStick", delegate (int kidx) {
    return (kidx >= ActionKey.min && kidx <= ActionKey.max ? actionPressed(cast(ActionKey)kidx) : false);
  });

  mesRegisterBuiltin("isStickWasRelease", delegate (int kidx) {
    return (kidx >= ActionKey.min && kidx <= ActionKey.max ? actionWasRelease(cast(ActionKey)kidx) : false);
  });

  mesRegisterBuiltin("conwrite", delegate (const(int)[] vargs) { doConWrite(vargs); });
  mesRegisterBuiltin("conwriteln", delegate (const(int)[] vargs) { doConWrite(vargs); conwriteln(); });

  mesRegisterBuiltin("spawn", (&spawnActor).toDelegate);
  mesRegisterBuiltin("kill", (&killActor).toDelegate);

  mesRegisterBuiltin("alive", delegate (ActorId act) { return act.alive; });

  mesRegisterBuiltin("Sound", (&Sound).toDelegate);

  mesRegisterBuiltin("NumObjects", (&NumObjects).toDelegate);

  mesRegisterBuiltin("putSprite", delegate (string pfx, int idx, int x, int y, int ctint=-1) { putSprite(pfx, cast(uint)idx, x, y, ctint); });

  mesRegisterBuiltin("SpawnRandom", (&SpawnRandom).toDelegate);
  mesRegisterBuiltin("Random", (&Random).toDelegate);

  mesRegisterBuiltin("stageHCAt", (&stageHCAt).toDelegate);
  mesRegisterBuiltin("stageAt", (&stageAt).toDelegate);

  mesRegisterBuiltin("stageHCPutAt", (&stageHCPutAt).toDelegate);
  mesRegisterBuiltin("stagePutAt", (&stagePutAt).toDelegate);

  mesRegisterBuiltin("iterFirst", (&actorIterStart).toDelegate);
  mesRegisterBuiltin("iterNext", (&actorIterNext).toDelegate);

  mesRegisterBuiltin("stageMonstersClear", delegate () { mons.length = 0; mons.assumeSafeAppend; });
  mesRegisterBuiltin("stageMonstersCount", delegate () => cast(int)mons.length);
  mesRegisterBuiltin("stageMonstersAppendEmpty", delegate () {
    if (mons.length > int.max/1024) assert(0, "too many stage monsters added");
    mons.length += 1;
    mons[$-1].type = 0/*MonsType.Nothing*/;
    return cast(int)mons.length-1;
  });

  mesRegisterBuiltin("getStageMonsterTypeAt", delegate (int idx) => (idx >= 0 && idx < mons.length ? mons[idx].type : 0));
  mesRegisterBuiltin("getStageMonsterDelayAt", delegate (int idx) => (idx >= 0 && idx < mons.length ? mons[idx].delayNext : 0));
  mesRegisterBuiltin("getStageMonsterPowerAt", delegate (int idx) => (idx >= 0 && idx < mons.length ? mons[idx].power : 0));
  mesRegisterBuiltin("getStageMonsterGangIdAt", delegate (int idx) => (idx >= 0 && idx < mons.length ? mons[idx].groupid : 0));
  mesRegisterBuiltin("getStageMonsterGangCountAt", delegate (int idx) => (idx >= 0 && idx < mons.length ? mons[idx].groupcount : 0));

  mesRegisterBuiltin("setStageMonsterTypeAt", delegate (int idx, int v) { if (idx >= 0 && idx < mons.length) mons[idx].type = v; });
  mesRegisterBuiltin("setStageMonsterDelayAt", delegate (int idx, int v) { if (idx >= 0 && idx < mons.length) mons[idx].delayNext = v; });
  mesRegisterBuiltin("setStageMonsterPowerAt", delegate (int idx, int v) { if (idx >= 0 && idx < mons.length) mons[idx].power = v; });
  mesRegisterBuiltin("setStageMonsterGangIdAt", delegate (int idx, int v) { if (idx >= 0 && idx < mons.length) mons[idx].groupid = v; });
  mesRegisterBuiltin("setStageMonsterGangCountAt", delegate (int idx, int v) { if (idx >= 0 && idx < mons.length) mons[idx].groupcount = v; });

  mesRegisterBuiltin("cheatGod", delegate () => cast(bool)cheatGod);
  mesRegisterBuiltin("cheatGod", delegate (bool v) { cheatGod = v; });

  mesRegisterBuiltin("cheatInvul", delegate () => cast(bool)cheatInvul);
  mesRegisterBuiltin("cheatInvul", delegate (bool v) { cheatInvul = v; });

  mesRegisterBuiltin("cheatGhost", delegate () => cast(bool)cheatGhost);
  mesRegisterBuiltin("cheatGhost", delegate (bool v) { cheatGhost = v; });

  mesRegisterBuiltin("cheatKonamiCode", delegate () => cast(bool)cheatKonamiCode);
  mesRegisterBuiltin("cheatKonamiCode", delegate (bool v) { cheatKonamiCode = v; });

  mesRegisterBuiltin("cheatSpawnGun", delegate () => cast(bool)cheatSpawnGun);
  mesRegisterBuiltin("cheatSpawnGun", delegate (bool v) { cheatSpawnGun = v; });

  mesRegisterBuiltin("cheatSpawnPup", delegate () => cast(bool)cheatSpawnPup);
  mesRegisterBuiltin("cheatSpawnPup", delegate (bool v) { cheatSpawnPup = v; });

  mesRegisterBuiltin("cheatSpawnType", delegate () => cast(int)cheatSpawnType);
  mesRegisterBuiltin("cheatSpawnType", delegate (int v) { cheatSpawnType = v; });

  mesRegisterBuiltin("cheatSpawnPower", delegate () => cast(int)cheatSpawnPower);
  mesRegisterBuiltin("cheatSpawnPower", delegate (int v) { cheatSpawnPower = v; });

  mesRegisterBuiltin("cheatToDevil", delegate () => cast(bool)cheatToDevil);
  mesRegisterBuiltin("cheatToDevil", delegate (bool v) { cheatToDevil = v; });

  mesRegisterBuiltin("cheatDevilDead", delegate () => cast(bool)cheatDevilDead);
  mesRegisterBuiltin("cheatDevilDead", delegate (bool v) { cheatDevilDead = v; });

  mesRegisterBuiltin("cheatSetStage", delegate () => cast(int)cheatSetStage);
  mesRegisterBuiltin("cheatSetStage", delegate (int v) { cheatSetStage = v; });

  mesRegisterBuiltin("loadStageData", (&loadStageData).toDelegate);
  mesRegisterBuiltin("resetStagePRNGs", (&resetStagePRNGs).toDelegate);

  // used for bat gangs
  mesRegisterBuiltin("clearGangs", delegate () { batganginfo[] = -1; });
  mesRegisterBuiltin("newGang", delegate (int gangid, int gangcount) {
    if (gangcount > 0 && gangid > 0 && gangid < batganginfo.length && batganginfo[gangid] == -1) {
      batganginfo[gangid] = gangcount;
      //conwriteln("new bat gang ", gangid);
    }
  });
  mesRegisterBuiltin("gangMemberDead", delegate (int gangid) {
    if (gangid > 0 && gangid < batganginfo.length) {
      --batganginfo[gangid];
      //conwriteln("gang ", gangid, " has ", batganginfo[gangid], " members lefs");
    }
  });
  mesRegisterBuiltin("isWholeGangDead", delegate (int gangid) {
    if (gangid > 0 && gangid < batganginfo.length) {
      //conwriteln("checking gang ", gangid, ": alldead=", (batganginfo[gangid] == 0));
      return (batganginfo[gangid] == 0);
    }
    return false;
  });

  mesRegisterBuiltin("sin", delegate (int degrees) {
    degrees = (degrees%360+360)%360;
    return sincostbl[degrees][0];
  });

  mesRegisterBuiltin("cos", delegate (int degrees) {
    degrees = (degrees%360+360)%360;
    return sincostbl[degrees][1];
  });


  mesRegisterBuiltin("soundAbort", (&soundAbort).toDelegate);
  mesRegisterBuiltin("musicLoopCount", (&musicLoopCount).toDelegate);
  mesRegisterBuiltin("musicPaused", delegate () => musicPaused);
  mesRegisterBuiltin("musicPaused", delegate (bool v) { musicPaused = v; });
  mesRegisterBuiltin("musicNew", (&musicNew).toDelegate);
  mesRegisterBuiltin("musicAbort", (&musicAbort).toDelegate);

  mesRegisterBuiltin("optionSoundOn", delegate () => cast(bool)xoptSoundOn);
  mesRegisterBuiltin("optionSoundOn", delegate (bool v) { xoptSoundOn = v; });

  mesRegisterBuiltin("optionMusicOn", delegate () => cast(bool)xoptMusicOn);
  mesRegisterBuiltin("optionMusicOn", delegate (bool v) { xoptMusicOn = v; });

  mesRegisterBuiltin("optionContinuousFire", delegate () => cast(bool)xoptContinuousFire);
  mesRegisterBuiltin("optionContinuousFire", delegate (bool v) { xoptContinuousFire = v; });

  conRegVar!xoptContinuousFire("cheat_continuous_fire", "allow continuous fire by holding fire button");

  mesRegisterBuiltin("optFixedSpawn", delegate () => cast(bool)optFixedSpawn);

  mesRegisterBuiltin("concmd", delegate (string cmd) { if (cmd.length) concmd(cmd); });

  mesRegisterBuiltin("actionBindClear", (&actionBindClear).toDelegate);
  mesRegisterBuiltin("actionBindFrameEnd", (&actionBindFrameEnd).toDelegate);


  mesRegisterBuiltin("GC_Collect", delegate () { import core.memory : GC; GC.collect(); GC.minimize(); });

  mesRegisterBuiltin("actorsPack", (&actorsPack).toDelegate);
  mesRegisterBuiltin("actorsGC", (&actorsGC).toDelegate);

  mesRegisterBuiltin("fillRect", delegate (int x, int y, int w, int h, int r, int g, int b, int a) {
    if (a <= 0 || w < 1 || h < 1) return;
    if (a > 255) a = 255;
    if (r < 0) r = 0; else if (r > 255) r = 255;
    if (g < 0) g = 0; else if (g > 255) g = 255;
    if (b < 0) b = 0; else if (b > 255) b = 255;
    glDisable(GL_TEXTURE_2D);
    if (a != 255) glEnable(GL_BLEND); else glDisable(GL_BLEND);
    glColor4f(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
    glBegin(GL_QUADS);
      glVertex2i(x, y);
      glVertex2i(x+w, y);
      glVertex2i(x+w, y+h);
      glVertex2i(x, y+h);
    glEnd();
    glColor3f(1, 1, 1);
    glDisable(GL_BLEND);
  });


  mesRegisterBuiltin("aq_reset_all", (&aq_reset_all).toDelegate);
  mesRegisterBuiltin("aq_append", (&aq_append).toDelegate);
  mesRegisterBuiltin("aq_reset", (&aq_reset).toDelegate);
  mesRegisterBuiltin("aq_count", (&aq_count).toDelegate);
  mesRegisterBuiltin("aq_get", (&aq_get).toDelegate);


  mesRegisterBuiltin("loadSpriteGfx", (&loadSpriteGfx).toDelegate);


  try {
    mesCompileFile(VFile("mes/main.mes"), delegate (const(char)[] fname) {
      conwriteln("...including script '", fname, "'...");
    });
    if (!mesCheckUndefined()) {
      mesDumpUndefined(stderr);
      assert(0);
    }
  } catch (CompilerError e) {
    conwriteln("ERROR in ", e.loc.fname, " at ", e.loc, ": ", e.msg);
    //throw e;
    assert(0);
  }

  { import core.memory : GC; GC.collect(); GC.minimize(); }
}
