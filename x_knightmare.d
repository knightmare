/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * Based on the DOS Knightmare source code by Andrew Zabolotny
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module x_knightmare;

import arsd.color;
import arsd.simpledisplay;
import arsd.image;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.cmdcon.keybinds;
import iv.glbinds.util;
import iv.prng.pcg32;
import iv.prng.seeder;
import iv.strex;
import iv.vfs;

import tatlas;
import zmythspr;
import keybinds;
import levelmap;
import mesactor;
import mesengine;
import monsters;
import music;


// ////////////////////////////////////////////////////////////////////////// //
version(single_binary) {
  immutable pakdata = import("data/base.pak");
}


// ////////////////////////////////////////////////////////////////////////// //
private __gshared VFSDriver dataPathDriver;
private __gshared string dataPath;


// ////////////////////////////////////////////////////////////////////////// //
void setDataPath (const(char)[] path) {
  if (dataPathDriver !is null) assert(0);
  if (path.length == 0) {
    dataPath = "./";
  } else if (path[$-1] != '/') {
    dataPath = path.idup~"/";
  } else {
    dataPath = path.idup;
  }
  dataPathDriver = vfsNewDiskDriver(dataPath);
  vfsRegister!"first"(dataPathDriver);
}


string getDataPath () { pragma(inline, true); return dataPath; }


void addPak (const(char)[] fname) {
  conwriteln("; adding '", fname, "'...");
  vfsAddPak(fname);
}


void addPak (VFile fl) {
  vfsAddPak(fl);
}


// ////////////////////////////////////////////////////////////////////////// //
int c2i() (in auto ref Color c) nothrow @safe @nogc {
  pragma(inline, true);
  return c.r|(c.g<<8)|(c.b<<16);
}


Color i2c() (in int c) nothrow @safe @nogc {
  pragma(inline, true);
  return Color(c&0xff, (c>>8)&0xff, (c>>16)&0xff);
}


// ////////////////////////////////////////////////////////////////////////// //
void initTitle () {
  MemoryImage title;
  scope(exit) delete title;
  {
    auto fl = VFile("title.png");
    if (fl.size > 1024*1024*32) assert(0, "title image too big");
    auto data = new ubyte[](cast(uint)fl.size);
    scope(exit) delete data;
    fl.rawReadExact(data);
    title = loadImageFromMemory(data);
  }
  uint[] img;
  scope(exit) delete img;
  img.length = title.width*title.height;
  // sky color
  auto csky = title.getPixel(0, 0);
  VMIFace["gtitle_csky"].set!int(c2i(csky));
  // find lightning colors
  Color[2] clgt;
  foreach (immutable dx; 1..title.width) if (title.getPixel(dx, 0) != csky) { clgt[0] = title.getPixel(dx, 0); break; }
  foreach_reverse (immutable dx; 1..title.width) if (title.getPixel(dx, 0) != csky) { clgt[1] = title.getPixel(dx, 0); break; }
  // create first overlay image with lightning
  foreach (immutable dy; 0..title.height) {
    foreach (immutable dx; 0..title.width) {
      Color c = title.getPixel(dx, dy);
      if (c != clgt[0]) c = Color.transparent; else c = Color.white;
      img[dy*title.width+dx] = c.asUint;
    }
  }
  atlasAdd("titleovl_", 0, img[], title.width, title.height);
  // create second overlay image with lightning
  foreach (immutable dy; 0..title.height) {
    foreach (immutable dx; 0..title.width) {
      Color c = title.getPixel(dx, dy);
      if (c != clgt[1]) c = Color.transparent; else c = Color.white;
      img[dy*title.width+dx] = c.asUint;
    }
  }
  atlasAdd("titleovl_", 1, img[], title.width, title.height);
  // create title image without lightnings
  foreach (immutable dy; 0..title.height) {
    foreach (immutable dx; 0..title.width) {
      Color c = title.getPixel(dx, dy);
      if (c == clgt[0] || c == clgt[1]) c = csky;
      img[dy*title.width+dx] = c.asUint;
    }
  }
  atlasAdd("title_", 0, img[], title.width, title.height);
}


// ////////////////////////////////////////////////////////////////////////// //
void createGamePhases () {
  conRegVar!paused("pause", "pause game");

  conRegFunc!(() {
    try {
      VMIFace["conhookAbortGame"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("abort_game", "abort current game and return to title");

  conRegFunc!(() {
    try {
      VMIFace["conhookGameOver"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("game_over", "run \"game over\" sequence");

  conRegFunc!(() {
    try {
      VMIFace["conhookStageNext"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("stage_next", "go to next stage");

  conRegFunc!(() {
    try {
      VMIFace["conhookStagePrev"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  })("stage_prev", "go to previous stage");
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool optPauseOnDefocus = true;
__gshared bool optVSync = true;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  import std.functional : toDelegate;

  glconShowKey = "M-Grave";

  conRegVar!optFixedSpawn("predictable_spawn", "should spawn PRNG be initialized with fixed seeds? (default: no)");
  conRegVar!optPauseOnDefocus("defocus_pause", "autopause the game when window loses focus");
  conRegVar!optVSync("v_vsync", "OpenGL vsync");
  conRegVar!Scale(1, 16, "v_scale", "game scale (can be set only on startup)");

  MESDisableDumps = true;
  conRegVar!MESDisableDumps("dbg_vm_nodump", "don't dump compiler VM code");

  //glconSetAndSealFPS(18);
  glconSetAndSealFPS(20);
  //glconSetAndSealFPS(35);

  static void setDP () {
    version(single_binary) {
      //immutable pakdata = import("data/base.pak");
      addPak(wrapMemoryRO(pakdata, "data/base.pak"));
    } else {
      version(rdmd) {
        setDataPath("data");
      } else version(Windows) {
        setDataPath("data");
      } else {
        import std.file : thisExePath;
        import std.path : dirName;
        setDataPath(thisExePath.dirName~"/data");
      }
      try {
        addPak(getDataPath~"base.pak");
      } catch (Exception e) {
      }
    }
  }

  setDP();

  setupDefaultKeyBindings();

  conProcessQueue(256*1024); // load config
  conProcessArgs!true(args);
  conProcessQueue(256*1024);

  conSealVar("predictable_spawn");
  conSealVar("v_vsync");
  conSealVar("v_scale");

  mesIntroduceEnum!Key; // arsd.Key

  loadScripts();

  mesRegisterBuiltin("paused", delegate () => cast(bool)paused);
  mesRegisterBuiltin("paused", delegate (bool v) { paused = v; });

  mesRegisterBuiltin("isAudioFucked", delegate () => cast(bool)isAudioFucked);

  mesRegisterBuiltin("optPauseOnDefocus", delegate () => cast(bool)optPauseOnDefocus);

  mesRegisterBuiltin("atlasUpdateTexture", delegate (string pfx, int idx) {
    if (auto ai = atlasGet(pfx, idx)) ai.updateTexture();
  });

  mesRegisterBuiltin("atlasSetPixel", delegate (string pfx, int idx, int x, int y, int c) {
    if (auto ai = atlasGet(pfx, idx)) {
      ai.setPixel(x, y, i2c(c));
    }
  });

  mesRegisterBuiltin("atlasGetPixel", delegate (string pfx, int idx, int x, int y) {
    if (auto ai = atlasGet(pfx, idx)) {
      auto clr = ai.getPixel(x, y);
      if (clr.a == 0) return -666;
      return c2i(clr);
    } else {
      return -666;
    }
  });


  loadStages();
  loadMonsters();
  loadMusic();
  loadSounds();

  musicStartThread();

  //paused = true;

  // first time setup
  oglSetupDG = delegate () {
    glconCtlWindow.vsync = optVSync;

    createGamePhases();
    initTitle();
    seedPRNGs();

    try {
      VMIFace["onGameLoadGraphics"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }

    // this will create texture
    //gxResize(glconCtlWindow.width, glconCtlWindow.height);

    atlasLoadGL();

    try {
      VMIFace["onGameInit"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  };

  // draw main screen
  redrawFrameDG = delegate () {
    oglSetup2D(glconCtlWindow.width, glconCtlWindow.height);

    /*
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_ACCUM_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    */

    // scale everything
    glMatrixMode(GL_MODELVIEW);
    glScalef(Scale, Scale, 1);

    try {
      VMIFace["onGameRender"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  };

  // rebuild main screen (do any calculations we might need)
  nextFrameDG = delegate () {
    try {
      VMIFace["onGameTick"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
    actionBindFrameEnd();
  };

  keyEventDG = delegate (KeyEvent event) {
    try {
      VMIFace["eventKeyKey"].set!Key(event.key);
      VMIFace["eventKeyPressed"].set!bool(event.pressed);
      VMIFace["eventKeyCtrl"].set!bool((event.modifierState&ModifierState.ctrl) != 0);
      VMIFace["eventKeyAlt"].set!bool((event.modifierState&ModifierState.alt) != 0);
      VMIFace["eventKeyShift"].set!bool((event.modifierState&ModifierState.shift) != 0);
      VMIFace["eventKeyHyper"].set!bool((event.modifierState&ModifierState.windows) != 0);

      // process bindings
      ActionKey kk;
      bool changed = doActionBind(event, &kk);
      VMIFace["onActionBindChanged"].set!bool(changed);
      VMIFace["onActionBindWasPress"].set!bool(event.pressed);
      if (changed) VMIFace["actionBindKK"].set!ActionKey(kk);

      VMIFace["onGameKeyEvent"]();
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  };

  /*
  mouseEventDG = delegate (MouseEvent event) {
  };

  charEventDG = delegate (dchar ch) {
  };

  resizeEventDG = delegate (int wdt, int hgt) {
  };
  */

  focusEventDG = delegate (bool focused) {
    try {
      VMIFace["onGameFocusEvent"](focused);
    } catch (Throwable e) {
      conwriteln("FATAL: ", e.msg);
      assert(0, e.msg);
    }
  };

  glconRunGLWindow/*Resizeable*/(320*Scale, 200*Scale, "Konami's Knightmare", "KNIGHTMARE");
  musicQuit();
}
