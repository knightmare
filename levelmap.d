/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * Based on the DOS Knightmare source code by Andrew Zabolotny
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module levelmap;

import arsd.color;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.cmdcon.gl;
import iv.cmdcon.keybinds;
import iv.glbinds.util;
import iv.prng.seeder;
import iv.prng.pcg32;
import iv.strex;
import iv.vfs.io;

import music;
import tatlas;
import zmythspr;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared int Scale = 3;
enum StageCount = 8;
enum MapWidth = 20;
enum MapHeight = 136;
enum ScrTilesH = 12;


// ////////////////////////////////////////////////////////////////////////// //
struct MonsterSpawn {
  int type;
  int delayNext;
  int power;
  // for bats
  int groupid;
  int groupcount;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte[MapWidth*MapHeight][StageCount] stages;
__gshared MonsterSpawn[][StageCount] monsters;

__gshared ubyte[MapWidth*MapHeight] stage, stagec; // c: tile hitcounter
__gshared MonsterSpawn[] mons;


int stageAt (int y, int x) nothrow @trusted @nogc {
  pragma(inline, true);
  return (x >= 0 && y >= 0 && x < MapWidth && y < MapHeight ? stage.ptr[y*MapWidth+x] : 0);
}

void stagePutAt (int y, int x, int b) nothrow @trusted @nogc {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < MapWidth && y < MapHeight) stage.ptr[y*MapWidth+x] = b&0xff;
}

int stageHCAt (int y, int x) nothrow @trusted @nogc {
  pragma(inline, true);
  return (x >= 0 && y >= 0 && x < MapWidth && y < MapHeight ? stagec.ptr[y*MapWidth+x] : 0);
}

void stageHCPutAt (int y, int x, int b) nothrow @trusted @nogc {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < MapWidth && y < MapHeight) stagec.ptr[y*MapWidth+x] = b&0xff;
}


__gshared PCG32 monsprng, spawnprng;

__gshared bool paused = false;

__gshared bool cheatGod = false;
__gshared bool cheatInvul = false;
__gshared bool cheatGhost = false;
__gshared bool cheatToDevil = false;
__gshared bool cheatSpawnGun = false;
__gshared bool cheatSpawnPup = false;
__gshared bool cheatDevilDead = false;
__gshared int cheatSpawnType = 0;
__gshared int cheatSpawnPower = 0;
__gshared int cheatSetStage = 0;
__gshared bool cheatKonamiCode = false;

__gshared bool optFixedSpawn = false;


shared static this () {
  conRegVar!cheatGod("god", "toggle god mode");
  conRegVar!cheatInvul("invul", "toggle invulnerability mode");
  conRegVar!cheatGhost("ghost", "toggle ghost mode");
  conRegVar!cheatSetStage("stage", "set current stage");
  conRegFunc!(() { cheatToDevil = true; })("todevil", "teleport to devil");
  conRegFunc!(() { cheatSpawnGun = true; })("sp_gun", "spawn weapon powerup");
  conRegFunc!(() { cheatSpawnPup = true; })("sp_pup", "spawn protection powerup");
  conRegFunc!(() { cheatDevilDead = true; })("kill_devil", "immediately kill the devil");
}


void seedPRNGs () {
  if (!optFixedSpawn) {
    spawnprng.seed(getUlongSeed(), 666);
    monsprng.seed(spawnprng.front, 69);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void loadStages () {
  auto fl = VFile("stages.stg");
  foreach (ref stg; stages[]) fl.rawReadExact(stg[]);
  assert(fl.tell == fl.size);
  // visible prizes:
  //   16: bridge
  //   17: rook
  //   18: wall
  //   19: knight
  //   20: queen
  // invisible prizes:
  //   21: king
  //   22: exit
  //   23: bridge (fast)
  //stages[0][(MapHeight-12)*MapWidth+10] = 22;
  /*
  stages[0][(MapHeight-12)*MapWidth+11] = 17;
  stages[0][(MapHeight-12)*MapWidth+12] = 18;
  stages[0][(MapHeight-12)*MapWidth+13] = 19;
  stages[0][(MapHeight-12)*MapWidth+14] = 20;
  stages[0][(MapHeight-12)*MapWidth+15] = 21;
  stages[0][(MapHeight-12)*MapWidth+16] = 22;
  stages[0][(MapHeight-12)*MapWidth+17] = 23;
  */
}


void loadMonsters () {
  auto fl = VFile("monsters.stg");
  foreach (ref mm; monsters[]) {
    ushort count = fl.readVarUNum!ushort;
    //conwriteln(count, " monsters...");
    foreach (immutable _; 0..count) {
      MonsterSpawn m;
      m.type = fl.readNum!ubyte;
      //if (m.type < 1 || m.type > MonsType.max) assert(0, "invalid monster type");
      m.delayNext = fl.readVarUNum!ushort;
      //if (m.delayNext == 0) assert(0, "invalid monster delay");
      m.power = fl.readVarUNum!ushort;
      mm ~= m;
    }
  }
  assert(fl.tell == fl.size);
}


// ////////////////////////////////////////////////////////////////////////// //
// returns next stidx
int loadSpriteGfx (string prefix, string fname, int stidx) {
  conwriteln("loading '", fname, "'...");
  bool masked = fname.endsWithCI(".SPR");
  auto fl = VFile(fname);
  uint[] img;
  scope(exit) delete img;
  while (fl.tell < fl.size) {
    auto spr = new MythSprite(fl, masked);
    scope(exit) delete spr;
    if (img.length < spr.width*spr.height) img.length = spr.width*spr.height;
    foreach (immutable dy; 0..spr.height) {
      foreach (immutable dx; 0..spr.width) {
        img[dy*spr.width+dx] = spr.getpix(dx, dy).asUint;
      }
    }
    atlasAdd(prefix, stidx++, img[], spr.width, spr.height);
  }
  return stidx;
}


// ////////////////////////////////////////////////////////////////////////// //
void resetStagePRNGs (int stidx) {
  monsprng.seed(stidx, 666);
  spawnprng.seed(stidx, 42);
}


void loadStageData (int stidx) {
  if (stidx < 1) stidx = 1; else if (stidx > StageCount) stidx = StageCount;
  stage[] = stages[stidx-1][];
  mons = monsters[stidx-1];
}


// ////////////////////////////////////////////////////////////////////////// //
void Sound (int idx, int xpos) {
  static immutable int[36] prio = [2,2,3,2,3,4,2,2,3,3,3,2,3,4,3,3,2,1,10,10,3,3,3,2,3,5,4,3,2,2,4,3,3,3,1,1];
  if (idx < 0 || idx > 35) return;
  //conwriteln("sound #", idx, "; prio=", prio[idx]);
  int realidx = idx-(idx == 29 ? 1 : 0);
  //realidx = 19; // i'll be back
  soundPlay(realidx, xpos, prio[idx]);
}


// ////////////////////////////////////////////////////////////////////////// //
void putSprite (const(char)[] prefix, uint idx, int x, int y, int ctint=-1) {
  auto ai = atlasGet(prefix, idx);
  if (ai is null) return;
  alias glx = x;
  alias gly = y;
  /*
  if (ctint == -1) {
    glColor4f(1, 1, 1, 1);
  } else {
    glColor4f((ctint&0xff)/255.0, ((ctint>>8)&0xff)/255.0, ((ctint>>16)&0xff)/255.0, 1);
  }
  */
  glColor4f(1, 1, 1, 1);
  //static if (alpha) glEnable(GL_BLEND); else glDisable(GL_BLEND);
  glEnable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, ai.tex);
  glBegin(GL_QUADS);
    glTexCoord2f(ai.frc.x0, ai.frc.y0); glVertex2i(glx, gly); // top-left
    glTexCoord2f(ai.frc.x1, ai.frc.y0); glVertex2i(glx+ai.rc.w, gly); // top-right
    glTexCoord2f(ai.frc.x1, ai.frc.y1); glVertex2i(glx+ai.rc.w, gly+ai.rc.h); // bottom-right
    glTexCoord2f(ai.frc.x0, ai.frc.y1); glVertex2i(glx, gly+ai.rc.h); // bottom-left
  glEnd();
  if (ctint != -1) {
    glColor4f((ctint&0xff)/255.0, ((ctint>>8)&0xff)/255.0, ((ctint>>16)&0xff)/255.0, 0.5);
    glBegin(GL_QUADS);
      glTexCoord2f(ai.frc.x0, ai.frc.y0); glVertex2i(glx, gly); // top-left
      glTexCoord2f(ai.frc.x1, ai.frc.y0); glVertex2i(glx+ai.rc.w, gly); // top-right
      glTexCoord2f(ai.frc.x1, ai.frc.y1); glVertex2i(glx+ai.rc.w, gly+ai.rc.h); // bottom-right
      glTexCoord2f(ai.frc.x0, ai.frc.y1); glVertex2i(glx, gly+ai.rc.h); // bottom-left
    glEnd();
  }
  glDisable(GL_BLEND);
}


// ////////////////////////////////////////////////////////////////////////// //
int SpawnRandom (int limit) {
  if (limit < 2) return 0;
  int res = cast(int)(spawnprng.front&0x7fff_ffff)%limit;
  spawnprng.popFront();
  return res;
}


int Random (int limit) {
  if (limit < 2) return 0;
  int res = cast(int)(monsprng.front&0x7fff_ffff)%limit;
  monsprng.popFront();
  return res;
}
