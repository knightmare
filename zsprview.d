/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module zsprview is aliced;

import arsd.simpledisplay;
import iv.vfs.io;

import zmythspr;


// ////////////////////////////////////////////////////////////////////////// //
MythSprite[] splist;
int spidx = 0;


// ////////////////////////////////////////////////////////////////////////// //
void paint (SimpleWindow w) {
  auto painter = w.draw();
  painter.outlineColor = Color.black;
  painter.fillColor = Color(32, 32, 32);
  painter.drawRectangle(Point(0, 0), w.width, w.height);

  void putpix (int x, int y, Color c) {
    painter.outlineColor = c;
    painter.fillColor = c;
    painter.drawRectangle(Point(x*4, y*4), 4, 4);
  }

  if (spidx < 0 || spidx >= splist.length) return;
  auto spr = splist[spidx];

  foreach (immutable dy; 0..spr.height) {
    foreach (immutable dx; 0..spr.width) {
      Color c = spr.getpix(dx, dy);
      if (c.a) putpix(dx, dy, c);
    }
  }
  writeln("spidx: ", spidx);
}


// ////////////////////////////////////////////////////////////////////////// //
void main () {
  auto dataPathDriver = vfsNewDiskDriver("data");
  vfsRegister!"first"(dataPathDriver);
  try {
    vfsAddPak("base.pak");
  } catch (Exception e) {
  }

  //splist = loadSprites("data/DEVILS.SPR", true);
  //splist = loadSprites("data/BULLET.SPR", true);
  //splist = loadSprites("SPR16X16.SPR", true);
  //splist = loadSprites("data/SPR16X24.SPR", true);
  //splist = loadSprites("data/SHADOWS.SPR", true);
  //splist = loadSprites("data/SPR8X8.SMB", false);
  //splist = loadSprites("data/TILES.SMB", false);
  //splist = loadSprites("data/ZST.SMB", false);
  //splist = loadSprites("data/BACK1.SMB", false);
  //splist = loadSprites("data/BACK2.SMB", false);
  //splist = loadSprites("data/FSOFT.SPR", true);
  splist = loadSprites("back1.smb", false);
  auto sdwin = new SimpleWindow(800, 600, "SprView");
  paint(sdwin);
  sdwin.eventLoop(0,
    delegate (KeyEvent ev) {
      if (!ev.pressed) return;
      if (ev == "Escape") sdwin.close();
      if (ev == "Left" && spidx > 0) { --spidx; paint(sdwin); }
      if (ev == "Right" && spidx < cast(int)splist.length-1) { ++spidx; paint(sdwin); }
    },
  );
}
