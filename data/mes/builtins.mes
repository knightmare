/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * Based on the DOS Knightmare source code by Andrew Zabolotny
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// ////////////////////////////////////////////////////////////////////////// //
enum Klass {
  Invalid,
  Normal,
  IAmBall,
  IAmShooted,
  IAmBadGuy,
  IAmWeapon,
  IAmProtection,
  Killed,
  IAmDevil,
  Finish,
  Dead,
  IAmFire,
  IAmHole,
  IAmPrincess,
  GameEnd,
  OverBridge,
  ThatIsAll,
  IAmBlinking,
  IAmBonus,
}


enum MonsType {
  Nothing,
  Sphere,
  Cloud,
  Vampir,
  Vampir1,
  Knight,
  Gun,
  Prot,
  Carlson,
  Daemon,
  Hag,
  JBall,
  Skeleton,
  Chaos,
  Mine,
  WalkingGuy,
  Monk,
  Ghost,
}


// ////////////////////////////////////////////////////////////////////////// //
void abort (string msg) builtin;

void conwrite (...) builtin;
void conwriteln (...) builtin;

alias write = conwrite;
alias writeln = conwriteln;

Actor spawn () builtin;
method void kill () builtin;
method bool alive () builtin;
int NumObjects () builtin;

/*
int NumPlayers () builtin;
int NumAlivePlayers () builtin;
Actor PlayerByIndex (int idx) builtin; // [1..2]

method bool isPlayer () builtin;
method bool isAlivePlayer () builtin;

method bool isBullet
*/

method bool isPlayer () {
  if (!alive) return false;
  auto kls = MyState;
  return
    kls == Klass.Normal ||
    kls == Klass.Killed ||
    kls == Klass.Finish ||
    kls == Klass.Dead ||
    kls == Klass.GameEnd ||
    kls == Klass.OverBridge ||
    kls == Klass.ThatIsAll ||
    kls == Klass.IAmBlinking;
}

method bool isAlivePlayer () {
  if (!alive) return false;
  auto kls = MyState;
  return
    kls == Klass.Normal ||
    kls == Klass.Finish ||
    kls == Klass.GameEnd ||
    kls == Klass.OverBridge ||
    kls == Klass.ThatIsAll ||
    kls == Klass.IAmBlinking;
}

method bool isBullet () {
  if (!alive) return false;
  return (MyState == Klass.IAmBall);
}

method bool isPlayerBullet () {
  if (!alive) return false;
  return (MyState == Klass.IAmShooted);
}

method bool isPowerup () {
  if (!alive) return false;
  auto kls = MyState;
  return
    kls == Klass.IAmWeapon ||
    kls == Klass.IAmProtection;
}

method bool isAphrodite () {
  if (!alive) return false;
  return (MyState == Klass.IAmPrincess);
}

method bool isDevil () {
  if (!alive) return false;
  return (MyState == Klass.IAmDevil);
}

method bool isHole () {
  if (!alive) return false;
  return (MyState == Klass.IAmHole);
}

method bool isEnemyOrFire () {
  if (!alive) return false;
  auto kls = MyState;
  return
    kls == Klass.IAmBadGuy ||
    kls == Klass.IAmFire ||
    kls == Klass.IAmBonus;
}


int NumPlayers () {
  int res = 0;
  for (auto act = iterFirst; act; act = act.iterNext) if (act.isPlayer) ++res;
  return res;
}

int NumAlivePlayers () {
  int res = 0;
  for (auto act = iterFirst; act; act = act.iterNext) if (act.isAlivePlayer) ++res;
  return res;
}

Actor PlayerByIndex (int idx) {
  if (idx < 1 || idx > 2) return null;
  for (auto act = iterFirst; act; act = act.iterNext) {
    if (act.isPlayer) {
      if (--idx == 0) return act;
    }
  }
  return null;
}


int SpawnRandom (int max) builtin;
int Random (int max) builtin;

void Sound (int idx, int xpos) builtin;
void putSprite (string pfx, int idx, int x, int y, int ctint=-1) builtin;

int stageHCAt (int y, int x) builtin;
int stageAt (int y, int x) builtin;

void stageHCPutAt (int y, int x, int v) builtin;
void stagePutAt (int y, int x, int v) builtin;

// warning! objects, spawned during the iteration, may or may not be included in iteration
Actor iterFirst () builtin;
Actor iterNext (Actor act) builtin; // returns `null` if there are no more actors

void stageMonstersClear () builtin;
int stageMonstersCount () builtin;
int stageMonstersAppendEmpty () builtin; // returns index

int getStageMonsterTypeAt (int idx) builtin;
int getStageMonsterDelayAt (int idx) builtin;
int getStageMonsterPowerAt (int idx) builtin;
int getStageMonsterGangIdAt (int idx) builtin;
int getStageMonsterGangCountAt (int idx) builtin;

void setStageMonsterTypeAt (int idx, int v) builtin;
void setStageMonsterDelayAt (int idx, int v) builtin;
void setStageMonsterPowerAt (int idx, int v) builtin;
void setStageMonsterGangIdAt (int idx, int v) builtin;
void setStageMonsterGangCountAt (int idx, int v) builtin;

bool cheatGod () builtin;
void cheatGod (bool v) builtin;
bool cheatInvul () builtin;
void cheatInvul (bool v) builtin;
bool cheatGhost () builtin;
void cheatGhost (bool v) builtin;
bool cheatInvincible () { return (cheatGhost || cheatInvul); }
bool cheatKonamiCode () builtin;
void cheatKonamiCode (bool v) builtin;
bool cheatSpawnGun () builtin;
void cheatSpawnGun (bool v) builtin;
bool cheatSpawnPup () builtin;
void cheatSpawnPup (bool v) builtin;
int cheatSpawnType () builtin;
void cheatSpawnType (int v) builtin;
int cheatSpawnPower () builtin;
void cheatSpawnPower (int v) builtin;
bool cheatToDevil () builtin;
void cheatToDevil (bool v) builtin;
bool cheatDevilDead () builtin;
void cheatDevilDead (bool v) builtin;
int cheatSetStage () builtin;
void cheatSetStage (int v) builtin;

bool isStick (int keyidx) builtin;
bool isStickWasRelease (int keyidx) builtin;

bool actionPressed (ActionKey key) { return isStick(cast(int)key); }
bool actionWasRelease (ActionKey key) { return isStickWasRelease(cast(int)key); }

// used for bat gangs
void clearGangs () builtin;
void newGang (int gangid, int gangcount) builtin;
void gangMemberDead (int gangid) builtin;
bool isWholeGangDead (int gangid) builtin;


int sin (int degrees) builtin; // *1024
int cos (int degrees) builtin; // *1024


void loadStageData (int stagenum) builtin; // map and monster spawn info
void resetStagePRNGs (int stagenum) builtin;
bool optFixedSpawn () builtin;

void GC_Collect () builtin;
void actorsPack () builtin;
void actorsGC () builtin;


// remove players
void removeAllEnemies () {
  for (auto act = iterFirst; act; act = act.iterNext) act.kill();
  actorsGC();
}

// won't remove players
void removeEverybodyExceptPlayers () {
  for (auto act = iterFirst; act; act = act.iterNext) if (!act.isPlayer) act.kill();
  actorsPack();
}


void soundAbort () builtin;
void musicAbort () builtin;
int musicLoopCount () builtin;
bool musicPaused () builtin;
void musicPaused (bool v) builtin;
void musicNew (string name, bool looped) builtin;

bool isAudioFucked () builtin;

bool paused () builtin;
void paused (bool v) builtin;


void fillRect (int x, int y, int w, int h, int r, int g, int b, int a=255) builtin;


void aq_reset_all () builtin;
void aq_append (Actor act, int qidx) builtin;
void aq_reset (int qidx) builtin;
int aq_count (int qidx) builtin;
Actor aq_get (int qidx, int idx) builtin;


bool optionSoundOn () builtin;
bool optionMusicOn () builtin;
bool optionContinuousFire () builtin;

void optionSoundOn (bool v) builtin;
void optionMusicOn (bool v) builtin;
void optionContinuousFire (bool v) builtin;

bool optPauseOnDefocus () builtin;

void concmd (string cmd) builtin;
void actionBindClear () builtin;
void actionBindFrameEnd () builtin;

ActionKey actionBindKK; // set at each frame
bool onActionBindChanged; // was `actionBindKK` pressed or released at this frame?
bool onActionBindWasPress; // was `actionBindKK` pressed at this frame?

void atlasSetPixel (string pfx, int idx, int x, int y, int c) builtin;
int atlasGetPixel (string pfx, int idx, int x, int y) builtin;
void atlasUpdateTexture (string pfx, int idx) builtin;


/*
struct KeyEvent {
  Key key;
  bool pressed;
}

KeyEvent eventKey;
*/
Key eventKeyKey;
bool eventKeyPressed;
bool eventKeyCtrl;
bool eventKeyAlt;
bool eventKeyShift;
bool eventKeyHyper;
