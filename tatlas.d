/* coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// texture atlases; used for sprites
module tatlas;
private:

import iv.cmdcon;
import iv.glbinds;


// ////////////////////////////////////////////////////////////////////////// //
__gshared uint atlasW = 1024, atlasH = 1024;
__gshared bool dbgAtlasRect = false;

shared static this () {
  conRegVar!atlasW(256, 4096, "r_atlas_width", "texture atlas width");
  conRegVar!atlasH(256, 4096, "r_atlas_height", "texture atlas height");
  conRegVar!dbgAtlasRect("dbg_atlas_rect", "draw bounding rectangle on each texture in atlas");
}


public void atlasSealVars () {
  conSealVar("r_atlas_width");
  conSealVar("r_atlas_height");
  conwriteln("atlas size: ", atlasW, "x", atlasH);
}


// ////////////////////////////////////////////////////////////////////////// //
public final class TexAtlas {
public:
  static struct Rect {
    int x, y, w, h;
    alias x0 = x;
    alias y0 = y;
  pure nothrow @safe @nogc:
    @property bool valid () const pure { pragma(inline, true); return (x >= 0 && y >= 0 && w > 0 && h > 0); }
    @property int area () const pure { pragma(inline, true); return w*h; }
    @property int x1 () const pure { pragma(inline, true); return x+w; }
    @property int y1 () const pure { pragma(inline, true); return y+h; }
    static @property Rect Invalid () pure { Rect res; return res; }
  }

  // texture coords in atlas
  static struct FRect {
    float x0, y0, x1, y1;
  }

private:
  enum BadRect = uint.max;

public:
  uint[] img;
  Rect[] rects;
  uint wdt, hgt;
  uint ogltex; // OpenGL texture

public:
  this (int awdt, int ahgt) nothrow @trusted {
    assert(awdt > 0 && ahgt > 0);
    img = new uint[](awdt*ahgt);
    img[] = 0xff000000u;
    rects ~= Rect(0, 0, awdt, ahgt); // one big rect
    wdt = awdt;
    hgt = ahgt;
  }

  @property const pure nothrow @safe @nogc {
    int width () { pragma(inline, true); return wdt; }
    int height () { pragma(inline, true); return hgt; }
    bool hasTexture () { pragma(inline, true); return (ogltex != 0); }
    uint tex () { pragma(inline, true); return ogltex; }
  }

  // return opengl texture id or 0
  uint createTexture () nothrow @trusted {
    releaseTexture();

    enum wrapOpt = GL_REPEAT;
    enum filterOpt = GL_NEAREST; //GL_LINEAR;
    enum ttype = GL_UNSIGNED_BYTE;

    glGenTextures(1, &ogltex);
    if (ogltex == 0) {
      conwriteln("can't create atlas texture");
      return 0;
    }

    GLint gltextbinding;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &gltextbinding);
    scope(exit) glBindTexture(GL_TEXTURE_2D, gltextbinding);

    glBindTexture(GL_TEXTURE_2D, ogltex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    GLfloat[4] bclr = 0.0;
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, wdt, hgt, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.ptr);

    return ogltex;
  }

  // release (delete) OpenGL texture
  void releaseTexture () nothrow @trusted @nogc {
    if (ogltex != 0) {
      glDeleteTextures(1, &ogltex);
      ogltex = 0;
    }
  }

  void updateTexture () nothrow @trusted {
    if (dbgAtlasRect) {
      foreach (const ref rc; rects) {
        imgRect(rc.x, rc.y, rc.w, rc.h, 0xff00ffu);
      }
    }
    if (ogltex == 0) {
      createTexture();
    } else {
      //glTextureSubImage2D(ogltex, 0, 0/*x*/, 0/*y*/, wdt, hgt, GL_RGBA, GL_UNSIGNED_BYTE, img.ptr);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0/*x*/, 0/*y*/, wdt, hgt, GL_RGBA, GL_UNSIGNED_BYTE, img.ptr); // this updates texture
    }
  }

  FRect texCoords (in Rect rc) nothrow @trusted @nogc {
    FRect res;
    res.x0 = cast(float)rc.x/cast(float)(width);
    res.y0 = cast(float)rc.y/cast(float)(height);
    res.x1 = cast(float)(rc.x+rc.w)/cast(float)(width);
    res.y1 = cast(float)(rc.y+rc.h)/cast(float)(height);
    return res;
  }

  // node id or BadRect
  private uint findBestFit (int w, int h) nothrow @trusted @nogc {
    uint fitW = BadRect, fitH = BadRect, biggest = BadRect;
    foreach (immutable idx, const ref r; rects) {
      if (r.w < w || r.h < h) continue; // absolutely can't fit
      if (r.w == w && r.h == h) return cast(uint)idx; // perfect fit
      if (r.w == w) {
        // width fit
        if (fitW == BadRect || rects.ptr[fitW].h < r.h) fitW = cast(uint)idx;
      } else if (r.h == h) {
        // height fit
        if (fitH == BadRect || rects.ptr[fitH].w < r.w) fitH = cast(uint)idx;
      } else {
        // get biggest rect
        if (biggest == BadRect || rects.ptr[biggest].area > r.area) biggest = cast(uint)idx;
      }
    }
    // both?
    if (fitW != BadRect && fitH != BadRect) return (rects.ptr[fitW].area > rects.ptr[fitH].area ? fitW : fitH);
    if (fitW != BadRect) return fitW;
    if (fitH != BadRect) return fitH;
    return biggest;
  }

  private void imgPutPixel (int x, int y, uint c) nothrow @trusted @nogc {
    if (x >= 0 && y >= 0 && x < wdt && y < hgt) img.ptr[y*wdt+x] = c|0x80000000u;
  }

  private void imgRect (int x, int y, int w, int h, uint c) nothrow @trusted @nogc {
    foreach (int d; 0..w) { imgPutPixel(x+d, y, c); imgPutPixel(x+d, y+h-1, c); }
    foreach (int d; 1..h-1) { imgPutPixel(x, y+d, c); imgPutPixel(x+w-1, y+d, c); }
  }

  private void imgRect4 (int x, int y, int w, int h, uint ct, uint cb, uint cl, uint cr) nothrow @trusted @nogc {
    foreach (int d; 0..w) { imgPutPixel(x+d, y, ct); imgPutPixel(x+d, y+h-1, cb); }
    foreach (int d; 1..h-1) { imgPutPixel(x, y+d, cl); imgPutPixel(x+w-1, y+d, cr); }
  }

  // returns invalid rect if there is no room
  Rect insert (const(uint)[] ximg, int xwidth, int xheight) @trusted {
    assert(xwidth > 0 && xheight > 0);
    if (xwidth > wdt || xheight > hgt) return Rect.Invalid;
    auto ri = findBestFit(xwidth, xheight);
    if (ri == BadRect) return Rect.Invalid;
    auto rc = rects.ptr[ri];
    auto res = Rect(rc.x, rc.y, xwidth, xheight);
    // split this rect
    if (rc.w == res.w && rc.h == res.h) {
      // best fit, simply remove this rect
      foreach (immutable cidx; ri+1..rects.length) rects.ptr[cidx-1] = rects.ptr[cidx];
      rects.length -= 1;
      rects.assumeSafeAppend; // for future; we probably won't have alot of best-fitting nodes initially
    } else {
      if (rc.w == res.w) {
        // split vertically
        rc.y += res.h;
        rc.h -= res.h;
      } else if (rc.h == res.h) {
        // split horizontally
        rc.x += res.w;
        rc.w -= res.w;
      } else {
        Rect nr = rc;
        // split in both directions (by longer edge)
        if (rc.w-res.w > rc.h-res.h) {
          // cut the right part
          nr.x += res.w;
          nr.w -= res.w;
          // cut the bottom part
          rc.y += res.h;
          rc.h -= res.h;
          rc.w = res.w;
        } else {
          // cut the bottom part
          nr.y += res.h;
          nr.h -= res.h;
          // cut the right part
          rc.x += res.w;
          rc.w -= res.w;
          rc.h = res.h;
        }
        rects ~= nr;
      }
      rects.ptr[ri] = rc;
    }
    // copy image data
    auto dpl = img.ptr+res.y*wdt+res.x;
    foreach (immutable dy; 0..xheight) {
      auto dp = dpl;
      auto sp = dy*xwidth;
      foreach (immutable dx; 0..xwidth) {
        if (sp >= ximg.length) break;
        *dp++ = ximg.ptr[sp++];
      }
      dpl += wdt;
    }
    if (dbgAtlasRect) {
      imgRect4(res.x, res.y, res.w, res.h, 0x0000ffu, 0x00ffffu, 0xff0000u, 0x00ff00u);
    }
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
enum MaxTexNameLength = 128;

public struct AtlasItem {
  uint tex;
  uint aidx; // atlas index in alist
  TexAtlas ta;
  TexAtlas.Rect rc;
  TexAtlas.FRect frc;
  char[MaxTexNameLength] namebuf = 0; // texture name
  const(char)[] name;

  Color getPixel (int x, int y) const nothrow @trusted @nogc {
    if (x >= 0 && y >= 0 && x < rc.w && y < rc.h) {
      immutable uint cu = ta.img[(y+rc.y)*ta.wdt+(x+rc.x)];
      return Color(cu&0xff, (cu>>8)&0xff, (cu>>16)&0xff, (cu>>24)&0xff);
    } else {
      return Color.transparent;
    }
  }

  void setPixel (int x, int y, in Color c) nothrow @trusted @nogc {
    if (x >= 0 && y >= 0 && x < rc.w && y < rc.h) ta.img[(y+rc.y)*ta.wdt+(x+rc.x)] = c.asUint;
  }

  void updateTexture () nothrow @trusted {
    ta.updateTexture();
  }
}

__gshared TexAtlas[] alist;
__gshared AtlasItem[string] aatex;


void atadd (const(char)[] name, uint aidx, TexAtlas.Rect rc) {
  AtlasItem ai;
  ai.aidx = aidx;
  ai.ta = alist.ptr[aidx];
  ai.rc = rc;
  ai.frc = alist.ptr[aidx].texCoords(rc);
  ai.namebuf[0..name.length] = name[];
  ai.name = ai.namebuf[0..name.length];
  aatex[name.idup] = ai;
  //conwriteln("[", name, "]: (", rc.x, ",", rc.y, ":", rc.w, ",", rc.h, ")-(", ai.frc.x0, ",", ai.frc.y0, ")-(", ai.frc.x1, ",", ai.frc.y1, ")");
}


// number of texture atlases
public uint atlasCount () {
  return cast(uint)alist.length;
}


// total number of textures atlases
public uint atlasTexCount () {
  return cast(uint)aatex.length;
}


// remove all texture atlases
public void atlasClear(bool clear=true) () {
  static if (clear) {
    foreach (TexAtlas ta; alist) {
      ta.atlas.releaseTexture();
      delete ta;
    }
  }
  delete alist;
  aatex.clear;
}


// check if texture with this name already created
public bool atlasHas (const(char)[] name) {
  if (name.length == 0) return false; // unnamed texture, get out of here
  if (name.length > MaxTexNameLength) name = name[0..MaxTexNameLength];
  return ((name in aatex) !is null);
}


public bool atlasHas (const(char)[] prefix, uint idx) {
  import core.stdc.stdio : snprintf;
  if (prefix.length > 1024) throw new Exception("prefix too long");
  char[1100] nbuf;
  nbuf[0..prefix.length] = prefix[];
  auto len = snprintf(nbuf.ptr+prefix.length, nbuf.length-prefix.length, "%u".ptr, idx);
  return atlasHas(nbuf[0..prefix.length+len]);
}


// remove all texture atlases
public void atlasAdd (const(char)[] name, const(uint)[] ximg, int xwidth, int xheight) {
  if (name.length == 0) return; // unnamed texture, get out of here
  if (name.length > MaxTexNameLength) name = name[0..MaxTexNameLength];
  if (xwidth < 1 || xheight < 1 || xwidth > 4096 || xheight > 4096) throw new Exception("invalid texture size");
  auto iname = name.idup;
  // find atlas to put into
  version(all) {
    foreach (immutable tidx, TexAtlas ta; alist) {
      auto rc = ta.insert(ximg, xwidth, xheight);
      if (rc.valid) {
        atadd(name, cast(uint)tidx, rc);
        return;
      }
    }
    import std.algorithm : max;
    // create new atlas
    auto ta = new TexAtlas(max(xwidth, atlasW), max(xheight, atlasH));
    alist ~= ta;
    auto rc = ta.insert(ximg, xwidth, xheight);
    if (!rc.valid) assert(0, "wtf?!");
    atadd(name, cast(uint)alist.length-1, rc);
  } else {
    // create new atlas
    auto ta = new TexAtlas(xwidth, xheight);
    alist ~= ta;
    auto rc = ta.insert(ximg, xwidth, xheight);
    if (!rc.valid) assert(0, "wtf?!");
    atadd(name, cast(uint)alist.length-1, rc);
  }
}


// remove all texture atlases
public void atlasAdd (const(char)[] prefix, uint idx, const(uint)[] ximg, int xwidth, int xheight) {
  import core.stdc.stdio : snprintf;
  if (prefix.length > 1024) throw new Exception("prefix too long");
  char[1100] nbuf;
  nbuf[0..prefix.length] = prefix[];
  auto len = snprintf(nbuf.ptr+prefix.length, nbuf.length-prefix.length, "%u".ptr, idx);
  atlasAdd(nbuf[0..prefix.length+len], ximg, xwidth, xheight);
}


// load all atlases into GPU
public void atlasLoadGL () {
  foreach (TexAtlas ta; alist) {
    if (ta.createTexture() == 0) throw new Exception("can't load atlas texture to GPU");
  }
  // fix texids
  foreach (ref AtlasItem ai; aatex.byValue) {
    if (ai.aidx >= alist.length) assert(0, "wtf?!");
    ai.tex = alist[ai.aidx].tex;
  }
}


// bind OpenGL texture
public AtlasItem* atlasBindGL (const(char)[] name) {
  if (name.length == 0) {
    // unnamed texture, get out of here
    glBindTexture(GL_TEXTURE_2D, 0);
    return null;
  }
  if (name.length > MaxTexNameLength) name = name[0..MaxTexNameLength];
  if (auto aip = name in aatex) {
    glBindTexture(GL_TEXTURE_2D, aip.tex);
    return aip;
  } else {
    glBindTexture(GL_TEXTURE_2D, 0);
    return null;
  }
}


public AtlasItem* atlasGet (const(char)[] name) {
  if (name.length == 0) return null; // unnamed texture, get out of here
  if (name.length > MaxTexNameLength) name = name[0..MaxTexNameLength];
  if (auto aip = name in aatex) return aip;
  return null;
}


public AtlasItem* atlasGet (const(char)[] prefix, uint idx) {
  import core.stdc.stdio : snprintf;
  if (prefix.length > 1024) throw new Exception("prefix too long");
  char[1100] nbuf;
  nbuf[0..prefix.length] = prefix[];
  auto len = snprintf(nbuf.ptr+prefix.length, nbuf.length-prefix.length, "%u".ptr, idx);
  return atlasGet(nbuf[0..prefix.length+len]);
}


import iv.bclamp;
import arsd.color;
import arsd.png;

public void atlasSavePngs () {
  import std.format : format;
  TrueColorImage img;
  foreach (immutable tidx, TexAtlas ti; alist) {
    auto fname = "z_%02d.png".format(tidx);
    conwriteln("writing '", fname, "'");
    if (img is null || img.width != ti.width || img.height != ti.height) img = new TrueColorImage(ti.width, ti.height);
    foreach (immutable dy; 0..ti.height) {
      foreach (immutable dx; 0..ti.width) {
        auto tc = ti.img[dy*ti.width+dx];
        Color c;
        c.asUint = tc;
        img.setPixel(dx, dy, c);
      }
    }
    writePng(fname, img);
  }
}


shared static this () {
  conRegFunc!atlasSavePngs("dbg_save_atlases", "save atlases to PNG images");
}
