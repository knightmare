/* Sound support
 * Copyright (c) 2000-2004 Russell Marks, Matan Ziv-Av, Philip Kendall
 * Copyright (c) 2016 Fredrick Meunier
 * Copyright (c) 2017 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
module music;
/* The AY white noise RNG algorithm is based on info from MAME's ay8910.c -
 * MAME's licence explicitly permits free use of info (even encourages it).
 */
import arsd.simpleaudio;

import iv.cmdcon;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
private:
import iv.blipbuf;

public enum AYFreq = 1789773;
enum CPUSpeed = 3579545;
enum TSPerFrame = AYFreq/2/60; // NTSC; PAL is 50

__gshared volumeAY = 150; // [0..400]
__gshared oldVolumeAY = 0;

enum speakerType = 2;

public struct SpeakerEqConfig {
  int bass;
  double treble;
}


static immutable SpeakerEqConfig[4] speakerEqConfig = [
  SpeakerEqConfig(200, -37.0), // TV
  SpeakerEqConfig(1000, -67.0), // Beeper
  SpeakerEqConfig(16, -8.0), // "normal" from bleepbuffer dox
  SpeakerEqConfig(0, 0.0), // Unfiltered
];


alias BlipSynth = BlipSynthBase!(BlipBuffer.Good, 32767);


/* Must be <=127 for all channels; (40*3) = 120.
 * (Now scaled up for 16-bit.)
 */
enum AmplAYTone = 40*256; // three of these


final class AYEmu {
public:
  // the AY steps down the external clock by 16 for tone and noise generators
  enum AY_CLOCK_DIVISOR = 16;

private:
  static immutable uint[16] toneLevels = (){
    /* AY output doesn't match the claimed levels; these levels are based
     * on the measurements posted to comp.sys.sinclair in Dec 2001 by
     * Matthew Westcott, adjusted as I described in a followup to his post,
     * then scaled to 0..0xffff.
     */
    static immutable ushort[16] levels = [
      0x0000, 0x0385, 0x053D, 0x0770,
      0x0AD7, 0x0FD5, 0x15B0, 0x230C,
      0x2B4C, 0x43C1, 0x5A4B, 0x732F,
      0x9204, 0xAFF1, 0xD921, 0xFFFF
    ];

    uint[16] res;
    // scale the values down to fit
    foreach (immutable f; 0..16) res[f] = (levels[f]*AmplAYTone+0x8000)/0xffff;
    return res;
  }();

  // bitmasks for envelope
  enum AY_ENV_CONT = 8;
  enum AY_ENV_ATTACK = 4;
  enum AY_ENV_ALT = 2;
  enum AY_ENV_HOLD = 1;

private:
  uint[3] toneTick, toneHigh;
  uint noiseTick;
  uint toneCycles, envCycles;
  uint envInternalTick, envTick;
  uint[3] tonePeriod;
  uint noisePeriod, envPeriod;
  ubyte[16] registers; // local copy of the AY registers
  BlipSynth[3] synth;

private:
  int rng = 1;
  int noiseToggle = 0;
  int envFirst = 1, envRev = 0, envCounter = 15;
  int[3] lastChanLevel = 0;

public:
  int framesDone = 0;

public:
  this () { reset(); }

  /// call this after initializing blip buffer
  void setupBlips () {
    immutable double treble = speakerEqConfig[speakerType].treble;
    foreach (ref BlipSynth sn; synth[]) {
      if (sn is null) sn = new BlipSynth();
      sn.setVolumeTreble(soundGetVolume(volumeAY), treble);
      sn.output = blipBuf;
    }
    oldVolumeAY = volumeAY;
  }

  /// call this on machine reset
  void reset () nothrow @trusted @nogc {
    noiseTick = noisePeriod = 0;
    envInternalTick = envTick = envPeriod = 0;
    toneCycles = envCycles = 0;
    toneTick[] = 0;
    toneHigh[] = 0;
    tonePeriod[] = 1;
    rng = 1;
    noiseToggle = 0;
    envFirst = 1;
    envRev = 0;
    envCounter = 15;
    framesDone = 0;
    lastChanLevel[] = -1;
    foreach (immutable f; 0..16) writeReg(f, 0, 0);
  }

  /// change AY register (immediate change)
  void writeReg (ubyte reg, ubyte val, uint nowts) nothrow @trusted @nogc {
    static immutable ubyte[16] ayvaluemask = [
      0xff, 0x0f, 0xff, 0x0f, 0xff, 0x0f, 0x1f, 0xff,
      0x1f, 0x1f, 0x1f, 0xff, 0xff, 0x0f, 0xff, 0xff,
    ];
    reg &= 0x0f;
    val &= ayvaluemask.ptr[reg&0x0f];
    // update ay register
    registers.ptr[reg] = val;
    // fix things as needed for some register changes
    switch (reg) {
      case 0: case 1: case 2: case 3: case 4: case 5:
        ubyte r = reg>>1;
        // a zero-len period is the same as 1
        tonePeriod.ptr[r] = (registers.ptr[reg&~1]|(registers.ptr[reg|1]&15)<<8);
        if (!tonePeriod.ptr[r]) ++tonePeriod.ptr[r];
        // important to get this right, otherwise e.g. Ghouls 'n' Ghosts has really scratchy, horrible-sounding vibrato
        if (toneTick.ptr[r] >= tonePeriod.ptr[r]*2) toneTick.ptr[r] %= tonePeriod.ptr[r]*2;
        break;
      case 6:
        noiseTick = 0;
        noisePeriod = registers.ptr[reg]&31;
        break;
      case 11: case 12:
        envPeriod = registers.ptr[11]|(registers.ptr[12]<<8);
        break;
      case 13:
        envInternalTick = envTick = envCycles = 0;
        envFirst = 1;
        envRev = 0;
        envCounter = (registers.ptr[13]&AY_ENV_ATTACK ? 0 : 15);
        break;
      default: break;
    }
  }

  private void doTone (ubyte chan, int level, uint toneCount, ref int var) nothrow @trusted @nogc {
    pragma(inline, true);
    toneTick.ptr[chan] += toneCount;
    if (toneTick.ptr[chan] >= tonePeriod.ptr[chan]) {
      toneTick.ptr[chan] -= tonePeriod.ptr[chan];
      toneHigh.ptr[chan] = !toneHigh.ptr[chan];
    }
    var = (level ? (toneHigh.ptr[chan] ? level : 0) : 0);
  }

  /// synthesize AY sound
  void soundOverlay (int samples) nothrow @trusted @nogc {
    if (samples < 1) return;

    int[3] toneLevel = void;
    int[3] lastChan = lastChanLevel[];
    uint endts = framesDone+AYFreq*samples/SampleRate;
    for (; framesDone < endts; framesDone += AY_CLOCK_DIVISOR) {
      // the tone level if no enveloping is being used
      foreach (immutable g; 0..3) toneLevel.ptr[g] = toneLevels.ptr[registers.ptr[8+g]&15];

      // envelope
      immutable int envshape = registers.ptr[13];
      {
        immutable int level = toneLevels.ptr[envCounter];
        foreach (immutable g; 0..3) if (registers.ptr[8+g]&16) toneLevel.ptr[g] = level;
      }

      // envelope output counter gets incr'd every 16 AY cycles
      envCycles += AY_CLOCK_DIVISOR;
      int noiseCount = 0;
      while (envCycles >= 16) {
        envCycles -= 16;
        ++noiseCount;
        ++envTick;
        while (envTick >= envPeriod) {
          envTick -= envPeriod;

          // do a 1/16th-of-period incr/decr if needed
          if (envFirst || ((envshape&AY_ENV_CONT) && !(envshape&AY_ENV_HOLD))) {
            if (envRev) {
              envCounter -= (envshape&AY_ENV_ATTACK ? 1 : -1);
            } else {
              envCounter += (envshape&AY_ENV_ATTACK ? 1 : -1);
            }
            if (envCounter < 0 ) envCounter = 0;
            if (envCounter > 15) envCounter = 15;
          }

          ++envInternalTick;
          while (envInternalTick >= 16) {
            envInternalTick -= 16;

            // end of cycle
            if (!(envshape & AY_ENV_CONT)) {
              envCounter = 0;
            } else {
              if (envshape&AY_ENV_HOLD) {
                if (envFirst && (envshape&AY_ENV_ALT)) envCounter = (envCounter ? 0 : 15);
              } else {
                // non-hold
                if (envshape&AY_ENV_ALT) {
                  envRev = !envRev;
                } else {
                  envCounter = (envshape&AY_ENV_ATTACK ? 0 : 15);
                }
              }
            }

            envFirst = 0;
          }

          // don't keep trying if period is zero
          if (!envPeriod) break;
        }
      }

      /* generate tone+noise... or neither.
       * (if no tone/noise is selected, the chip just shoves the
       * level out unmodified. This is used by some sample-playing
       * stuff.)
       */
      toneCycles += AY_CLOCK_DIVISOR;
      immutable int toneCount = toneCycles>>3;
      toneCycles &= 7;

      immutable ubyte mixer = registers.ptr[7];
      foreach (immutable cidx, int cv; toneLevel[]) {
        if ((mixer&(0x01U<<cast(ubyte)cidx)) == 0) doTone(cast(ubyte)cidx, cv, toneCount, cv);
        if ((mixer&(0x08U<<cast(ubyte)cidx)) == 0 && noiseToggle) cv = 0;
        // we can skip `lastChan[]` logic, but it saves us some cycles by skipping bleepsynth
        if (lastChan.ptr[cidx] != cv) { synth.ptr[cidx].update(framesDone, cv); lastChan.ptr[cidx] = cv; }
      }

      // update noise RNG/filter
      noiseTick += noiseCount;
      while (noiseTick >= noisePeriod) {
        noiseTick -= noisePeriod;
        if ((rng&1)^(rng&2 ? 1 : 0)) noiseToggle = !noiseToggle;
        // rng is 17-bit shift reg, bit 0 is output. input is bit 0 xor bit 3.
        if (rng&1) rng ^= 0x24000;
        rng >>= 1;
        // don't keep trying if period is zero
        if (!noisePeriod) break;
      }
    }
    lastChanLevel[] = lastChan[];
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public __gshared BlipBuffer blipBuf;
public __gshared AYEmu sndvAY;


// ////////////////////////////////////////////////////////////////////////// //
double soundGetVolume (int volume) {
  if (volume < 0) volume = 0; else if (volume > 400) volume = 400;
  return volume/100.0;
}


// ////////////////////////////////////////////////////////////////////////// //
/// initialize sound system
public void soundInit () {
  // initialize blip buffer
  if (blipBuf is null) blipBuf = new BlipBuffer();
  blipBuf.clockRate = AYFreq; // Hz
  blipBuf.sampleRate = SampleRate;
  blipBuf.bassFreq = speakerEqConfig[speakerType].bass;
  // initialize A
  if (sndvAY is null) sndvAY = new AYEmu();
  sndvAY.setupBlips();
  sndvAY.reset();
}


// ////////////////////////////////////////////////////////////////////////// //
public int soundRead (short[] buffer) nothrow @trusted {
  return blipBuf.readSamples!(true, true)(buffer.ptr, cast(int)buffer.length/2)*2; // fake stereo
}


public void soundAdvanceSamples (uint count) nothrow @trusted {
  sndvAY.soundOverlay(count);
  if (sndvAY.framesDone) {
    blipBuf.endFrame(sndvAY.framesDone);
    sndvAY.framesDone = 0;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void soundResetAY () nothrow @trusted @nogc {
  if (sndvAY !is null) sndvAY.reset();
  blipBuf.clear();
}


// don't make the change immediately; record it for later, to be made by sound_frame() (via soundOverlay())
public void soundWriteAY (ubyte reg, ubyte val, uint nowts=0) nothrow @trusted @nogc {
  if (sndvAY !is null) sndvAY.writeReg(reg, val, nowts);
}


// ////////////////////////////////////////////////////////////////////////// //
struct PSGFrameData {
  ubyte[14] regs;
  ushort mask; // bit0: register 0 was changed; and so on
  ushort smpdelay; // wait this number of samples before next data chunk (can't be 0)

  void clear () pure nothrow @safe @nogc { regs[] = 0; mask = 0; smpdelay = 0; }

  bool opEquals() (in auto ref PSGFrameData frm) nothrow @trusted @nogc {
    import std.math : abs;
    if (frm.mask != mask) return false;
    //if (frm.smpdelay != smpdelay) return false;
    if (abs(cast(int)frm.smpdelay-cast(int)smpdelay) > 16) return false;
    foreach (immutable ubyte b; 0..14) {
      if (mask&(1<<b)) {
        if (frm.regs.ptr[b] != regs.ptr[b]) return false;
      }
    }
    return true;
  }
}

__gshared PSGFrameData[][string] muzax;


// ////////////////////////////////////////////////////////////////////////// //
PSGFrameData[] loadPSGF (VFile fl) {
  char[4] sign;
  fl.rawReadExact(sign[]);
  if (sign[] != "PSGF") assert(0, "invalid signature");
  if (fl.readNum!ubyte != 0) assert(0, "invalid version");

  uint flen = fl.readNum!uint;
  if (flen < 1 || flen > 32767) assert(0, "invalid number of frames");
  auto res = new PSGFrameData[](flen);

  foreach (ref frm; res[]) {
    fl.rawReadExact(frm.regs[]);
    frm.mask = fl.readNum!ushort;
    frm.smpdelay = fl.readNum!ushort;
  }

  //assert(fl.size == fl.tell);
  //writeln(res.length, " frames loaded");

  return res;
}


public void loadMusic () {
  void loadit (string name) {
    conwriteln("loading music '", name, "'...");
    muzax[name] = loadPSGF(VFile("mus/"~name~".psgf"));
  }
  loadit("bgm1");
  loadit("bgm2");
  loadit("ending");
  loadit("gameover");
  loadit("miss");
  loadit("powerup");
  loadit("start");
}


// ////////////////////////////////////////////////////////////////////////// //
import core.atomic;
import core.thread;
import core.time;

import iv.follin.resampler;
import iv.follin.utils;


__gshared const(PSGFrameData)[] curmusic;
__gshared uint curmusframe;
__gshared bool curmuslooped;

__gshared const(short)[] cursound = null;
__gshared uint cursndpos = 0;
__gshared int cursndprio = -666;
__gshared const(short)[] newsound = null;
shared int wantnewsound = 0;
shared int cursndcomplete = 0;

shared int musloopcount = 0;

shared int wantnewmusic = 0;
__gshared bool newmuslooped = false;
__gshared const(PSGFrameData)[] newmus = null;

shared bool muspaused = false;
shared int musthreadstate = 0;


public __gshared bool xoptMusicOn = true;
public __gshared bool xoptSoundOn = true;
public __gshared bool isAudioFucked = false;

shared static this () {
  conRegVar!xoptMusicOn("snd_music", "on/off music");
  conRegVar!xoptSoundOn("snd_sound", "on/off sound");
  conRegVar!volumeAY(0, 400, "mus_volume", "music volume");
}


// ////////////////////////////////////////////////////////////////////////// //
public @property int musicLoopCount () { return atomicLoad(musloopcount); } ///

public @property bool musicPaused () { return atomicLoad(muspaused); } ///
public @property void musicPaused (bool v) { atomicStore(muspaused, v); } ///


// ////////////////////////////////////////////////////////////////////////// //
public void musicNew (string name, bool looped) {
  if (auto mp = name in muzax) {
    while (cas(&wantnewmusic, 0, 1) != 0) {}
    newmuslooped = looped;
    newmus = *mp;
    atomicStore(muspaused, false);
    atomicStore(musloopcount, 0);
    atomicStore(wantnewmusic, 2);
    //conwriteln("queued new music: '", name, "' (looped: ", looped, ")");
  } else {
    musicAbort();
  }
}


public void musicAbort () {
  while (cas(&wantnewmusic, 0, 1) != 0) {}
  newmuslooped = false;
  newmus = null;
  atomicStore(muspaused, false);
  atomicStore(musloopcount, 0);
  atomicStore(wantnewmusic, 2);
}


// ////////////////////////////////////////////////////////////////////////// //
public void soundPlay (int idx, int pan, int prio) {
  if (idx < 0 || idx >= soundlist.length) { soundAbort(); return; }
  while (cas(&wantnewsound, 0, 1) != 0) {}
  bool allowed = (cursndprio < prio) || (atomicLoad(cursndcomplete) != 0);
  if (!allowed && cursndprio == prio && cursound.length-cursndpos <= soundlist[idx].length) allowed = true;
  if (allowed) {
    newsound = soundlist[idx];
    cursndprio = prio;
    atomicStore(wantnewsound, 2);
  } else {
    //conwriteln("rejected sound #", idx, " due to priority: cur=", cursndprio, "; prio=", prio);
  }
}


public void soundAbort () {
  while (cas(&wantnewsound, 0, 1) != 0) {}
  newsound = null;
  cursndprio = -666;
  atomicStore(cursndcomplete, 1);
  atomicStore(wantnewsound, 2);
}


// ////////////////////////////////////////////////////////////////////////// //
//TODO: panning [0..319]
void mixSound (short[] buffer) {
  //buffer[] = 0;
  if (cursndpos >= cursound.length) {
    atomicStore(cursndcomplete, 1);
    return;
  }
  auto left = buffer.length/2;
  if (left > cursound.length-cursndpos) left = cursound.length-cursndpos;
  auto sp = cast(const(short)*)cursound.ptr+cursndpos;
  auto dp = buffer.ptr;
  foreach (immutable idx; 0..left) {
    // left
    int v = *dp;
    v += *sp;
    if (v < short.min) v = short.min; else if (v > short.max) v = short.max;
    if (xoptSoundOn) *dp = cast(short)v;
    ++dp;
    // right
    v = *dp;
    v += *sp++;
    if (v < short.min) v = short.min; else if (v > short.max) v = short.max;
    if (xoptSoundOn) *dp = cast(short)v;
    ++dp;
  }
  cursndpos += left;
}


void renderMusic (short[] buffer) {
  // process volume changes
  auto vay = volumeAY;
  if (oldVolumeAY != volumeAY) sndvAY.setupBlips();
  // is music paused?
  if (atomicLoad(muspaused)) {
    buffer[] = 0;
    return;
  }
  // music complete?
  if (curmusframe >= curmusic.length) {
    if (curmusic.length > 0) {
      atomicOp!"+="(musloopcount, 1);
      if (!curmuslooped) curmusic = null;
    }
    if (!curmuslooped || curmusic.length == 0) {
      buffer[] = 0;
      return;
    }
    curmusframe = 0;
  }
  auto svbuf = buffer;
  // fill buffer
  while (buffer.length >= 2) {
    auto rd = soundRead(buffer[]);
    buffer = buffer[rd..$];
    if (buffer.length > 0) {
      if (curmusframe >= curmusic.length) {
        atomicOp!"+="(musloopcount, 1);
        if (!curmuslooped || curmusic.length == 0) {
          curmusic = null;
          break;
        }
        curmusframe = 0;
      }
      auto frm = &curmusic[curmusframe++];
      foreach (immutable ubyte b; 0..14) {
        if (frm.mask&(1U<<b)) soundWriteAY(b, frm.regs[b]);
      }
      soundAdvanceSamples(frm.smpdelay);
    }
  }
  if (buffer.length) buffer[0..$] = 0;
  // oops
  if (!xoptMusicOn) svbuf[] = 0;
}


void musicThread () {
  soundInit();
  try {
    auto ao = AudioOutput(0);
    isAudioFucked = (ao.handle is null);
    ao.fillData = (short[] buffer) {
      //TODO: proper locking
      if (atomicLoad(musthreadstate) == 666) {
        buffer[] = 0;
        ao.stop();
        return;
      }
      // want new music?
      if (atomicLoad(wantnewmusic) == 2) {
        curmusic = newmus;
        curmuslooped = newmuslooped;
        atomicStore(musloopcount, 0);
        soundResetAY();
        curmusframe = 0;
        atomicStore(wantnewmusic, 0);
        //conwriteln("started new music, ", curmusic.length, " frames");
      }
      // want new sound?
      if (atomicLoad(wantnewsound) == 2) {
        cursound = newsound;
        cursndpos = 0;
        atomicStore(cursndcomplete, (cursound.length == 0 ? 1 : 0));
        atomicStore(wantnewsound, 0);
      }
      renderMusic(buffer);
      // mix digital sound channel
      mixSound(buffer);
    };
    atomicStore(musthreadstate, 2);
    ao.play();
    atomicStore(musthreadstate, 667);
  } catch (Exception e) {
    isAudioFucked = true;
    if (atomicLoad(musthreadstate) != 666) atomicStore(musthreadstate, 2);
    for (;;) {
      import core.thread;
      import core.time;
      if (atomicLoad(musthreadstate) == 666) break;
      Thread.sleep(100.msecs);
    }
    atomicStore(musthreadstate, 667);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void musicStartThread () {
  if (atomicLoad(musthreadstate)) return;
  atomicStore(musthreadstate, 1);
  auto trd = new Thread(&musicThread);
  trd.isDaemon = true;
  trd.start();
}


public void musicQuit () {
  if (atomicLoad(musthreadstate)) {
    atomicStore(musthreadstate, 666);
    while (atomicLoad(musthreadstate) != 667) {}
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared short[][] soundlist;


public void loadSounds () {
  SpeexResampler srb;

  auto fl = VFile("MYTH.SND");
  fl.seek(9);
  //auto hend = fl.readNum!ubyte;
  //int hend = 219;
  static struct SDir { uint ofs; ushort len; }
  SDir[] dir;
  scope(exit) delete dir;
  int hend = 100;
  int count = 0;
  while (fl.tell < hend) {
    auto w0 = fl.readNum!uint;
    auto w1 = fl.readNum!ushort;
    //conprintfln("%2d: 0x%08x 0x%04x  %10d %5d", count, w0, w1, w0, w1);
    if (count == 0) hend = w0;
    dir ~= SDir(w0, w1);
    ++count;
  }

  float[] fbufout;
  scope(exit) delete fbufout;
  fbufout.length = 44100*10;

  //conwriteln(fl.tell, " : ", hend);
  //assert(fl.tell == hend);
  soundlist.length = dir.length;
  foreach (immutable sndidx, ref snd; soundlist) {
    byte[] xbuf;
    scope(exit) delete xbuf;
    // load sound
    xbuf.length = dir[sndidx].len;
    fl.seek(dir[sndidx].ofs);
    fl.rawReadExact(xbuf);
    // resample sound
    srb.setup(1, /*11025/2*/6100, 44100, /*alsaRQuality*/4);

    // convert to float
    float[] fbufin;
    scope(exit) delete fbufin;
    fbufin.length = xbuf.length;
    foreach (immutable idx; 0..xbuf.length) {
      fbufin[idx] = cast(float)xbuf[idx]/128.0f;
    }

    SpeexResampler.Data srbdata;
    uint inpos = 0;
    for (;;) {
      srbdata = srbdata.init; // just in case
      srbdata.dataIn = fbufin[inpos..$];
      srbdata.dataOut = fbufout[];
      if (srb.process(srbdata) != 0) assert(0, "resampling error");
      //{ import core.stdc.stdio; printf("inpos=%u; smpCount=%u; iu=%u; ou=%u\n", cast(uint)inpos, cast(uint)fbufin.length, cast(uint)srbdata.inputSamplesUsed, cast(uint)srbdata.outputSamplesUsed); }
      if (srbdata.outputSamplesUsed) {
        auto curpos = snd.length;
        snd.length += srbdata.outputSamplesUsed;
        tflFloat2Short(fbufout[0..srbdata.outputSamplesUsed], snd[curpos..curpos+srbdata.outputSamplesUsed]);
      } else {
        // no data consumed, no data produced, so we're done
        if (inpos >= fbufin.length) break;
      }
      inpos += cast(uint)srbdata.inputSamplesUsed;
    }
    conwriteln("converted sound #", sndidx+1, " of ", soundlist.length, " (", snd.length, ")");
  }
}
